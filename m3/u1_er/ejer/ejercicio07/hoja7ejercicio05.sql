﻿--
-- Script was generated by Devart dbForge Studio for MySQL, Version 8.0.40.0
-- Product home page: http://www.devart.com/dbforge/mysql/studio
-- Script date 15/01/2019 13:11:36
-- Server version: 5.5.5-10.1.36-MariaDB
-- Client version: 4.1
--

-- 
-- Disable foreign keys
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Set SQL mode
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Set character set the client will use to send SQL statements to the server
--
SET NAMES 'utf8';

--
-- Set default database
--
USE hoja7ejercicio05;

--
-- Drop table `disenan`
--
DROP TABLE IF EXISTS disenan;

--
-- Drop table `realizan`
--
DROP TABLE IF EXISTS realizan;

--
-- Drop table `practica`
--
DROP TABLE IF EXISTS practica;

--
-- Drop table `profesor`
--
DROP TABLE IF EXISTS profesor;

--
-- Drop table `hacen`
--
DROP TABLE IF EXISTS hacen;

--
-- Drop table `alumno`
--
DROP TABLE IF EXISTS alumno;

--
-- Drop table `examen`
--
DROP TABLE IF EXISTS examen;

--
-- Set default database
--
USE hoja7ejercicio05;

--
-- Create table `examen`
--
CREATE TABLE examen (
  nExamen int(11) NOT NULL AUTO_INCREMENT,
  nPreguntas int(11) DEFAULT NULL,
  fecha date DEFAULT NULL,
  PRIMARY KEY (nExamen)
)
ENGINE = INNODB,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create table `alumno`
--
CREATE TABLE alumno (
  matricula varchar(255) NOT NULL,
  nombre varchar(255) DEFAULT NULL,
  grupo varchar(255) DEFAULT NULL,
  PRIMARY KEY (matricula)
)
ENGINE = INNODB,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create table `hacen`
--
CREATE TABLE hacen (
  id int(11) NOT NULL AUTO_INCREMENT,
  fecha date DEFAULT NULL,
  matricula varchar(255) DEFAULT NULL,
  nExamen int(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create index `matricula` on table `hacen`
--
ALTER TABLE hacen
ADD UNIQUE INDEX matricula (matricula);

--
-- Create index `nExamen` on table `hacen`
--
ALTER TABLE hacen
ADD UNIQUE INDEX nExamen (nExamen);

--
-- Create foreign key
--
ALTER TABLE hacen
ADD CONSTRAINT FK_hacen_matricula FOREIGN KEY (matricula)
REFERENCES alumno (matricula) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Create foreign key
--
ALTER TABLE hacen
ADD CONSTRAINT FK_hacen_nExamen FOREIGN KEY (nExamen)
REFERENCES examen (nExamen) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Create table `profesor`
--
CREATE TABLE profesor (
  dni varchar(9) NOT NULL,
  nombre varchar(255) DEFAULT NULL,
  PRIMARY KEY (dni)
)
ENGINE = INNODB,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create table `practica`
--
CREATE TABLE practica (
  idPractica int(11) NOT NULL AUTO_INCREMENT,
  titulo varchar(255) DEFAULT NULL,
  dificultad varchar(255) DEFAULT NULL,
  PRIMARY KEY (idPractica)
)
ENGINE = INNODB,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create table `realizan`
--
CREATE TABLE realizan (
  id int(11) NOT NULL AUTO_INCREMENT,
  fecha date DEFAULT NULL,
  nota float DEFAULT NULL,
  matricula varchar(255) DEFAULT NULL,
  idPractica int(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create index `idPractica` on table `realizan`
--
ALTER TABLE realizan
ADD UNIQUE INDEX idPractica (idPractica);

--
-- Create foreign key
--
ALTER TABLE realizan
ADD CONSTRAINT FK_realizan_idPractica FOREIGN KEY (idPractica)
REFERENCES practica (idPractica) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Create foreign key
--
ALTER TABLE realizan
ADD CONSTRAINT FK_realizan_matricula FOREIGN KEY (matricula)
REFERENCES alumno (matricula) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Create table `disenan`
--
CREATE TABLE disenan (
  id int(11) NOT NULL AUTO_INCREMENT,
  idPractica int(11) DEFAULT NULL,
  fecha date DEFAULT NULL,
  dni varchar(9) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create index `dni` on table `disenan`
--
ALTER TABLE disenan
ADD UNIQUE INDEX dni (dni);

--
-- Create index `idPractica` on table `disenan`
--
ALTER TABLE disenan
ADD UNIQUE INDEX idPractica (idPractica);

--
-- Create foreign key
--
ALTER TABLE disenan
ADD CONSTRAINT FK_disenan_dni FOREIGN KEY (dni)
REFERENCES profesor (dni) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Create foreign key
--
ALTER TABLE disenan
ADD CONSTRAINT FK_disenan_idPractica FOREIGN KEY (idPractica)
REFERENCES practica (idPractica) ON DELETE CASCADE ON UPDATE CASCADE;

-- 
-- Dumping data for table examen
--
-- Table hoja7ejercicio05.examen does not contain any data (it is empty)

-- 
-- Dumping data for table alumno
--
-- Table hoja7ejercicio05.alumno does not contain any data (it is empty)

-- 
-- Dumping data for table profesor
--
-- Table hoja7ejercicio05.profesor does not contain any data (it is empty)

-- 
-- Dumping data for table practica
--
-- Table hoja7ejercicio05.practica does not contain any data (it is empty)

-- 
-- Dumping data for table realizan
--
-- Table hoja7ejercicio05.realizan does not contain any data (it is empty)

-- 
-- Dumping data for table hacen
--
-- Table hoja7ejercicio05.hacen does not contain any data (it is empty)

-- 
-- Dumping data for table disenan
--
-- Table hoja7ejercicio05.disenan does not contain any data (it is empty)

-- 
-- Restore previous SQL mode
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Enable foreign keys
-- 
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;