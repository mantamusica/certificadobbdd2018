﻿--
-- Script was generated by Devart dbForge Studio for MySQL, Version 8.0.40.0
-- Product home page: http://www.devart.com/dbforge/mysql/studio
-- Script date 22/01/2019 11:04:59
-- Server version: 5.5.5-10.1.36-MariaDB
-- Client version: 4.1
--

-- 
-- Disable foreign keys
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Set SQL mode
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

  DROP DATABASE IF EXISTS hoja8ejercicio02;

  CREATE DATABASE IF NOT EXISTS hoja8ejercicio02;
-- 
-- Set character set the client will use to send SQL statements to the server
--
SET NAMES 'utf8';

--
-- Set default database
--
USE hoja8ejercicio02;

--
-- Drop table `posee_casa`
--
DROP TABLE IF EXISTS posee_casa;

--
-- Drop table `posee_piso`
--
DROP TABLE IF EXISTS posee_piso;

--
-- Drop table `persona`
--
DROP TABLE IF EXISTS persona;

--
-- Drop table `piso`
--
DROP TABLE IF EXISTS piso;

--
-- Drop table `bloque_casas`
--
DROP TABLE IF EXISTS bloque_casas;

--
-- Drop table `casa_particular`
--
DROP TABLE IF EXISTS casa_particular;

--
-- Drop table `zona_urbana`
--
DROP TABLE IF EXISTS zona_urbana;

--
-- Set default database
--
USE hoja8ejercicio02;

--
-- Create table `zona_urbana`
--
CREATE TABLE zona_urbana (
  nombreZona varchar(255) NOT NULL,
  categoria varchar(255) DEFAULT NULL,
  PRIMARY KEY (nombreZona)
)
ENGINE = INNODB,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create table `casa_particular`
--
CREATE TABLE casa_particular (
  numero int(11) DEFAULT NULL,
  m2 int(11) DEFAULT NULL,
  nombreZona varchar(255) DEFAULT NULL
)
ENGINE = INNODB,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create index `UK_casa_particular` on table `casa_particular`
--
ALTER TABLE casa_particular
ADD UNIQUE INDEX UK_casa_particular (numero, nombreZona);

--
-- Create index `UK_casa_particular_numero` on table `casa_particular`
--
ALTER TABLE casa_particular
ADD UNIQUE INDEX UK_casa_particular_numero (numero);

--
-- Create foreign key
--
ALTER TABLE casa_particular
ADD CONSTRAINT FK_casa_particular_nombreZona FOREIGN KEY (nombreZona)
REFERENCES zona_urbana (nombreZona) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Create table `bloque_casas`
--
CREATE TABLE bloque_casas (
  dir varchar(255) NOT NULL,
  calle varchar(255) DEFAULT NULL,
  numero int(11) DEFAULT NULL,
  nPisos int(11) DEFAULT NULL,
  nombreZona varchar(255) DEFAULT NULL,
  PRIMARY KEY (dir)
)
ENGINE = INNODB,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create foreign key
--
ALTER TABLE bloque_casas
ADD CONSTRAINT FK_bloque_casas_nombreZona FOREIGN KEY (nombreZona)
REFERENCES zona_urbana (nombreZona) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Create table `piso`
--
CREATE TABLE piso (
  p_p varchar(255) DEFAULT NULL,
  planta varchar(255) DEFAULT NULL,
  puerta varchar(255) DEFAULT NULL,
  m2 int(11) DEFAULT NULL,
  dir varchar(255) DEFAULT NULL
)
ENGINE = INNODB,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create index `UK_piso` on table `piso`
--
ALTER TABLE piso
ADD UNIQUE INDEX UK_piso (p_p, dir);

--
-- Create index `UK_piso_p_p` on table `piso`
--
ALTER TABLE piso
ADD UNIQUE INDEX UK_piso_p_p (p_p);

--
-- Create foreign key
--
ALTER TABLE piso
ADD CONSTRAINT FK_piso_dir FOREIGN KEY (dir)
REFERENCES bloque_casas (dir) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Create table `persona`
--
CREATE TABLE persona (
  dni varchar(255) NOT NULL,
  nombre varchar(255) DEFAULT NULL,
  edad int(11) DEFAULT NULL,
  numero int(11) DEFAULT NULL,
  nombreZona varchar(255) DEFAULT NULL,
  p_p varchar(255) DEFAULT NULL,
  dir varchar(255) DEFAULT NULL,
  PRIMARY KEY (dni)
)
ENGINE = INNODB,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create index `UK_persona` on table `persona`
--
ALTER TABLE persona
ADD UNIQUE INDEX UK_persona (numero, nombreZona);

--
-- Create index `UK_persona2` on table `persona`
--
ALTER TABLE persona
ADD UNIQUE INDEX UK_persona2 (p_p, dir);

--
-- Create foreign key
--
ALTER TABLE persona
ADD CONSTRAINT FK_persona FOREIGN KEY (p_p, dir)
REFERENCES piso (p_p, dir) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Create foreign key
--
ALTER TABLE persona
ADD CONSTRAINT FK_persona2 FOREIGN KEY (numero, nombreZona)
REFERENCES casa_particular (numero, nombreZona) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Create table `posee_piso`
--
CREATE TABLE posee_piso (
  dniPersona varchar(255) NOT NULL,
  p_p varchar(255) DEFAULT NULL,
  dir varchar(255) DEFAULT NULL,
  PRIMARY KEY (dniPersona)
)
ENGINE = INNODB,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create index `UK_posee_piso` on table `posee_piso`
--
ALTER TABLE posee_piso
ADD UNIQUE INDEX UK_posee_piso (p_p, dir);

--
-- Create foreign key
--
ALTER TABLE posee_piso
ADD CONSTRAINT FK_posee_piso FOREIGN KEY (p_p, dir)
REFERENCES piso (p_p, dir) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Create foreign key
--
ALTER TABLE posee_piso
ADD CONSTRAINT FK_posee_piso_dniPersona FOREIGN KEY (dniPersona)
REFERENCES persona (dni) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Create table `posee_casa`
--
CREATE TABLE posee_casa (
  dniPersona varchar(255) NOT NULL,
  numero int(11) DEFAULT NULL,
  nombreZona varchar(255) DEFAULT NULL,
  PRIMARY KEY (dniPersona)
)
ENGINE = INNODB,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create index `UK_posee_casa` on table `posee_casa`
--
ALTER TABLE posee_casa
ADD UNIQUE INDEX UK_posee_casa (numero, nombreZona);

--
-- Create foreign key
--
ALTER TABLE posee_casa
ADD CONSTRAINT FK_posee_casa FOREIGN KEY (numero, nombreZona)
REFERENCES casa_particular (numero, nombreZona) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Create foreign key
--
ALTER TABLE posee_casa
ADD CONSTRAINT FK_posee_casa_dniPersona FOREIGN KEY (dniPersona)
REFERENCES persona (dni) ON DELETE CASCADE ON UPDATE CASCADE;

-- 
-- Dumping data for table zona_urbana
--
-- Table hoja8ejercicio02.zona_urbana does not contain any data (it is empty)

-- 
-- Dumping data for table bloque_casas
--
-- Table hoja8ejercicio02.bloque_casas does not contain any data (it is empty)

-- 
-- Dumping data for table casa_particular
--
-- Table hoja8ejercicio02.casa_particular does not contain any data (it is empty)

-- 
-- Dumping data for table piso
--
-- Table hoja8ejercicio02.piso does not contain any data (it is empty)

-- 
-- Dumping data for table persona
--
-- Table hoja8ejercicio02.persona does not contain any data (it is empty)

-- 
-- Dumping data for table posee_piso
--
-- Table hoja8ejercicio02.posee_piso does not contain any data (it is empty)

-- 
-- Dumping data for table posee_casa
--
-- Table hoja8ejercicio02.posee_casa does not contain any data (it is empty)

-- 
-- Restore previous SQL mode
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Enable foreign keys
-- 
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;