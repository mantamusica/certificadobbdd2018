﻿UPDATE persona p
SET p.dato1 = 1
WHERE LEFT(nombre, 1) = 'c';

UPDATE persona p
SET p.dato2 = DAYOFMONTH(NOW())
WHERE p.dato1 = 1;

SELECT
  *
FROM persona p
WHERE QUARTER(p.fechaNacimiento) = 1;

UPDATE persona p
SET p.dato3 = p.dato1 + p.dato2
WHERE QUARTER(fechaNacimiento) = 1;

SELECT
  *
FROM persona p
WHERE QUARTER(p.fechaNacimiento) = 1;

UPDATE empresa e
SET e.trabajadores = 0;

SELECT
  p.codigoEmpresa,
  COUNT(p.codigoEmpresa) n
FROM empresa e
  JOIN persona p
    ON p.codigoEmpresa = e.codigo
GROUP BY p.codigoEmpresa;

UPDATE empresa e JOIN (SELECT
  p.codigoEmpresa,
  COUNT(p.codigoEmpresa) n
FROM empresa e
  JOIN persona p
    ON p.codigoEmpresa = e.codigo
GROUP BY p.codigoEmpresa) AS c1
  ON c1.codigoEmpresa=e.codigo
  SET e.trabajadores= c1.n;

SELECT * FROM empresa e;