﻿DROP DATABASE IF EXISTS empresas;

CREATE DATABASE IF NOT EXISTS empresas;

USE empresas;

SET NAMES 'utf8';

DROP TABLE IF EXISTS persona;

DROP TABLE IF EXISTS empresa;



CREATE TABLE empresa (
  codigo int(11) NOT NULL AUTO_INCREMENT,
  nombre char(20) DEFAULT NULL,
  poblacion varchar(255) DEFAULT NULL,
  cp int(11) DEFAULT NULL,
  trabajadores int(11) DEFAULT NULL,
  PRIMARY KEY (codigo)
);

CREATE TABLE persona (
  id int(11) NOT NULL AUTO_INCREMENT,
  nombre varchar(255) DEFAULT NULL,
  apellidos varchar(255) DEFAULT NULL,
  poblacion varchar(255) DEFAULT NULL,
  fechaNacimiento date DEFAULT NULL,
  fechaTrabaja date DEFAULT NULL,
  dato1 int(11) DEFAULT NULL,
  dato2 int(11) DEFAULT NULL,
  dato3 int(11) DEFAULT NULL,
  codigoEmpresa int(11) DEFAULT NULL,
  PRIMARY KEY (id,codigoEmpresa),
  CONSTRAINT FK_personas_codigoEmpresa FOREIGN KEY (codigoEmpresa)
  REFERENCES empresa (codigo) ON DELETE CASCADE ON UPDATE CASCADE
);

