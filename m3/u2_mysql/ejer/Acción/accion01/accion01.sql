﻿-- Realizar los siguientes cálculos en los campos derivados;

-- a. Venden.total = producto.precio*venden.cantidad

UPDATE venden v
INNER JOIN productos p
  ON v.producto = p.id
SET v.total = p.precio * v.cantidad;

SELECT
  *
FROM venden v;

-- b. nombreCompleto = nombre + apellidos;

UPDATE cliente c
SET c.nombreCompleto = CONCAT(c.nombre, ' ', c.apellidos);

SELECT
  *
FROM cliente c;

-- Realizar los siguientes cálculos en los campos derivados

-- a. Venden.total = producto.precio*venden.cantidad*(1-cliente.descuento)

UPDATE venden v
INNER JOIN productos p
  ON v.producto = p.id
INNER JOIN cliente c
  ON v.cliente = c.id
SET v.total = p.precio * v.cantidad * (1 - (c.descuento / 100));

SELECT
  *
FROM venden v;

-- b. Productos.cantidad=suma de todos los productos vendidos

CREATE OR REPLACE VIEW productosVendidos
AS
SELECT
  v.producto id,
  SUM(v.cantidad) total
FROM venden v
GROUP BY v.producto;

UPDATE productos p
INNER JOIN (SELECT
    *
  FROM productosVendidos) pV
  ON p.id = pV.id
SET p.cantidad = pV.total;

SELECT
  *
FROM productos p;

-- c. Cliente.cantidad=suma de todos los productos comprados por ese cliente

CREATE OR REPLACE VIEW productosComprados
AS
SELECT
  v.cliente id,
  SUM(v.cantidad) total
FROM venden v
GROUP BY v.cliente;

UPDATE cliente c
INNER JOIN (SELECT
    *
  FROM productosComprados) pC
  ON c.id = pC.id
SET c.cantidad = pC.total;

SELECT
  *
FROM cliente c;

-- Realizar los siguientes cambios en la base de datos

-- a. Añadir un campo en la tabla productos denominado precioTotal de tipo Float

ALTER TABLE productos ADD COLUMN IF NOT EXISTS precioTotal float;

-- b. Añadir un campo en la tabla clientes denominado precioTotal de tipo Float

ALTER TABLE cliente ADD COLUMN IF NOT EXISTS precioTotal float;

-- Realizar los siguientes cálculos en los campos derivados

  -- a. productos.precioTotal = suma de todos los precios de ese producto vendidos

  CREATE OR REPLACE VIEW sumaTotalProductos AS
  SELECT v.producto,SUM(v.total)sumaTotal FROM venden v GROUP BY v.producto;

  UPDATE productos p 
    INNER JOIN sumaTotalProductos tp ON p.id=tp.producto
    SET p.precioTotal = tp.sumaTotal;

  SELECT * FROM productos p;

  -- b. cliente.precioTotal = suma de todos los precios de ese producto comprados

  CREATE OR REPLACE VIEW sumaTotalClientes AS
  SELECT v.cliente,SUM(v.total)sumaTotal FROM venden v GROUP BY v.cliente;

  UPDATE cliente c 
    INNER JOIN sumaTotalClientes tp ON c.id=tp.cliente
    SET c.precioTotal = tp.sumaTotal;

  SELECT * FROM cliente c;