﻿-- consulta 1

CREATE OR REPLACE VIEW verduras AS
SELECT p.producto FROM productos p WHERE p.rubro = 'verduras';

UPDATE productos p JOIN verduras v USING(producto)
  SET p.DESCUENTO1=p.importe_base * 0.1;

 -- con if

UPDATE productos p SET p.DESCUENTO1 = IF(p.rubro = 'verduras',p.importe_base * 0.1,0);

 -- otro tipo

UPDATE productos p SET p.DESCUENTO1 = p.importe_base*IF(p.rubro = 'verduras',0.1,0);

-- consulta 2

CREATE OR REPLACE VIEW atados AS
SELECT p.producto FROM productos p WHERE p.`u/medida` = 'atado';

UPDATE productos p SET p.DESCUENTO2=p.importe_base * 0.05;
UPDATE productos p JOIN atados a USING(producto)
  SET p.DESCUENTO2=p.importe_base * 0.20;

 -- con if

UPDATE productos p SET p.DESCUENTO1 = IF(p.`u/medida` = 'atado',p.importe_base * 0.2,0);

-- consulta 3

CREATE OR REPLACE VIEW frutas AS
SELECT p.producto FROM productos p WHERE p.rubro = 'frutas' AND p.importe_base >15;

UPDATE productos p JOIN frutas f USING(producto)
  SET p.DESCUENTO3=p.importe_base * 0.20;

-- consulta 4

CREATE OR REPLACE VIEW granjas AS
SELECT p.producto FROM productos p WHERE p.granja = 'primavera' OR p.granja = 'litoral';

UPDATE productos p SET p.DESCUENTO4=p.importe_base * 0.25;
UPDATE productos p JOIN granjas g USING(producto)
  SET p.DESCUENTO4=p.importe_base * 0.50;

-- consulta 5

CREATE OR REPLACE VIEW granjasProductos AS
SELECT p.producto FROM productos p WHERE (p.granja = 'la garota' OR p.granja = 'la pocha') AND (p.rubro = 'frutas' OR p.rubro = 'verduras');

UPDATE productos p JOIN granjasProductos g USING(producto)
  SET p.AUMENTO1=p.importe_base * 0.10;

-- consulta 6

CREATE OR REPLACE VIEW unidad AS
SELECT * FROM productos p WHERE p.`u/medida`='unidad';

CREATE OR REPLACE VIEW kilo AS
SELECT * FROM productos p WHERE p.`u/medida`='kilo';

UPDATE productos p JOIN atados a USING(producto)
  SET p.PRESENTACION=1;
UPDATE productos p JOIN unidad u USING(producto)
  SET p.PRESENTACION=2;
UPDATE productos p JOIN kilo k USING(producto)
  SET p.PRESENTACION=3;

-- consulta 7

CREATE OR REPLACE VIEW importeMenor10 AS
SELECT * FROM productos p WHERE p.importe_base<10;

CREATE OR REPLACE VIEW importeEntre10Y20 AS
SELECT * FROM productos p WHERE p.importe_base BETWEEN 10 AND 20;

CREATE OR REPLACE VIEW importeMayor20 AS
SELECT * FROM productos p WHERE p.importe_base>20;

UPDATE productos p JOIN importeMenor10 a USING(producto)
  SET p.CATEGORIA='A';
UPDATE productos p JOIN importeEntre10Y20 u USING(producto)
  SET p.CATEGORIA='B';
UPDATE productos p JOIN importeMayor20 k USING(producto)
  SET p.CATEGORIA='C';

-- consulta 8

CREATE OR REPLACE VIEW frutaLitoral AS
SELECT * FROM productos p WHERE p.granja='litoral' AND p.rubro='frutas';

CREATE OR REPLACE VIEW verdurasCeibal AS
SELECT * FROM productos p WHERE p.granja='el ceibal' AND p.rubro='verduras';

CREATE OR REPLACE VIEW semillasCanuto AS
SELECT * FROM productos p WHERE p.granja='el canuto' AND p.rubro='semillas';

UPDATE productos p JOIN frutaLitoral a USING(producto)
  SET p.AUMENTO2=p.importe_base*0.1;
UPDATE productos p JOIN verdurasCeibal u USING(producto)
  SET p.AUMENTO2=p.importe_base*0.15;
UPDATE productos p JOIN semillasCanuto k USING(producto)
  SET p.AUMENTO2=p.importe_base*0.2;



