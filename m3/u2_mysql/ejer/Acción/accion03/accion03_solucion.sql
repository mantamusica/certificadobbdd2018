﻿

/* Liquidación de haber para el mes en curso. SALARIO BASICO dependiendo si jornalero o efectivo y el 
  importe correspondiente a este por hora o dia. Los importes correspondintes se localizan en la nueva
  tabla creada. */

ALTER TABLE empleados DROP COLUMN IF EXISTS salarioBasico;
ALTER TABLE empleados ADD COLUMN IF NOT EXISTS salarioBasico float;

UPDATE empleados e
INNER JOIN importes i
  ON e.Tipo_trabajo = i.tipo
SET e.salarioBasico =
IF(e.Tipo_trabajo = 'Jornalero', e.Horas_trabajadas * i.valor,
IF(e.Tipo_trabajo = 'Efectivo', e.Dias_trabajdos * i.valor, 0));



/* Incrementamos 15% salario basico para chofer y azafata, el resto un 5%. Esta bonificacion solo se realizará cuando
  los empleados hallan trabajado no menos de 20 dias o 200 horas respectivamente. Este importe deberá mostrarse en la
  columna PREMIOS. */

ALTER TABLE empleados DROP COLUMN IF EXISTS premios;
ALTER TABLE empleados ADD COLUMN IF NOT EXISTS premios float;

UPDATE empleados e
SET e.premios = e.salarioBasico *
CASE
  WHEN (e.rubro = 'CHOFER' OR e.rubro = 'AZAFATA') AND (e.Horas_trabajadas >= 200 OR e.Dias_trabajdos >= 20) THEN 0.15
  WHEN (NOT(e.rubro = 'CHOFER' OR e.rubro = 'AZAFATA') AND (e.Horas_trabajadas >= 200 OR e.Dias_trabajdos >= 20)) THEN 0.05
 ELSE 0 END;

/* Calcular el sueldo nominal en base al salario basico y los premios obtenidos. Este campo le teneis que crear en la cuenta tabla.*/

ALTER TABLE empleados DROP COLUMN IF EXISTS sueldoNominal;
ALTER TABLE empleados ADD COLUMN IF NOT EXISTS sueldoNominal float;

UPDATE empleados e
SET e.sueldoNominal =
e.salarioBasico + e.premios;

/* En base a sueldo nominal se deberán calcular los descuentos respectivos, los que corresponden a BPS 13%, IRP 3% para sueldos nominales
  menos a 4 SMN, 6% para sueldos nominales entre 4 y 10 SMN(inclusive), y 9% para sueldos nominales mayores a 10 SMN (SMN:salario mínimo nacional, que 
  para el mes en curso corresponde a 1.160). Crear ambos campos en la tabla.*/

ALTER TABLE empleados DROP COLUMN IF EXISTS BPS;
ALTER TABLE empleados ADD COLUMN IF NOT EXISTS BPS float;

ALTER TABLE empleados DROP COLUMN IF EXISTS IRP;
ALTER TABLE empleados ADD COLUMN IF NOT EXISTS IRP float;

UPDATE empleados e
SET e.BPS = e.sueldoNominal * 0.13;

UPDATE empleados e
SET e.IRP = e.sueldoNominal *
CASE WHEN sueldoNominal < 4 * 1160 THEN 0.03 WHEN sueldoNominal > 10 * 1160 THEN 0.09 ELSE 0.06 END;

/* Calcular todos los descuentos en la columna SUBTOTAL DESCUENTOS.*/

ALTER TABLE empleados DROP COLUMN IF EXISTS subtotalDescuentos;
ALTER TABLE empleados ADD COLUMN IF NOT EXISTS subtotalDescuentos float;

UPDATE empleados e
SET e.subtotalDescuentos = e.BPS + e.IRP;

/* Calcular el sueldo líquido en base al nominal y los descuentos correspondientes.*/

ALTER TABLE empleados DROP COLUMN IF EXISTS sueldoLiquido;
ALTER TABLE empleados ADD COLUMN IF NOT EXISTS sueldoLiquido float;

UPDATE empleados e
SET e.sueldoLiquido = e.salarioBasico + e.premios - e.subtotalDescuentos;

/* calcular una nueva columna v-Transporte, de forma tal de registrar el importe correspondiente a vales de transporte
  , que la empresa dará a sus empleados. dicho importe se calculará de la siguiente manera:
  
  1. para los empleados mayores de 40 años que no vivenn en los barrios de cordon y centro, 200
  2. para los mayores de 40 años que viven en estos barrios, 150
  3. mientras que para los menores de 40 años*/

ALTER TABLE empleados DROP COLUMN IF EXISTS vTransporte;
ALTER TABLE empleados ADD COLUMN IF NOT EXISTS vTransporte float;

UPDATE empleados e
  SET e.vTransporte = 
  CASE
  WHEN (e.Barrio = 'Cordon' OR e.Barrio = 'Centro') AND (DATEDIFF(NOW(),e.`Fecha-nac`)/365 > 40) THEN 150
  WHEN (NOT(e.Barrio != 'Cordon' OR e.Barrio != 'Centro')) AND (DATEDIFF(NOW(),e.`Fecha-nac`)/365 > 40) THEN 200
  ELSE 100
  END;

/* Agregar vAlimentacion, de forma tal de registrar el importe correspondiente a vales de alimentacion, que la empresa
  dará a sus empleados. Dicho importe se calculará de la siguiente manera:
  1. para los empleados con ingresos líquidos menores a los 5000, 300.
  2. para los que poseen ingresos entre 5000 incluido y 10000 inclusive,
    2.1. jornaleros 200.
    2.2. efectivos 100.
  3. mientras que los que poseen ingresos superiores a los 10000, no se acreditarán vales de alimentación.*/

ALTER TABLE empleados DROP COLUMN IF EXISTS vAlimentacion;
ALTER TABLE empleados ADD COLUMN IF NOT EXISTS vAlimentacion float;

UPDATE empleados e
  SET vAlimentacion = 
  CASE
  WHEN e.salarioBasico < 5000 THEN 300
  WHEN (e.salarioBasico BETWEEN 5000 AND 10000) AND (e.Tipo_trabajo = 'Jornalero') THEN 200
  WHEN (e.salarioBasico BETWEEN 5000 AND 10000) AND (e.Tipo_trabajo = 'Efectivo') THEN 100
  ELSE 0
  END;
