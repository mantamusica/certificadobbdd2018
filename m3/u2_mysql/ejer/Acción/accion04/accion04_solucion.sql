﻿-- bicis.kms

ALTER TABLE bicis DROP COLUMN IF EXISTS kms;
ALTER TABLE bicis ADD COLUMN IF NOT EXISTS kms float DEFAULT 0;

CREATE OR REPLACE VIEW sumaKms
AS
SELECT
  bici,
  SUM(DISTINCT kms) total
FROM alquileres
GROUP BY bici;

UPDATE bicis b
JOIN sumaKms c1
  ON b.bicis_id = c1.bici
SET b.kms = c1.total;

-- bicis.años

ALTER TABLE bicis DROP COLUMN IF EXISTS años;
ALTER TABLE bicis ADD COLUMN IF NOT EXISTS años float DEFAULT 0;

CREATE OR REPLACE VIEW diferenciaAnos
AS
SELECT
  b.bicis_id,
  TRUNCATE(DATEDIFF(NOW(), b.fechaCompra) / 365, 0) fecha
FROM bicis b;

UPDATE bicis b
JOIN diferenciaAnos c1
  ON b.bicis_id = c1.bicis_id
SET b.años = c1.fecha;

-- bicis.averias

ALTER TABLE bicis DROP COLUMN IF EXISTS averias;
ALTER TABLE bicis ADD COLUMN IF NOT EXISTS averias float DEFAULT 0;

CREATE OR REPLACE VIEW contarAverias
AS
SELECT
  cod,
  COUNT(*) numero
FROM averias
GROUP BY bici;

UPDATE bicis
JOIN contarAverias c1
  ON c1.cod = bicis_id
SET averias = c1.numero;

-- bicis.alquileres

ALTER TABLE bicis DROP COLUMN IF EXISTS alquileres;
ALTER TABLE bicis ADD COLUMN IF NOT EXISTS alquileres float DEFAULT 0;

CREATE OR REPLACE VIEW contarAlquileres
AS
SELECT
  bici,
  COUNT(*) numero
FROM alquileres
GROUP BY bici;

UPDATE bicis
JOIN contarAlquileres c1
  ON c1.bici = bicis_id
SET alquileres = c1.numero;

-- bicis.gastos

ALTER TABLE bicis DROP COLUMN IF EXISTS gastos;
ALTER TABLE bicis ADD COLUMN IF NOT EXISTS gastos float DEFAULT 0;

UPDATE bicis b
JOIN averias a ON b.bicis_id = a.bici
SET gastos = a.coste*b.averias;

-- bicis.beneficios

ALTER TABLE bicis DROP COLUMN IF EXISTS beneficios;
ALTER TABLE bicis ADD COLUMN IF NOT EXISTS beneficios float DEFAULT 0;

CREATE OR REPLACE VIEW sumaBeneficios
AS
SELECT
  bicis_id,
  SUM(precio) total
FROM bicis b
GROUP BY b.bicis_id;

UPDATE bicis b
JOIN sumaBeneficios c1
  ON b.bicis_id = c1.bicis_id
SET b.beneficios = c1.total;

-- alquileres.clientes

CREATE OR REPLACE VIEW contarAlquileresCliente
AS
SELECT
  cliente,
  COUNT(*) numero
FROM alquileres
GROUP BY cliente;

UPDATE clientes
INNER JOIN contarAlquileresCliente ac
  ON ac.cliente = codigo
SET alquileres = ac.numero;

-- alquileres descuento

UPDATE alquileres a
INNER JOIN clientes c
  ON a.cliente = c.codigo
SET a.descuento = a.precio_total - c.descuento;

-- alquileres.precio_total

CREATE OR REPLACE VIEW precioBici
AS
SELECT
  b.bicis_id,
  SUM(precio) precio
FROM bicis b
GROUP BY b.bicis_id;

SELECT
  *
FROM alquileres a
  INNER JOIN precioBici b
    ON a.bici = b.bicis_id;

UPDATE alquileres a
INNER JOIN precioBici b
  ON a.bici = b.bicis_id
SET a.precio_total = b.precio - ((b.precio * a.descuento) / 100);



