﻿
-- Consultas Selección 

/* 
  Consulta 1 Listar las edades de los ciclistas(sin repetidos)
*/

SELECT DISTINCT
  nombre,
  edad
FROM ciclista;

/* 
  Consulta 2 Listar las edades de los ciclistas de Artiach 
*/

SELECT
  edad
FROM ciclista
WHERE nomequipo = 'Artiach';

/* 
  Consulta 3 Listar las edades de los ciclistas de Artiach o de Amore Vita
*/

SELECT
  edad
FROM ciclista
WHERE nomequipo IN ('Artiach', 'Amore Vita');

SELECT
  edad
FROM ciclista
WHERE nomequipo = 'Artiach'
OR nomequipo = 'Amore Vita';

/* 
  Consulta 4 Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor q 30
*/

SELECT
  dorsal
FROM ciclista
WHERE edad < 25
OR edad > 30;

SELECT
  dorsal
FROM ciclista
WHERE edad < 25
UNION
SELECT
  dorsal
FROM ciclista
WHERE edad > 30;
/* 
  Consulta 5 Listar los dorsales de los ciclistas cuya edad este entre 28 y 32 y además que solo sean de Banesto 
*/

SELECT
  dorsal
FROM ciclista
WHERE edad BETWEEN 28 AND 32
AND nomequipo = 'Banesto';

SELECT
  c1.dorsal
FROM (SELECT
    c.dorsal
  FROM ciclista c
  WHERE c.edad BETWEEN 28 AND 32) AS c1
  JOIN (SELECT
      c.dorsal
    FROM ciclista c
    WHERE c.nomequipo = 'Banesto') AS c2
    ON c1.dorsal = c2.dorsal;
/* 
  Consulta 6 Indicame el nombre de los ciclistas que el numero de caracteres del nombre sea mayor q 8
*/

SELECT
  nombre
FROM ciclista
WHERE CHAR_LENGTH(nombre) > 8;

/* 
  Consulta 7 Listame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayuscula que debe 
  mostrar el nombre en mayuscula   
*/

SELECT
  nombre,
  dorsal,
  UPPER(nombre) mayúscula
FROM ciclista;
/* 
  Consulta 8 Listar todos los ciclistas que han llevado el maillot MGE (amarillo) en alguna etapa
*/

SELECT DISTINCT
  c.nombre
FROM lleva l
  INNER JOIN ciclista c
    ON c.dorsal = l.dorsal
WHERE l.código = 'MGE';

SELECT DISTINCT
  l.dorsal
FROM lleva l
WHERE l.código = 'MGE';

/* 
  Consulta 9 Listar  el nombre de los puertos cuya altura sea mayor que 1500
*/

SELECT
  nompuerto
FROM puerto
WHERE altura > 1500;
/* 
  Consulta 10 Listar el dorsal de los ciclistas que hayn ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura este entre 
  1800 y 3000
*/

SELECT
  dorsal
FROM puerto
WHERE pendiente > 8
OR altura BETWEEN 1800 AND 3000;

SELECT
  dorsal
FROM puerto
WHERE pendiente > 8
UNION
SELECT
  dorsal
FROM puerto
WHERE altura BETWEEN 1800 AND 3000;
/* 
  Consulta 11 Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor q 8 cuya altura este entre 1800 y 3000
*/

SELECT
  dorsal
FROM puerto
WHERE pendiente > 8
AND altura BETWEEN 1800 AND 3000;