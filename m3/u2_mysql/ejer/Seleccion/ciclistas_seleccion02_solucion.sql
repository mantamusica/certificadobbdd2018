﻿
-- Consultas Selección 

/* 
  Consulta 1 Número de ciclistas que hay
*/

SELECT
  COUNT(*) AS nCiclistas
FROM ciclista;

/* 
  Consulta 2 Núemro de ciclistas que hay en el Banesto 
*/

SELECT
  COUNT(*)
FROM ciclista
WHERE nomequipo = 'Banesto';

/* 
  Consulta 3 La edad media de los ciclistas
*/

SELECT
  AVG(edad)
FROM ciclista;

/* 
  Consulta 4 La edad media de los cicliestas de Banesto
*/

SELECT
  AVG(edad)
FROM ciclista
WHERE nomequipo = 'Banesto';

/* 
  Consulta 5 La edad media de los ciclistas de cada equipo 
*/

SELECT
  nomequipo,
  AVG(edad)
FROM ciclista
GROUP BY nomequipo;

/* 
  Consulta 6 el numero de ciclistas por equipo
*/

SELECT
  nomequipo,
  COUNT(*)
FROM ciclista
GROUP BY nomequipo;

/* 
  Consulta 7 el numero total de puertos   
*/

SELECT
  COUNT(*)
FROM puerto;

/* 
  Consulta 8 el numero total de puertos mayores de 1500
*/

SELECT
  COUNT(*)
FROM puerto
WHERE altura > 1500;

/* 
  Consulta 9 listar el nombre de los equipos que tengan mas de 4 ciclistas
*/

SELECT
  nomequipo
FROM ciclista
  GROUP BY nomequipo
HAVING COUNT(nombre) > 4;

/* 
  Consulta 10 listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad este entre 28 y 32
*/

SELECT
  nomequipo
FROM ciclista
WHERE edad BETWEEN 28 AND 32
    GROUP BY nomequipo
HAVING COUNT(nombre) > 4;

/* 
  Consulta 11 indicame el numero de etapas que ha ganado cada uno de los ciclistas
*/

SELECT
  dorsal,
  COUNT(numetapa)
FROM etapa
GROUP BY dorsal;

/* 
  Consulta 12 indicame el dorsal de los ciclistas que hayan ganado más de 1 etapa
*/

SELECT
  dorsal,
  COUNT(numetapa) etapas
FROM etapa
GROUP BY dorsal
HAVING etapas > 1;

SELECT
  dorsal
FROM etapa
GROUP BY dorsal
HAVING COUNT(numetapa) > 1;
  