﻿
-- Consultas Selección 

/* 
  Consulta 1 Listar edades de todos los ciclistas de Banesto
*/

SELECT
  DISTINCT edad
FROM ciclista
WHERE nomequipo = 'Banesto';

/* 
  Consulta 2 Listar las edades de los ciclistas que son de Banesto y Navigare. 
*/

SELECT
  edad
FROM ciclista
WHERE nomequipo = 'Banesto'
OR nomequipo = 'Navigare';

SELECT
  edad
FROM ciclista
WHERE nomequipo IN('Banesto', 'Navigare');

/* 
  Consulta 3 el dorsal de los ciclistas que son de Banesto y cuya edad esta entre 25 y 32
*/

SELECT
  dorsal
FROM ciclista
WHERE nomequipo = 'Banesto'
AND edad BETWEEN 25 AND 32;

/* 
  Consulta 4 el dorsal de los ciclistas que son de Banesto o cuya edad esta entre 25 y 32
*/

SELECT
  dorsal
FROM ciclista
WHERE nomequipo = 'Banesto'
OR edad BETWEEN 25 AND 32;

/* 
  Consulta 5 Listar la inicial del equipo de los ciclistas cuyo nombre comience por R 
*/

SELECT DISTINCT LEFT(c.nomequipo,1)
FROM ciclista c
WHERE LEFT(c.nombre,1)='R';

/* 
  Consulta 6 Listar el codigo de las etapas que su salida y llegada sea en la misma poblacion
*/

SELECT
  numetapa
FROM etapa
WHERE salida = llegada;

/* 
  Consulta 7 Listar el codigo de las etapas que su salida y llegada no sean en la misma poblacion y
  que conozcamos el dorsal del ciclista que ha ganado la etapa.   
*/

SELECT
  numetapa
FROM etapa
WHERE salida <> llegada
AND dorsal IS NOT NULL;

/* 
  Consulta 8 Listar el nombre de los puertos cuya altura este entre 1000 y 2000
  o que la altura sea mayor 2400
*/

SELECT
  nompuerto
FROM puerto
WHERE altura BETWEEN 1000 AND 2000
OR altura > 2400;

/* 
  Consulta 9 Listar el dorsal de los ciclistas que hayan ganado los puertos cuya altura este
  entre 1000 y 2000 o que la altura sea mayor que 2400
*/

SELECT
  dorsal
FROM puerto
WHERE altura BETWEEN 1000 AND 2000
OR altura > 2400;

/* 
  Consulta 10 Listar el número de ciclistas que hayan ganado alguna etapa
*/

SELECT 
  COUNT(DISTINCT dorsal)
FROM etapa;

/* 
  Consulta 11 Listar el numero de etapas que tengan puerto
*/

SELECT DISTINCT
  numetapa
FROM puerto;

/* 
  Consulta 12 Listar el número de ciclistas que hayan ganado algún puerto
*/

SELECT DISTINCT
  dorsal
FROM puerto;

/* 
  Consulta 13 Listar el código de la etapa con el número de puertos que tiene
*/

SELECT DISTINCT
  numetapa,
  COUNT(nompuerto)
FROM puerto
  GROUP BY numetapa;

/* 
  Consulta 14 Altura media de los puertos
*/

SELECT
  AVG(altura)
FROM puerto;

/* 
  Consulta 15 Indicar el codigo de etapa cuya altura media de sus puertos esté por encima de 1500
*/

SELECT
  numetapa,
  AVG(altura)
FROM puerto
GROUP BY numetapa
HAVING AVG(altura) > 1500;

/* 
  Consulta 16 Indicar el número de etapas que cumplen la condición anterior
*/

SELECT
  COUNT(numetapa)
FROM puerto
GROUP BY numetapa
HAVING AVG(altura) > 1500;

/* 
  Consulta 17 Listar el dorsal del ciclista con el número de veces que ha llevado algún maillot
*/

SELECT
  dorsal,
  COUNT(código)
FROM lleva
GROUP BY dorsal;

/* 
  Consulta 18 Listar el dorsal con el código del maillot y cuantas veces ese ciclista
  ha llevado ese maillot
*/

SELECT
  dorsal,
  código,
  COUNT(código)
FROM lleva
GROUP BY dorsal,código;

/* 
  Consulta 19 Listar el código de la etapa , el ciclista, el numero del maillot que ese ciclista ha
  llevado en cada etapa
*/

SELECT
  numetapa,
  dorsal, 
  COUNT(*)
FROM lleva
GROUP BY dorsal,numetapa;




  