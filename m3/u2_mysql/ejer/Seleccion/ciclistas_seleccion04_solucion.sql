﻿
-- Consultas Selección 

/* 
  Consulta 1 Nombre y edad de los ciclistas que han ganado etapas
*/

SELECT DISTINCT
  c.nombre,
  c.edad
FROM ciclista c
  INNER JOIN etapa e
    ON c.dorsal = e.dorsal;


/* 
  Consulta 2 Nombre y edad de los ciclistas que han ganado puertos. 
*/

SELECT DISTINCT
  c.nombre,
  c.edad
FROM ciclista c
  INNER JOIN puerto p
    ON c.dorsal = p.dorsal;


/* 
  Consulta 3 Nombre y edad de los ciclistas que han ganado etapas y puertos.
*/



/* 
  Consulta 4 Listar el director de los equipos que tengan ciclistas que hayan ganado algun etapa
*/

SELECT DISTINCT
  e.director
FROM equipo e
  INNER JOIN ciclista c
    ON e.nomequipo = c.nomequipo
  INNER JOIN etapa e1
    ON c.dorsal = e1.dorsal;

/* 
  Consulta 5 Dorsal y nombre de los ciclistas que hayan llevado algún maillot 
*/

SELECT DISTINCT
  c.dorsal,
  c.nombre
FROM ciclista c
  INNER JOIN lleva l
    ON c.dorsal = l.dorsal;

/* 
  Consulta 6 Dorsal y nombre de los ciclistas que hayan llevado maillot amarillo
*/

SELECT DISTINCT
  c.dorsal,
  c.nombre
FROM ciclista c
  INNER JOIN lleva l
    ON c.dorsal = l.dorsal
WHERE l.código = 'MGE';

-- c1
CREATE OR REPLACE VIEW C1
AS
SELECT
  m.código
FROM maillot m
WHERE m.código = 'amarillo';

-- c2
CREATE OR REPLACE VIEW C2
AS
SELECT DISTINCT
  l.dorsal
FROM lleva l
WHERE l.código IN (SELECT
    *
  FROM C1);

-- c2p
CREATE OR REPLACE VIEW C2p
AS
SELECT DISTINCT
  l.dorsal
FROM lleva l
JOIN C1 c USING(código);


SELECT DISTINCT
  c.dorsal,
  c.nombre
FROM ciclista c
  INNER JOIN c2 USING (dorsal);


DROP VIEW C1;
  DROP VIEW C2;
  DROP VIEW C2P;
/* 
  Consulta 7 Dorsal de los ciclistas que hayn llevado algún maillot y que ha ganado etapas.   
*/

SELECT DISTINCT
  c.dorsal
FROM ciclista c
  INNER JOIN lleva l
    ON c.dorsal = l.dorsal
  INNER JOIN etapa e
    ON c.dorsal = e.dorsal;

-- c1
CREATE OR REPLACE VIEW ciclistaMaillot AS
SELECT l.dorsal FROM lleva l;

-- c2
CREATE OR REPLACE VIEW ciclistaEtapa AS
SELECT e.dorsal FROM etapa e;

SELECT DISTINCT m.dorsal FROM ciclistaMaillot m NATURAL JOIN ciclistaEtapa e;

DROP VIEW ciclistaEtapa, ciclistaMaillot;
/* 
  Consulta x Ciclistas que hayan ganado puertos de mas de 1200 m.   
*/

SELECT DISTINCT
  *
FROM ciclista c
  INNER JOIN puerto p
    ON c.dorsal = p.dorsal
WHERE p.altura > 1200;

-- otra manera más rápida

SELECT DISTINCT
  p.dorsal
FROM puerto p
WHERE p.altura > 1200;

SELECT
  c.*
FROM (SELECT DISTINCT
    p.dorsal
  FROM puerto p
  WHERE p.altura > 1200) c1
  INNER JOIN ciclista c
    ON c.dorsal = c1.dorsal;

/* Vamos a usar vistas*/

CREATE OR REPLACE VIEW consulta2C1
AS
SELECT DISTINCT
  p.dorsal
FROM puerto p
WHERE p.altura > 1200;

CREATE OR REPLACE VIEW consulta2C2
AS
SELECT
  c.*
FROM consulta2C1 c1
  INNER JOIN ciclista c
    ON c.dorsal = c1.dorsal;




/* 
  Consulta 8 Indicar los km de las etapas que hayn ganado ciclistas de Banesto y que tengan puertos
*/

SELECT
  SUM(DISTINCT e.kms) kmsTotales
FROM etapa e
  INNER JOIN ciclista c
    ON e.dorsal = c.dorsal
  INNER JOIN puerto p
    ON e.numetapa = p.numetapa
WHERE c.nomequipo = 'Banesto';

-- Optimizar consulta


/* 
  Consulta 9 Indicar el número de etapa de las etapas que tengan puertos
*/

SELECT
DISTINCT
  COUNT(e.numetapa)
FROM etapa e
  INNER JOIN puerto p
    ON e.numetapa = p.numetapa;


/* 
  Consulta 10 Listar el número de los ciclistas que hayan ganado alguna etapa con puerto
*/

SELECT
  COUNT(c.dorsal)
FROM ciclista c
  INNER JOIN etapa e
    ON c.dorsal = e.dorsal
  INNER JOIN puerto p
    ON c.dorsal = p.dorsal;

/* 
  Consulta 11 Indicar el nombre de los puertos que hayan sido ganados por ciclistas de Banesto
*/
SELECT
  nompuerto
FROM puerto p
  INNER JOIN ciclista c
    ON p.dorsal = c.dorsal
WHERE c.nomequipo = 'Banesto';

-- optimizacion



/* 
  Consulta 12 Listar el número de etapas que tengan puerto que hayan sido ganados por ciclistas
  de Banesto con más de 200 km
*/

SELECT
  e.numetapa
FROM etapa e
  INNER JOIN ciclista c
    ON e.dorsal = c.dorsal
WHERE c.nomequipo = 'Banesto'
AND e.kms > 200;
  

  