﻿
-- Consultas Selección 

/* 
  Consulta 1 Nombre y edad de los ciclistas que no han ganado etapas
*/

SELECT DISTINCT
  c.nombre,
  c.edad
FROM ciclista c
  LEFT JOIN etapa e
    ON c.dorsal = e.dorsal
WHERE e.dorsal IS NULL;


/* 
  Consulta 2 Nombre y edad de los ciclistas que no han ganado puertos. 
*/

SELECT DISTINCT
  c.nombre,
  c.edad
FROM ciclista c
  LEFT JOIN puerto p
    ON c.dorsal = p.dorsal
WHERE p.dorsal IS NULL;


/* 
  Consulta 3 Listar el director de los equipos que tengan ciclistas que no hayan ganado ninguna etapa.
*/

CREATE OR REPLACE VIEW seleccion05consulta3c1 AS
SELECT DISTINCT
  c.nomequipo
FROM ciclista c
  LEFT JOIN puerto p
    ON c.dorsal = p.dorsal
WHERE p.dorsal IS NULL;

SELECT DISTINCT
  e.director
FROM equipo e
WHERE e.nomequipo
IN (SELECT * FROM  seleccion05consulta3c1);

/* 
  Consulta 4 Dorsal y nombre de los ciclistas que no hayan ganado ningún maillot
*/

SELECT DISTINCT
  c.nombre,
  c.edad
FROM ciclista c
  LEFT JOIN lleva ll
    ON c.dorsal = ll.dorsal
WHERE ll.dorsal IS NULL;

/* 
  Consulta 5 Dorsal y nombre de los ciclistas que no hayan llevado maillot amarillo nunca 
*/

SELECT DISTINCT
  c.nombre,
  c.edad
FROM ciclista c
  LEFT JOIN lleva ll
    ON c.dorsal = ll.dorsal
WHERE ll.código = 'MGE' IS NULL;

/* 
  Consulta 6 Indicar el número de etapa que no tengan puertos.   
*/

SELECT DISTINCT
  e.numetapa
FROM etapa e
  LEFT JOIN puerto p
    ON p.numetapa = e.numetapa
WHERE p.numetapa IS NULL;


/* 
  Consulta 7 Indicar la distancia media de las etapas que no tengan puertos
*/

SELECT
  AVG(e.kms)
FROM etapa e
WHERE e.numetapa IN (SELECT DISTINCT
    e.numetapa
  FROM etapa e
    LEFT JOIN puerto p
      ON p.numetapa = e.numetapa
  WHERE p.numetapa IS NULL);

/* 
  Consulta 8 Listar el número de ciclistas que no hayan ganado alguna etapa
*/

SELECT DISTINCT
  COUNT(*)
FROM ciclista c
  LEFT JOIN (SELECT dorsal FROM etapa) C1
    ON c.dorsal = C1.dorsal
WHERE C1.dorsal IS NULL;


/* 
  Consulta 9 Listar el dorsal de los ciclistas que hayan ganado alguna etapa que no tenga puerto
*/

SELECT DISTINCT e.dorsal FROM etapa e
WHERE e.numetapa IN (SELECT DISTINCT
  e.numetapa
FROM etapa e
  LEFT JOIN puerto p
    ON p.numetapa = e.numetapa
WHERE p.numetapa IS NULL);

/* 
  Consulta 10 Listar el dorsal de los ciclistas que hayan ganado únicamente etapas que no tengan puertos
*/

SELECT
  COUNT(c.dorsal)
FROM ciclista c
  INNER JOIN etapa e
    ON c.dorsal = e.dorsal
  INNER JOIN puerto p
    ON c.dorsal = p.dorsal;


/*
  11. Ciclistas que han ganado etapas y que no han ganado puertos
*/

CREATE OR REPLACE VIEW ganadoPuertos AS
SELECT p.dorsal FROM puerto p;

SELECT DISTINCT e.dorsal FROM etapa e
LEFT JOIN ganadoPuertos c1
  USING(dorsal)
WHERE c1.dorsal IS NULL;

SELECT c.nombre, c.nomequipo FROM (SELECT DISTINCT e.dorsal FROM etapa e
LEFT JOIN ganadoPuertos c1
  USING(dorsal)
WHERE c1.dorsal IS NULL) c JOIN ciclista c USING (dorsal);

