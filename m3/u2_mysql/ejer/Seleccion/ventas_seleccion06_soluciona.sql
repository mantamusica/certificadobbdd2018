﻿
-- Consultas Selección 

/* 
  Consulta 1 Número de vendedores cuya fecha de alta sea en febrero de cualquier año
*/

SELECT
  COUNT(*)
FROM vendedores
WHERE EXTRACT(MONTH FROM FechaAlta) = 2;

/* 
  Consulta 2 Número de vendedores guapos y feos. 
*/

SELECT
  COUNT(*)
FROM vendedores v
WHERE v.`Guap@` = 0
AND v.`Guap@` = 1;

/* 
  Consulta 3 El nombre del producto más caro.
*/

CREATE OR REPLACE VIEW precioMaximo
AS
SELECT
  MAX(Precio)
FROM productos p;

SELECT
  NomProducto
FROM productos
WHERE Precio = (SELECT
    MAX(Precio)
  FROM productos p);

/* 
  Consulta 4 El precio medio de los productos por grupo
*/

SELECT
  p.IdGrupo,
  AVG(p.Precio)
FROM productos p
GROUP BY p.IdGrupo;

/* 
  Consulta 5 Indica que grupo tiene producto que se hayan vendido alguna vez 
*/

SELECT DISTINCT
  p.IdGrupo
FROM productos p;

/* 
  Consulta 6 Indica cuales son los grupos de los cuales no se ha vendido ningún producto.   
*/

  SELECT DISTINCT g.IdGrupo FROM productos p
    LEFT JOIN grupos g USING(IdGrupo)
  WHERE g.IdGrupo IS NULL;



/* 
  Consulta 7 Número de poblaciones cuyos vendedores son guapos
*/

SELECT COUNT(DISTINCT v.Poblacion) FROM vendedores v WHERE v.`Guap@`=1;

/* 
  Consulta 8 Nombre de la población que tienen más vendedores casados
*/

CREATE OR REPLACE VIEW vendedoresCasados AS
SELECT v.Poblacion,COUNT(v.Poblacion) vCasados FROM vendedores v WHERE v.EstalCivil='casado' GROUP BY v.Poblacion;

SELECT c.Poblacion FROM vendedoresCasados c WHERE c.vCasados=(SELECT MAX(c1.vCasados) FROM vendedoresCasados c1);

/* 
  Consulta 9 Población donde ninguno de sus vendedores esté casado
*/

SELECT DISTINCT v.Poblacion FROM vendedores v WHERE v.EstalCivil <> 'casado';

/* 
  Consulta 10 Vendedor que no ha vendido nada
*/


SELECT DISTINCT * FROM vendedores ve
  LEFT JOIN ventas v ON ve.IdVendedor = v.`Cod Vendedor`
  WHERE v.`Cod Vendedor` IS NULL;

/* 
  Consulta 11 El vendedor que ha vendido más kilos. Quiero conocer su nombre.
*/

  CREATE OR REPLACE VIEW kilosVendedor AS
  SELECT v.`Cod Vendedor`,SUM(v.Kilos) totalKilos FROM ventas v GROUP BY v.`Cod Vendedor`;

  SELECT MAX(v.totalKilos) FROM kilosVendedor v;

  SELECT v.`Cod Vendedor` FROM kilosVendedor v WHERE v.totalKilos = (SELECT MAX(v.totalKilos) FROM kilosVendedor v);
