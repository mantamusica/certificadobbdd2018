﻿
USE m3u2h1;

SELECT c.`CÓDIGO CLIENTE`, c.EMPRESA empresa, c.POBLACIÓN poblacion FROM clientes c JOIN pedidos p USING(`CÓDIGO CLIENTE`)WHERE c.POBLACIÓN IN('Madrid','Barcelona','Zaragoza');

CREATE OR REPLACE VIEW clientesMadBarZarOptimizada AS
SELECT p.`CÓDIGO CLIENTE`, mbz.empresa, mbz.poblacion, p.`NÚMERO DE PEDIDO`  FROM pedidos p JOIN clientesMadBarZar mbz USING(`CÓDIGO CLIENTE`);

CREATE OR REPLACE VIEW pedidos2002Tarj  AS
SELECT p.`CÓDIGO CLIENTE`, p.`NÚMERO DE PEDIDO`, p.`FECHA DE PEDIDO`, p.`FORMA DE PAGO` FROM pedidos p WHERE p.`FORMA DE PAGO` = 'Tarjeta' AND YEAR(p.`FECHA DE PEDIDO`) = 2002;

CREATE OR REPLACE VIEW clientesPedidos2002Tarj AS
SELECT * FROM clientes c JOIN pedidos2002Tarj pt USING(`CÓDIGO CLIENTE`) ORDER BY pt.`FECHA DE PEDIDO`;

CREATE OR REPLACE VIEW listadoPedidos AS
SELECT COUNT(*) npedidos, p.`CÓDIGO CLIENTE` FROM pedidos p GROUP BY p.`CÓDIGO CLIENTE`;

CREATE OR REPLACE VIEW MaxListadoPedidos AS
SELECT MAX(npedidos) maximo FROM listadoPedidos p;

CREATE OR REPLACE VIEW clienteMaxNumPedidos AS
SELECT * FROM listadoPedidos p JOIN MaxListadoPedidos mlp ON p.npedidos = mlp.maximo;

