﻿CREATE OR REPLACE VIEW precioMayor100 AS
  SELECT
  `p`.`CÓDIGO ARTÍCULO` AS `codigoArticulo`,
  `p`.`SECCIÓN` AS `seccion`,
  `p`.`PRECIO` AS `precio`
FROM `productos` `p`
WHERE (`p`.`PRECIO` > 100);

CREATE OR REPLACE VIEW deportesPrecMay100 AS
  SELECT
  `m`.`codigoArticulo` AS `codigoArticulo`,
  `m`.`seccion` AS `seccion`,
  `m`.`precio` AS `precio`
FROM `preciomayor100` `m`
WHERE (`m`.`seccion` = 'deportes');

INSERT INTO precioMayor100 VALUES('AR90','NOVEDADES',5);

INSERT INTO precioMayor100 VALUES('AR91','NOVEDADES',110);

INSERT INTO deportesPrecMay100 VALUES('AR92','NOVEDADES',5);

INSERT INTO deportesPrecMay100 VALUES('AR93','DEPORTES',5);

INSERT INTO deportesPrecMay100 VALUES('AR94','DEPORTES',200);
