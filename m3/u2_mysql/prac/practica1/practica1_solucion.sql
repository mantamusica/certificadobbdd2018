﻿
-- Practica 1

/* 
  Consulta 1 Mostrar todos los campos y todos los registros de la tabla empleado
*/

  SELECT * FROM emple;

/* 
  Consulta 2 Mostrar todos los campos y todos los registros de la tabla departamento 
*/

  SELECT * FROM depart d;

/* 
  Consulta 3 Mostrar el apellido y oficio de cada empleado 
*/

  SELECT apellido, oficio FROM emple;

/* 
  Consulta 4 Mostrar localización y número de cada departamento 
*/

  SELECT dnombre, loc FROM depart;

/* 
  Consulta 5 Mostrar el número, nombre y localización de cada departamento 
*/

  SELECT dept_no AS numero, dnombre AS departamento, loc AS localizacion FROM depart;

/* 
  Consulta 6 Indicar el número de empleados que hay  
*/

  SELECT COUNT(e.emp_no) AS Num_Empleados FROM emple e;

/* 
  Consulta 7 Datos de los empleados ordenados por apellido de forma ascendente   
*/

  SELECT * FROM emple ORDER BY apellido ASC;
/* 
  Consulta 8 Datos de los empleados ordenados por apellido de forma descendente   
*/

  SELECT * FROM emple ORDER BY apellido DESC;

/* 
  Consulta 9 Indicar el numero de departamentos que hay 
*/

  SELECT COUNT(*) FROM depart;
/* 
  Consulta 10 Indicar el número de empleados mas el numero de departamentos   
*/

  SELECT (SELECT COUNT(*) FROM depart) + COUNT(*) FROM emple;
/* 
  Consulta 11 Datos de los empleados ordenados por número de departamento descendentemente  
*/

  SELECT * FROM emple INNER JOIN depart WHERE emple.dept_no = depart.dept_no ORDER BY emple.dept_no DESC;

/* 
  Consulta 12 Datos de los empleados ordenados por número de departamento descendentemente y por oficio ascendente  
*/

  SELECT * FROM emple INNER JOIN depart WHERE emple.dept_no = depart.dept_no ORDER BY emple.dept_no DESC, depart.oficio ASC; 
/* 
  Consulta 13 Datos de los empleados ordenados por número de departamento descendentemente y por apellido ascendentemente
*/

  SELECT * FROM emple INNER JOIN depart WHERE emple.dept_no = depart.dept_no ORDER BY emple.dept_no DESC, emple.apellido ASC; 
/* 
  Consulta 14 Mostrar los códigos de los empleados cuyo salario sea mayor que 2000  
*/

  SELECT emp_no FROM emple WHERE salario > 2000;
/* 
  Consulta 15 Mostrar los códigos y apellidos de los empleados cuyo salario sea menor que 2000  
*/

  SELECT emp_no, apellidos FROM emple WHERE salario < 2000;
/* 
  Consulta 16 Mostrar los datos de los empleados cuyo salario este entre 1500 y 2500   
*/

  SELECT * FROM emple WHERE salario BETWEEN 1500 AND 2500;
/* 
  Consulta 17 Mostrar los datos de los empleados cuyo oficio sea ʻANALISTAʼ  
*/

  SELECT * FROM emple WHERE oficio =  ʻANALISTAʼ;
/* 
  Consulta 18 Mostrar los datos de los empleados cuyo oficio sea ANALISTA y ganen mas de 2000 € 
*/

  SELECT * FROM emple WHERE oficio =  ʻANALISTAʼ AND salario > 2000;
/* 
  Consulta 19 Seleccionar el apellido y oficio de los empleados del departamento número 20  
*/

  SELECT apellido, oficio FROM emple WHERE dept_no = 20;
/* 
  Consulta 20 Contar el número de empleados cuyo oficio sea VENDEDOR   
*/

  SELECT COUNT(*) FROM emple WHERE oficio = ʻVENDEDORʼ;
/* 
  Consulta 21 Mostrar todos los datos de los empleados cuyos apellidos comiencen por m o por n ordenados por apellido de forma ascendente  
*/

  SELECT * FROM emple WHERE apellido LIKE 'm%' OR 'n%' ORDER BY apellido DESC;
/* 
  Consulta 22 Seleccionar los empleados cuyo oficio sea ʻVENDEDORʼ. Mostrar los datos ordenados por apellido de forma ascendente  
*/

  SELECT * FROM emple WHERE oficio = ʻVENDEDORʼ ORDER BY apellido ASC;  
/* 
  Consulta 23 Mostrar los apellidos del empleado que mas gana   
*/

  SELECT apellido FROM emple ORDER BY salario ASC LIMIT 1;
/* 
  Consulta 24 Mostrar los empleados cuyo departamento sea 10 y cuyo oficio sea ʻANALISTAʼ. Ordenar el resultado por apellido y oficio de forma ascendente   
*/

  SELECT * FROM emple WHERE oficio =  ʻANALISTAʼ AND dept_no = 10 ORDER BY apellido ASC;
/* 
  Consulta 25 Realizar un listado de los distintos meses en que los empleados se han dado de alta  
*/

  SELECT MONTH(fecha_alt) FROM emple;
/* 
  Consulta 26 Realizar un listado de los distintos años  en que los empleados se han dado de alta 
*/

  SELECT YEAR(fecha_alt) FROM emple;
/* 
  Consulta 27 Realizar un listado de los distintos días del mes en que los empleados se han dado de alta 
*/

  SELECT DAYOFMONTH(fecha_alt) FROM emple;
/* 
  Consulta 28 Mostrar los apellidos de los empleados que tengan un salario mayor que 2000 o que pertenezcan al departamento número 20 
*/

  SELECT apellido FROM emple WHERE salario > 2000 OR dept_no = 20;
/* 
  Consulta 29 Realizar un listado donde nos coloque el apellido del empleado y el nombre del departamento al que pertenece   
*/

  SELECT emple.apellido, depart.dnombre FROM emple INNER JOIN depart WHERE emple.dept_no = depart.dept_no;
/* 
  Consulta 30 Realizar un listado donde nos coloque el apellido del empleado, el oficio del empleado y el nombre del departamento al que pertenece. Ordenar los resultados por apellido de forma descendente   
*/

  SELECT emple.apellido, depart.oficio, depart.dnombre FROM emple INNER JOIN depart WHERE emple.dept_no = depart.dept_no;
/* 
  Consulta 31 Listar el número de empleados por departamento. La salida del comando debe ser como la que vemos a continuación 
*/

  SELECT emple.dept_no, Count(emple.emp_no) AS Cuenta FROM emple GROUP BY emple.dept_no;  
/* 
  Consulta 32 Realizar el mismo comando anterior pero obteniendo una salida como la que vemos   
*/

  SELECT depart.dnombre, Count(emple.emp_no) AS Cuenta FROM emple INNER JOIN depart WHERE emple.dept_no = depart.dept_no GROUP BY emple.dept_no;
/* 
  Consulta 33 Listar el apellido de todos los empleados y ordenarlos por oficio, y por nombre 
*/

  SELECT apellido FROM emple ORDER BY oficio AND apellido;
/* 
  Consulta 34 Seleccionar de la tabla EMPLE los empleados cuyo apellido empiece por ʻAʼ.
  Listar el apellido de los empleados   
*/

  SELECT apellido FROM emple WHERE apellido LIKE 'A%';
/* 
  Consulta 35 Seleccionar de la tabla EMPLE los empleados cuyo apellido empiece por ʻAʼ o por ’M’. 
  Listar el apellido de los empleados  
*/

  SELECT apellido FROM emple WHERE apellido LIKE 'A%' OR 'M%';  
/* 
  Consulta 36 Seleccionar de la tabla EMPLE los empleados cuyo apellido no termine por ʻZʼ. 
  Listar todos los campos de la tabla empleados   
*/

  SELECT apellido FROM emple WHERE apellido NOT LIKE '%Z';
/* 
  Consulta 37 Seleccionar de la tabla EMPLE aquellas filas cuyo APELLIDO empiece por ʻAʼ y el OFICIO tenga una ʻEʼ en cualquier posición. 
  Ordenar la salida por oficio y por salario de forma descendente  
*/

  SELECT apellido FROM emple WHERE apellido LIKE 'A%' AND oficio LIKE '%E%' ORDER BY salario DESC;
