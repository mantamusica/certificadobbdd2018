﻿USE practica10;

CREATE TABLE IF NOT EXISTS practica10.coche (
  codigo int(11) NOT NULL AUTO_INCREMENT,
  matricula char(8) NOT NULL UNIQUE,
  marca varchar(25) DEFAULT NULL,
  modelo varchar(25) DEFAULT NULL,
  color enum ('rojo', 'verde', 'negro', 'plateado', 'blanco', 'azul') DEFAULT 'blanco',
  garage int(11) DEFAULT NULL,
  precioAlquiler float DEFAULT 0,
  PRIMARY KEY (codigo)
)
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS practica10.garage (
  codigo int(11) NOT NULL AUTO_INCREMENT,
  nombre varchar(25) DEFAULT NULL,
  direccion varchar(25) DEFAULT NULL,
  PRIMARY KEY (codigo)
)
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS practica10.cliente (
  codigo int(11) NOT NULL AUTO_INCREMENT,
  nif char(8) NOT NULL UNIQUE,
  nombre varchar(25) DEFAULT NULL,
  direccion varchar(25) DEFAULT NULL,
  ciudad varchar(25) DEFAULT NULL,
  telefono varchar(25) DEFAULT NULL,
  PRIMARY KEY (codigo)
)
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS practica10.agencia (
  codigo int(11) NOT NULL AUTO_INCREMENT,
  nombre varchar(25) DEFAULT NULL,
  PRIMARY KEY (codigo)
)
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS practica10.reserva (
  numero int(11) NOT NULL AUTO_INCREMENT,
  fechaIni date DEFAULT NULL,
  fechaFin date DEFAULT NULL,
  cliente int(11),
  agencia int(11),
  precioTotal float,
  devuelta boolean,
  PRIMARY KEY (numero)
)
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS practica10.listaReserva (
  reserva int(11) NOT NULL,
  coche int(11) NOT NULL,
  litrosInicio int(11) NOT NULL,
  precioFinal float DEFAULT NULL,
  PRIMARY KEY (reserva,coche)
)
ENGINE = INNODB;

ALTER TABLE practica10.coche
ADD CONSTRAINT FK_coche_garage FOREIGN KEY (garage)
REFERENCES practica10.garage (codigo) ON DELETE CASCADE ON UPDATE CASCADE; 

ALTER TABLE practica10.reserva
ADD CONSTRAINT FK_reserva_cliente FOREIGN KEY (cliente)
REFERENCES practica10.cliente (codigo) ON DELETE CASCADE ON UPDATE CASCADE; 

ALTER TABLE practica10.reserva
ADD CONSTRAINT FK_reserva_agencia FOREIGN KEY (agencia)
REFERENCES practica10.agencia (codigo) ON DELETE CASCADE ON UPDATE CASCADE; 

ALTER TABLE practica10.listareserva
ADD CONSTRAINT FK_listareserva_reserva FOREIGN KEY (reserva)
REFERENCES practica10.reserva (numero) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE practica10.listareserva
ADD CONSTRAINT FK_listareserva_coche2 FOREIGN KEY (coche)
REFERENCES practica10.coche (codigo) ON DELETE CASCADE ON UPDATE CASCADE;