﻿CREATE OR REPLACE VIEW clientesagencia2 AS
  SELECT DISTINCT
  `r`.`cliente` AS `cliente`
FROM `reserva` `r`
WHERE (`r`.`agencia` = 2);

CREATE OR REPLACE VIEW clientesagencia2masde3 AS
SELECT
  `r`.`cliente` AS `cliente`,
  COUNT(0) AS `reservas`
FROM (`reserva` `r`
  JOIN `cliente` `c`
    ON ((`r`.`cliente` = `c`.`codigo`)))
GROUP BY `r`.`cliente`
HAVING (`reservas` > 3);

CREATE OR REPLACE VIEW clientesMurcia AS
SELECT
  `c`.`codigo` AS `codigo`
FROM `cliente` `c`
WHERE (`c`.`ciudad` LIKE 'M%');

CREATE OR REPLACE VIEW codagenciareservasnodevueltas AS
SELECT DISTINCT
  `r`.`agencia` AS `agencia`
FROM (`reserva` `r`
  JOIN `reservasnodevueltas` `nd`
    ON ((`r`.`numero` = `nd`.`numero`)));

CREATE OR REPLACE VIEW clienteContarResPrecioMedio AS
SELECT r.cliente, COUNT(*)numero, AVG(r.precioTotal)precioMedio FROM reserva r GROUP BY r.cliente;

CREATE OR REPLACE VIEW codagenciareservasnodevueltas AS
SELECT DISTINCT
  `r`.`agencia` AS `agencia`
FROM (`reserva` `r`
  JOIN `reservasnodevueltas` `nd`
    ON ((`r`.`numero` = `nd`.`numero`)));

CREATE OR REPLACE VIEW sumaporreservas AS
SELECT
  `l`.`reserva` AS `reserva`,
  SUM(`r`.`precioTotal`) AS `suma`
FROM (`reserva` `r`
  JOIN `listareserva` `l`
    ON ((`r`.`numero` = `l`.`reserva`)))
GROUP BY `l`.`reserva`;

CREATE OR REPLACE VIEW reservasPosteriorFecha AS
SELECT DISTINCT r.agencia FROM reserva r WHERE DAY(r.fechaIni)>DAY('2004-01-15') ORDER BY r.agencia;

CREATE OR REPLACE VIEW agenciaNullreservasPosteriorFecha as
SELECT a.codigo, a.nombre FROM agencia a left JOIN reservasPosteriorFecha pf ON a.codigo = pf.agencia
WHERE pf.agencia IS NULL;

CREATE OR REPLACE VIEW reservasposteriorfecha AS
  SELECT DISTINCT
  `r`.`agencia` AS `agencia`
FROM `reserva` `r`
WHERE (DAYOFMONTH(`r`.`fechaIni`) > DAYOFMONTH('2004-01-15'))
ORDER BY `r`.`agencia`;

CREATE OR REPLACE VIEW reservasporcoche AS
SELECT
  `l`.`coche` AS `coche`,
  COUNT(0) AS reservas
FROM `listareserva` `l`
GROUP BY `l`.`coche`;

CREATE OR REPLACE VIEW reservasnodevueltas AS
  SELECT
  `r`.`numero` AS `numero`,
  `r`.`fechaIni` AS `fechaIni`,
  `r`.`fechaFin` AS `fechaFin`,
  `r`.`precioTotal` AS `precioTotal`
FROM `reserva` `r`
WHERE (`r`.`devuelta` = 0)
ORDER BY `r`.`fechaFin` DESC;

CREATE OR REPLACE VIEW masDe3reservasporcoche AS
SELECT coche FROM reservasporcoche r WHERE reservas>3;

CREATE OR REPLACE VIEW cochemasDe3reservasporcoche AS
SELECT c.matricula,c.marca,c.modelo FROM coche c INNER JOIN masDe3reservasporcoche d ON c.codigo=d.coche;

CREATE OR REPLACE VIEW maximoNumReservas AS
SELECT MAX(reservas)maximo FROM reservasporcoche r;

CREATE OR REPLACE VIEW cocheConMasReservas AS
SELECT r.coche FROM reservasporcoche r INNER JOIN maximoNumReservas nr ON r.reservas = nr.maximo;

CREATE OR REPLACE VIEW cocheNuncaReservado AS
SELECT * FROM coche c left JOIN reservasporcoche r ON c.codigo=r.coche WHERE r.coche IS NULL;

CREATE OR REPLACE VIEW reservasMasdeUnaSemana AS
SELECT *FROM reserva r WHERE DATEDIFF(r.fechaFin,r.fechaIni)/7 >1;

CREATE OR REPLACE VIEW agenciasreservasMasdeUnaSemana AS
SELECT r.agencia, COUNT(*) numero FROM reserva r
JOIN reservasMasdeUnaSemana mus USING(numero) GROUP BY r.agencia HAVING numero > 3;

CREATE OR REPLACE VIEW nombreagenciasreservasMasdeUnaSemana AS
  SELECT a.codigo,a.nombre FROM agencia a INNER JOIN agenciasreservasMasdeUnaSemana mus ON a.codigo=mus.agencia;

-- ultima consulta

CREATE OR REPLACE VIEW maxPrecioPorCoche AS
SELECT l.coche,MAX(r.precioTotal)maximo FROM reserva r INNER JOIN listareserva l ON r.numero = l.reserva GROUP BY l.coche;

CREATE OR REPLACE VIEW cochemaxPrecioPorCoche AS
SELECT ppc.coche, c.marca, c.modelo, ppc.maximo FROM coche c INNER JOIN maxPrecioPorCoche ppc ON c.codigo=ppc.coche;

CREATE OR REPLACE VIEW reservaPrecioTotal AS
SELECT r.numero, r.precioTotal,l.coche FROM listareserva l INNER JOIN reserva r ON l.reserva = r.numero;

CREATE OR REPLACE VIEW reservaMasCaraPorcoche AS
SELECT pt.coche, ppc.marca, ppc.modelo, pt.precioTotal FROM reservaPrecioTotal pt JOIN cochemaxPrecioPorCoche ppc USING(coche) WHERE pt.precioTotal = ppc.maximo;



 