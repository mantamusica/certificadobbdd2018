﻿-- reservas (numero, fechaInicio, fechaFin, coche, marca, modelo) de coches del garage con codigo 1 realizadas a través de la agencia 4.

-- reservas codigo 4

CREATE OR REPLACE VIEW agencia4
AS
SELECT
  *
FROM reserva r
WHERE r.agencia = 4;

-- coche garage 1

CREATE OR REPLACE VIEW garage1
AS
SELECT
  *
FROM coche c
WHERE c.garage = 1;

CREATE OR REPLACE VIEW reservasAgencia4Garage1
AS
SELECT
  l.reserva,
  l.coche,
  a.fechaIni,
  a.fechaFin,
  g.marca,
  g.modelo
FROM listareserva l
  INNER JOIN garage1 g
    ON l.coche = g.codigo
  INNER JOIN agencia4 a
    ON l.reserva = a.numero;

-- primera reserva que se inició y periodo de duracion(reserva, fechaInicio, dias)

CREATE OR REPLACE VIEW primeraResDuracion
AS
SELECT
  r.numero,
  r.fechaIni,
  DATEDIFF(r.fechaFin, r.fechaIni) dias
FROM reserva r
ORDER BY r.fechaIni ASC LIMIT 1;

-- Garages(codigo, nombre) de los que nunca se ha reservado ningún coche.

CREATE OR REPLACE VIEW garageNuncaReservado
AS
SELECT
  c.codigo,
  c.marca
FROM coche c
  LEFT JOIN listareserva l
    ON c.codigo = l.coche
WHERE l.coche IS NULL;

-- Clientes(nif,nombre) que hayan reservado algún coche de la marca vw mas de dos veces

CREATE OR REPLACE VIEW cochesReservMas2
AS
SELECT
  l.coche,
  COUNT(*) numero
FROM listareserva l
GROUP BY l.coche
HAVING numero > 2;

SELECT
  *
FROM coche c
  INNER JOIN cochesReservMas2 rm
    ON c.codigo = rm.coche
WHERE c.marca = 'WildBlueYonder';

-- coche mas reservado(matricula, marca, modelo, numeroReservas)

CREATE OR REPLACE VIEW numeroReservasCoche
AS
SELECT
  l.coche,
  COUNT(*) numero
FROM listareserva l
GROUP BY l.coche;

CREATE OR REPLACE VIEW maximonumeroReservasCoche
AS
SELECT
  MAX(numero) maximo
FROM numeroReservasCoche rc;

CREATE OR REPLACE VIEW cochemaximonumeroReservasCoche
AS
SELECT
  *
FROM numeroReservasCoche rc
  INNER JOIN maximonumeroReservasCoche rc1
    ON rc.numero = rc1.maximo;

CREATE OR REPLACE VIEW datoscochemaximonumeroReservasCoche
AS
SELECT
  c.matricula,
  c.marca,
  c.modelo,
  c1.maximo
FROM coche c
  INNER JOIN cochemaximonumeroReservasCoche c1
    ON c.codigo = c1.coche;

-- coche mas reservado por el mismo cliente mas veces, (matricula, cliente, numeroReservas);

CREATE OR REPLACE VIEW contarCocheCliente
AS
SELECT
  cliente,
  coche,
  COUNT(*) numero
FROM reserva r
  INNER JOIN listareserva l
    ON r.numero = l.reserva
GROUP BY cliente,
         coche;

CREATE OR REPLACE VIEW maxContarCocheCliente
AS
SELECT
  MAX(numero) maximo
FROM contarCocheCliente cc;

CREATE OR REPLACE VIEW cocheresmasvecesmismocliente
AS
SELECT
  cliente,
  coche,
  numero
FROM contarCocheCliente cc
  INNER JOIN maxContarCocheCliente ccc
    ON cc.numero = ccc.maximo;

-- ultima reserva realizada por cada cliente (cliente, reserva, fechaIni) tambien debe aparecer los clientes sin reservas.

CREATE OR REPLACE VIEW clientesSinReservas
AS
SELECT
  r.numero,
  c.codigo cliente,
  r.fechaIni
FROM cliente c
  LEFT JOIN reserva r
    ON c.codigo = r.cliente
WHERE r.cliente IS NULL;

CREATE OR REPLACE VIEW ultimaReservaCliente
AS
SELECT
  r.numero,
  r.cliente,
  MAX(r.fechaIni) fechaIni
FROM reserva r
GROUP BY r.cliente;

CREATE OR REPLACE VIEW ultimaReservaClienteYClientesSinReserva
AS
SELECT
  *
FROM ultimaReservaCliente
UNION
SELECT
  *
FROM clientesSinReservas sr;

-- suma total del precio de las reservas realizadas por clientes que hayan hecho menos de 3 reservas(cliente, sumaPrecio)

CREATE OR REPLACE VIEW clientesMenos3Reservas
AS
SELECT
  r.cliente,
  COUNT(*) numero
FROM reserva r
GROUP BY r.cliente
HAVING numero < 3;

CREATE OR REPLACE VIEW totalPrecioResclientesMenos3Reservas
AS
SELECT
  r.cliente,
  SUM(r.precioTotal)
FROM reserva r
  JOIN clientesMenos3Reservas mr USING (cliente)
GROUP BY r.cliente;

-- reservas con un solo coche en la lista reservas (reserva, matricula, agencia)

CREATE OR REPLACE VIEW cochesSoloUnaReserva
AS
SELECT
  *
FROM reservasporcoche r
WHERE reservas = 1;

CREATE OR REPLACE VIEW matriculaCochesSoloUnaReserva
AS
SELECT
  c.codigo,
  c.matricula
FROM coche c
  JOIN cochesSoloUnaReserva sur1
    ON c.codigo = sur1.coche;

CREATE OR REPLACE VIEW resMatUnSoloCoche
AS
SELECT
  *
FROM listareserva l
  INNER JOIN matriculaCochesSoloUnaReserva sur
    ON sur.codigo = l.coche;

CREATE OR REPLACE VIEW resMatAgenciaUnSoloCoche
AS
SELECT
  mausc.reserva,
  mausc.matricula,
  r.agencia
FROM reserva r
  INNER JOIN resMatUnSoloCoche mausc
    ON r.numero = mausc.reserva;

-- clientes que solo han hecho reservas de una agencia(cliente, agencia)

CREATE OR REPLACE VIEW clientes1Agencia
AS
SELECT
  r.cliente,
  COUNT(r.agencia) agencias
FROM reserva r
GROUP BY r.cliente
HAVING agencias = 1;

CREATE OR REPLACE VIEW clienteAgencia
AS
SELECT
  r.cliente,
  r.agencia
FROM reserva r
  JOIN clientes1Agencia a USING (cliente);

-- clientes(nif,nombre) que han hecho reservas de todas las agencias

CREATE OR REPLACE VIEW numeroAgencias
AS
SELECT
  COUNT(*) numero
FROM agencia a;

SELECT
  r.cliente,
  COUNT(r.agencia) agencias
FROM reserva r
GROUP BY r.cliente
HAVING agencias = (SELECT
    numero
  FROM numeroAgencias);

/* para cada coche(matricula), indicar cuantas veces se ha reservado(cuantas_reservas) y cual es la fecha de inicio de la reserva
    mas reciente(fechaInicio). En el resultado de la consulta tambien deben aparecer los coches que no han sido reservados ninguna
    vez, mostrando en la columna_reservas un 0 y en la fecha_inicio un NULL. */

CREATE OR REPLACE VIEW cocheContarReservas
AS
SELECT
  l.coche,
  COUNT(l.reserva) reservas
FROM listareserva l
GROUP BY l.coche;

CREATE OR REPLACE VIEW cocheMaxFechaIni
AS
SELECT
  l.coche,
  MAX(r.fechaIni) fechaIni
FROM reserva r
  INNER JOIN listareserva l
    ON r.numero = l.reserva
GROUP BY l.coche;

CREATE OR REPLACE VIEW cocheContReservFechaIni
AS
SELECT
  ccr.coche,
  ccr.reservas,
  cmfi.fechaIni
FROM cocheContarReservas ccr
  INNER JOIN cocheMaxFechaIni cmfi
    ON ccr.coche = cmfi.coche;

CREATE OR REPLACE VIEW matricocheContReservFechaIni
AS
SELECT
  c.matricula,
  crfi.reservas,
  crfi.fechaIni
FROM coche c
  INNER JOIN cocheContReservFechaIni crfi
    ON c.codigo = crfi.coche;

CREATE OR REPLACE VIEW cochesNoAlquilados
AS
SELECT
  matricula,
  reserva resevas,
  l.precioFinal fechaIni
FROM coche
  LEFT JOIN listareserva l
    ON coche.codigo = l.coche
WHERE l.coche IS NULL;

CREATE OR REPLACE VIEW matricocheContReservFechaIniUNIONcochesNoAlquilados AS
SELECT
  *
FROM matricocheContReservFechaIni crfi
UNION
SELECT
  *
FROM cochesNoAlquilados na;



     

