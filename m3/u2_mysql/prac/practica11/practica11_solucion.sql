﻿/*1.  PRODUCTOS se detallan los diferentes productos que se comercializaban. Insertar un campo rotulada como DES_RUBRO  y
  en esta mostrar el tipo de rubro, dependiendo de la información detallada en la tabla rubros.*/

UPDATE productos p
INNER JOIN rubros r
  ON p.`COD-RUBRO` = r.`COD-RUBRO`
SET p.`DESC-RUBRO` = r.DESCUENTO;

/*2.  Realizar el mismo procedimiento, pero en la tabla nombrada como Marcas, la que contiene la información de las 
diferentes marcas con las que se trabaja. */

UPDATE marcas p
INNER JOIN rubros r
  ON p.`COD-RUBRO` = r.`COD-RUBRO`
SET p.`DESC-RUBRO` = r.DESCUENTO;

/* En la tabla PRECIOS, se detallan los precios de los diferentes artículos que se venden. */


/*  a. en esta se deberá mostrar por cada columna de código su descripción correspondiente(por ejemplo para
Cod-Producto, se deberá mostrar Desc-Producto,etc) Tener en cuenta que la información requerida se localiza en las
hojas respectivas. */

UPDATE precios p
JOIN rubros r USING (`COD-RUBRO`)
JOIN productos p1 USING (`COD-PRODUCTO`)
SET p.`DESC-PRODUCTO` = p1.`DESC-PRODUCTO`;

UPDATE precios p
  JOIN marcas m USING (`COD-MARCA`)
  SET p.`DESC-MARCA`=m.`DESC-MARCA`;

/*  b. En un campo rotulada como COD.ARTICULO, deberá generar un código que identifique a un producto por su
marca. Este se formará de unir el Cod_Producto, mas una p de producto, mas el cod_marca mas una m ej 1p4m.
Dicho código será utilizado más adelante en este ejercicio.*/

UPDATE precios p
SET p.`COD-ARTICULO` = CONCAT(p.`COD-PRODUCTO`, 'p', p.`COD-MARCA`, 'm');

/* c. En la columna IVA INC. se deberá calcular el precio del artículo con su iva incluido. Tener en cuenta que cada 
rubro al cual pertenecen los artículos, posee un iva específico, dato que se localiza en la hoja de rubros.*/

UPDATE precios p
JOIN rubros r USING (`COD-RUBRO`)
SET p.`IVA_Incl.` = (p.IMPORTE * r.IVA) + p.IMPORTE;

/* En la tabla gestion, se detalla la información de las diferentes ventas realizadas.*/

/* a. en la columna COD_ARTICULO, deberá formar el mismo código que el generado en la hoja Precios. Dicho dato le
  le servirá para mostrar de forma automática el precio unit. (detallado como importe en la hoja precios) de cada
articulo. */

UPDATE gestion g
SET g.`COD-ARTICULO` = CONCAT(g.`COD-PRODUCTO`, 'p', g.`COD-MARCA`, 'm');

UPDATE gestion g
JOIN marcas m USING (`COD-MARCA`)
JOIN precios p USING (`COD-RUBRO`)
SET g.`PRECIO_UNIT.` = p.IMPORTE;

/* b. Calcular la columna importe dato que surge del precio unitario y la cantidad
  haciendo su multiplicación. */

UPDATE gestion g
SET g.IMPORTE = g.CANTIDAD * g.`PRECIO_UNIT.`;

/*  c. Mostrar el importe correspondiente al cálculo de la columna DESCUENTO, teniendo en cuenta que se realizará
    un descuento en la venta de los artículos, dependiendo del rubro y del día de la semana que se realice la misma.
    Los días lunes poseen descuento los fiambres, los días miercoles los lácteos y los días viernes los productos de 
    panadería. El porcentaje se localiza en la hoja Rubros. */

CREATE OR REPLACE VIEW DIA_SEMANA
AS
SELECT
  g.gestion_id,
  (ELT(WEEKDAY(g.FECHA) + 1, 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo')) AS DIA_SEMANA
FROM gestion g;

SELECT
  g.gestion_id,
  WEEKDAY(g.FECHA)
FROM gestion g;

UPDATE gestion g
JOIN marcas m
  ON g.`COD-MARCA` = m.`COD-MARCA`
SET g.DESCUENTO = g.IMPORTE * CASE WHEN WEEKDAY(g.FECHA) = 2 AND
      g.`DESC-RUBRO` = 'fiambres' THEN m.`DESC-RUBRO` WHEN WEEKDAY(g.FECHA) = 4 AND
      g.`DESC-RUBRO` = 'lacteos' THEN m.`DESC-RUBRO` WHEN WEEKDAY(g.FECHA) = 6 AND
      g.`DESC-RUBRO` = 'panaderia' THEN m.`DESC-RUBRO` ELSE 0 END;

/*  d. En el subtotal se deberá mostrar el importe de la venta, considerando el descuento correspondiente
*/

UPDATE gestion g
SET g.SUBTOTAL = g.IMPORTE - g.DESCUENTO;

/*  e. En el iva, se deberá mostrar el importe correspondiente al cálculo del iva, tomando en cuento los porcentajes
    correspondientes de la hoja rubros. */

UPDATE gestion g
INNER JOIN productos p
  ON g.`COD-PRODUCTO` = p.`COD-PRODUCTO`
JOIN rubros r
  ON p.`COD-RUBRO` = r.`COD-RUBRO`
SET g.IVA = g.SUBTOTAL * r.IVA;

/*  f. obtener el total, considerando el subtotal y el iva.
*/

UPDATE gestion g
SET g.TOTAL = g.SUBTOTAL + g.IVA;

/*  g. mostrar las descripciones para las columnas PRODUCTO, RUBRO y MARCA.
*/

UPDATE gestion g
JOIN marcas m USING (`COD-MARCA`)
SET g.`DESC-MARCA` = m.`DESC-MARCA`;

UPDATE gestion g
JOIN productos p USING (`COD-PRODUCTO`)
SET g.`DESC-PRODUCTO` = p.`DESC-PRODUCTO`;

UPDATE gestion g
JOIN productos p
  ON g.`COD-PRODUCTO` = p.`COD-PRODUCTO`
JOIN rubros r
  ON p.`COD-RUBRO` = r.`COD-RUBRO`
SET g.`DESC-RUBRO` = r.`DESC-RUBRO`;