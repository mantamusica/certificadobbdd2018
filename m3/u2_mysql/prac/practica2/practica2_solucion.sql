﻿
-- Practica 1

/* 
  Consulta 1 Indicar el número de ciudades que hay en la tabla ciudades 
*/

  SELECT COUNT(nombre) AS ciudades FROM ciudad;

/* 
  Consulta 2 Indicar el nombre de las ciudades que tengan una población por encima de la población media  
*/

  SELECT nombre FROM ciudad WHERE poblacion > (SELECT AVG(poblacion)FROM ciudad);

/* 
  Consulta 3 Indicar el nombre de las ciudades que tengan una población por debajo de la población media  
*/

  SELECT nombre FROM ciudad WHERE poblacion < (SELECT AVG(poblacion)FROM ciudad);

/* 
  Consulta 4 Indicar el nombre de la ciudad con la población máxima  
*/

  SELECT nombre, MAX(poblacion) FROM ciudad ;

/* 
  Consulta 5 Indicar el nombre de la ciudad con la población mínima 
*/

  SELECT nombre, MIN(poblacion) FROM ciudad ;

/* 
  Consulta 6 Indicar el número de ciudades que tengan una población por encima de la población media   
*/

  SELECT COUNT(*) AS num_ciudades FROM ciudad WHERE poblacion < (SELECT AVG(poblacion) FROM ciudad);

/* 
  Consulta 7 Indicarme el número de personas que viven en cada ciudad    
*/

  SELECT nombre, SUM(poblacion) FROM ciudad GROUP BY nombre ASC;
/* 
  Consulta 8 Utilizando la tabla trabaja indicarme cuantas personas trabajan en cada una de las compañias    
*/

  SELECT * FROM emple ORDER BY apellido DESC;

/* 
  Consulta 9 Indicarme la compañía que mas trabajadores tiene  
*/

  CREATE OR REPLACE VIEW consulta9c1 as
  SELECT t.id_compania,count(*)numero FROM trabaja t group BY t.id_compania;

  CREATE OR REPLACE VIEW consulta9c2 as
  SELECT c1.id_compania,MAX(numero)maximo FROM consulta9c1 c1;

  SELECT c2.nombre FROM consulta9c1 c1
    INNER JOIN compañia c2 ON c2.id = c1.id_compania
    JOIN consulta9c2 c USING (id_compania);

/* 
  Consulta 10 Indicarme el salario medio de cada una de las compañias    
*/

  SELECT compañia.nombre, AVG(trabaja.salario) FROM trabaja
  INNER JOIN compañia ON trabaja.id_compania = compañia.id
  GROUP BY compañia.nombre;
/* 
  Consulta 11 Listarme el nombre de las personas y la población de la ciudad donde viven   
*/

  SELECT persona.nombre, ciudad.nombre AS ciudad FROM persona
  INNER JOIN ciudad ON ciudad.id = persona.id_ciudad;

/* 
  Consulta 12 Listar el nombre de las personas, la calle donde vive y la población de la ciudad donde vive   
*/

  SELECT persona.nombre, persona.calle, ciudad.nombre AS ciudad FROM persona
  INNER JOIN ciudad ON ciudad.id = persona.id_ciudad;
/* 
  Consulta 13 Listar el nombre de las personas, la ciudad donde vive y la ciudad donde está la compañía para la que trabaja 
*/
  SELECT p.nombre, c.nombre AS ciudad, co.nombre AS empresa, c1.nombre AS vive FROM persona p
  INNER JOIN ciudad c ON p.id_ciudad = c.id
  INNER JOIN trabaja t ON p.id = t.id_persona
  INNER JOIN compañia co ON t.id_compania = co.id
  INNER JOIN ciudad c1 ON c1.id = co.id_ciudad;
  
/* 
  Consulta 14 Realizar el algebra relacional y explicar la siguiente consulta 
*/

  -- Consulta 14 Álgebra relacional en WORD
/* 
  Consulta 15 Listarme el nombre de la persona y el nombre de su supervisor  
*/

  SELECT nombre FROM persona
    UNION
    SELECT nombre FROM supervisor;
/* 
  Consulta 16 Listarme el nombre de la persona, el nombre de su supervisor y las ciudades donde residen cada una de ellos    
*/

  SELECT persona.nombre, ciudad.nombre FROM persona
    INNER JOIN ciudad ON ciudad.id = persona.id_ciudad
    UNION
    SELECT nombre, ciudad FROM supervisor;
/* 
  Consulta 17 Indicarme el número de ciudades distintas que hay en la tabla compañía   
*/

  SELECT COUNT(id_ciudad) FROM compañia;
/* 
  Consulta 18 Indicarme el número de ciudades distintas que hay en la tabla personas 
*/

  SELECT COUNT(id_ciudad) FROM persona;
/* 
  Consulta 19 Indicarme el nombre de las personas que trabajan para FAGOR   
*/

  SELECT persona.nombre, c.nombre FROM persona
  INNER JOIN trabaja t1 ON t1.id_persona = persona.id
  INNER JOIN compañia c ON c.id = t1.id_compania
  WHERE c.nombre = 'FAGOR';
/* 
  Consulta 20 Indicarme el nombre de las personas que no trabajan para FAGOR    
*/

  SELECT persona.nombre, c.nombre FROM persona
  INNER JOIN trabaja t1 ON t1.id_persona = persona.id
  INNER JOIN compañia c ON c.id = t1.id_compania
  WHERE c.nombre <> 'FAGOR';
/* 
  Consulta 21 Indicarme el número de personas que trabajan  para INDRA   
*/

  SELECT COUNT(persona.nombre) FROM persona
  INNER JOIN trabaja t1 ON t1.id_persona = persona.id
  INNER JOIN compañia c ON c.id = t1.id_compania
  WHERE c.nombre = 'INDRA';
/* 
  Consulta 22 Indicarme el nombre de las personas que trabajan para FAGOR o para INDRA   
*/

  SELECT COUNT(persona.nombre) FROM persona
  INNER JOIN trabaja t1 ON t1.id_persona = persona.id
  INNER JOIN compañia c ON c.id = t1.id_compania
  WHERE c.nombre = 'FAGOR' OR 'INDRA';  
/* 
  Consulta 23 Listar la población donde vive cada persona, sus salario, su nombre y la compañía para la que trabaja. Ordenar la salida por nombre de la persona y por salario de forma descendente   
*/

  SELECT c.nombre, t.salario, p.nombre, co.nombre FROM persona p
  INNER JOIN ciudad c ON p.id_ciudad = c.id
  INNER JOIN compañia co ON c.id = co.id_ciudad
  INNER JOIN trabaja t ON p.id = t.id_persona;