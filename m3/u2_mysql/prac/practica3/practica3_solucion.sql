﻿
-- Practica 3


/* 1- Visualizar el número de empleados de cada departamento. Utilizar GROUP BY para agrupar por departamento. */

SELECT
  dept_no,
  COUNT(*)
FROM emple
GROUP BY dept_no;

/* 2. Visualizar los departamentos con más de 5 empleados. Utilizar GROUP BY para agrupar por departamento y HAVING para
establecer la condición sobre los grupos. */

SELECT
  dept_no,
  COUNT(emp_no) empleados
FROM emple
GROUP BY dept_no
HAVING empleados > 5;

/* 3. Hallar la media de los salarios de cada departamento (utilizar la función avg y GROUP BY). */

SELECT
  dept_no,
  AVG(salario)
FROM emple
GROUP BY dept_no;

/* 4. Visualizar el nombre de los empleados vendedores del departamento ʻVENTASʼ (Nombre del departamento=ʼVENTASʼ,
oficio=ʼVENDEDORʼ). */

SELECT
  e.apellido
FROM emple e
  INNER JOIN depart d USING (dept_no)
WHERE d.dnombre = 'VENTAS'
AND e.oficio = 'VENDEDOR';

/* 5. Visualizar el número de vendedores del departamento ʻVENTASʼ (utilizar la función COUNT sobre la consulta anterior). */

SELECT
  COUNT(e.apellido)
FROM emple e
  INNER JOIN depart d USING (dept_no)
WHERE d.dnombre = 'VENTAS';

/* 6. Visualizar los oficios de los empleados del departamento ʻVENTASʼ. */

SELECT DISTINCT
  e.oficio
FROM emple e
  INNER JOIN depart d USING (dept_no)
WHERE d.dnombre = 'VENTAS';

/* 7. A partir de la tabla EMPLE, visualizar el número de empleados de cada departamento cuyo oficio sea ʻEMPLEADOʼ (utilizar
GROUP BY para agrupar por departamento. En la cláusula WHERE habrá que indicar que el oficio es ʻEMPLEADOʼ). */

SELECT
  dept_no,
  COUNT(*)
FROM emple e
  INNER JOIN depart d USING (dept_no)
WHERE e.oficio = 'VENDEDOR'
GROUP BY dept_no;

/* 8. Visualizar el departamento con más empleados. */

CREATE OR REPLACE VIEW consulta8C1
AS
SELECT
  dept_no,
  COUNT(emp_no) empleados
FROM emple e
GROUP BY dept_no;

SELECT
  d.dnombre,
  c1.*
FROM depart d
  JOIN consulta8C1 c1 USING (dept_no)
WHERE c1.empleados = (SELECT
    MAX(c1.empleados) maximo
  FROM consulta8C1 c1);

/* 9. Mostrar los departamentos cuya suma de salarios sea mayor que la media de salarios de todos los empleados. */

CREATE OR REPLACE VIEW mediaSalarios
AS
SELECT
  AVG(salario) mediaSalario
FROM emple e;

CREATE OR REPLACE VIEW sumaSalarioPorDept
AS
SELECT
  dept_no,
  SUM(salario) sumaSalario
FROM emple
GROUP BY dept_no;

SELECT
  d.dnombre
FROM depart d
  JOIN sumaSalarioPorDept spd USING (dept_no)
WHERE spd.sumaSalario > (SELECT
    AVG(salario) mediaSalario
  FROM emple e);

/* 10. Para cada oficio obtener la suma de salarios. */

SELECT
  oficio,
  SUM(salario)
FROM emple
GROUP BY oficio;


/* 11. Visualizar la suma de salarios de cada oficio del departamento ʻVENTASʼ. */

CREATE OR REPLACE VIEW deparVentas
AS
SELECT
  *
FROM depart
WHERE dnombre = 'VENTAS';

SELECT
  e.oficio,
  e.salario
FROM emple e
  JOIN deparVentas d USING (dept_no)
GROUP BY e.oficio;

/* 12. Visualizar el número de departamento que tenga más empleados cuyo oficio sea empleado. */

CREATE OR REPLACE VIEW empleados
AS
SELECT
  *
FROM emple e
WHERE e.oficio = 'EMPLEADO';

CREATE OR REPLACE VIEW numEmpleados
AS
SELECT
  e.dept_no,
  COUNT(*) empleados
FROM empleados e
GROUP BY e.dept_no
ORDER BY empleados DESC;

SELECT
  dept_no
FROM numEmpleados LIMIT 1;


/* 13. Mostrar el número de oficios distintos de cada departamento. */

SELECT
  e.oficio,
  COUNT(DISTINCT e.oficio)
FROM emple e
GROUP BY e.dept_no;


/* 14. Mostrar los departamentos que tengan más de dos personas trabajando en la misma profesión. */

SELECT
  oficio
FROM emple
GROUP BY oficio
HAVING COUNT(dept_no) > 2;

/* 15. Dada la tabla HERRAMIENTAS, visualizar por cada estantería la suma de las unidades. */

SELECT
  h.estanteria,
  SUM(h.unidades)
FROM herramientas h
GROUP BY h.estanteria;

/* 16. Visualizar la estantería con más unidades de la tabla HERRAMIENTAS. (con totales y sin totales) */

CREATE OR REPLACE VIEW unidades
AS
SELECT
  h.estanteria,
  SUM(h.unidades) sumaUnidades
FROM herramientas h
GROUP BY h.estanteria;

SELECT
  MAX(sumaUnidades)
FROM unidades u;

SELECT
  u.estanteria
FROM unidades u
WHERE u.sumaUnidades = (SELECT
    MAX(sumaUnidades)
  FROM unidades u);


/* 17. Mostrar el número de médicos que pertenecen a cada hospital, ordenado por número descendente de hospital. */

SELECT
  h.cod_hospital,
  SUM(h.num_plazas)
FROM hospitales h
GROUP BY h.cod_hospital
ORDER BY h.cod_hospital DESC;

/* 18. Realizar una consulta en la que se muestre por cada hospital el nombre de las especialidades que tiene. */

SELECT
  h.nombre,
  m.especialidad
FROM medicos m
  JOIN hospitales h USING (cod_hospital);

/* 19. Realizar una consulta en la que aparezca por cada hospital y en cada especialidad el número de médicos (tendrás que partir
de la consulta anterior y utilizar GROUP BY). */

SELECT
  h.nombre,
  COUNT(m.especialidad)
FROM medicos m
  JOIN hospitales h USING (cod_hospital)
GROUP BY h.cod_hospital,m.especialidad;

/* 20. Obtener por cada hospital el número de empleados. */

SELECT
  cod_hospital,
  SUM(num_plazas)
FROM hospitales
GROUP BY cod_hospital;

/* 21. Obtener por cada especialidad el número de trabajadores. */

SELECT
  m.especialidad,
  COUNT(m.especialidad)
FROM medicos m
  JOIN hospitales h USING (cod_hospital)
GROUP BY m.especialidad;

/* 22. Visualizar la especialidad que tenga más médicos. */

CREATE OR REPLACE VIEW numEspecialistas
AS
SELECT
  m.especialidad,
  COUNT(m.especialidad) numEspecialista
FROM medicos m
  JOIN hospitales h USING (cod_hospital)
GROUP BY m.especialidad;

SELECT
  e.especialidad
FROM numEspecialistas e
WHERE e.numEspecialista = (SELECT
    MAX(numEspecialista)
  FROM numEspecialistas e1);

/* 23. ¿Cuál es el nombre del hospital que tiene mayor número de plazas? */

CREATE OR REPLACE VIEW maxPlazas
AS
SELECT
  MAX(num_plazas)
FROM hospitales h;

SELECT
  nombre
FROM hospitales h
WHERE h.num_plazas = (SELECT
    MAX(num_plazas)
  FROM hospitales h);

/* 24. Visualizar las diferentes estanterías de la tabla HERRAMIENTAS ordenados descendentemente por estantería. */

SELECT
  h.estanteria
FROM herramientas h
ORDER BY h.estanteria DESC;

/* 25. Averiguar cuántas unidades tiene cada estantería. */

SELECT
  h.estanteria,
  SUM(h.unidades)
FROM herramientas h
GROUP BY h.estanteria;

/* 26. Visualizar las estanterías que tengan más de 15 unidades */

CREATE OR REPLACE VIEW estanterias
AS
SELECT
  h.estanteria,
  SUM(h.unidades) totalUnidades
FROM herramientas h
GROUP BY h.estanteria;

SELECT
  *
FROM estanterias e
WHERE e.totalUnidades > 15;


/* 27. ¿Cuál es la estantería que tiene más unidades? */

CREATE OR REPLACE VIEW sumaUnidades
AS
SELECT
  h.estanteria,
  SUM(h.unidades) unidades
FROM herramientas h
GROUP BY h.estanteria;

SELECT
  su.estanteria
FROM sumaUnidades su
WHERE su.unidades = (SELECT
    MAX(su.unidades) numMaximo
  FROM sumaUnidades su);

/* 28. A partir de las tablas EMPLE y DEPART mostrar los datos del departamento que no tiene ningún empleado. */

SELECT
  d.dept_no,
  d.dnombre
FROM emple e
  RIGHT JOIN depart d
    ON d.dept_no = e.dept_no
WHERE e.dept_no IS NULL;


/* 29. Mostrar el número de empleados de cada departamento. En la salida se debe mostrar también los departamentos que no
tienen ningún empleado. */

CREATE OR REPLACE VIEW contarEmpleados
AS
SELECT
  dept_no,
  COUNT(*) n
FROM emple
GROUP BY dept_no;

SELECT
  dnombre,
  IFNULL(n, 0) NUMERO_DE_EMPLEADOS
FROM depart
  LEFT JOIN contarEmpleados c1 USING (dept_no);

/* 30. Obtener la suma de salarios de cada departamento, mostrando las columnas DEPT_NO, SUMA DE SALARIOS y DNOMBRE.
En el resultado también se deben mostrar los departamentos que no tienen asignados empleados. */

CREATE OR REPLACE VIEW contarEmpleados
AS
SELECT
  dept_no,
  SUM(salario) sumaSalario
FROM emple
GROUP BY dept_no;

SELECT
  dnombre,
  IFNULL(sumaSalario, 0) NUMERO_DE_EMPLEADOS
FROM depart
  LEFT JOIN contarEmpleados c1 USING (dept_no);

/* 31. Utilizar la función IFNULL en la consulta anterior para que en el caso de que un departamento no tenga empleados, aparezca
como suma de salarios el valor 0. */

CREATE OR REPLACE VIEW contarEmpleados
AS
SELECT
  dept_no,
  SUM(salario) sumaSalario
FROM emple
GROUP BY dept_no;

SELECT
  dnombre,
  IFNULL(sumaSalario, 0) NUMERO_DE_EMPLEADOS
FROM depart
  LEFT JOIN contarEmpleados c1 USING (dept_no);

/* 32. Obtener el número de médicos que pertenecen a cada hospital, mostrando las columnas COD_HOSPITAL, NOMBRE y NÚMERO DE MÉDICOS.
  En el resultado deben aparecer también los datos de los hospitales que no tienen médicos.: */

SELECT
  h.cod_hospital COD_HOSPITAL,
  h.nombre NOMBRE,
  SUM(h.num_plazas) NÚMERO_DE_MEDICOS
FROM medicos m
  INNER JOIN hospitales h
    ON h.cod_hospital = m.cod_hospital
GROUP BY COD_HOSPITAL;
  


