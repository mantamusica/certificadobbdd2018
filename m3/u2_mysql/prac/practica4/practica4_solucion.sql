﻿
-- Practica 3


/* 1- Averigua el dni de todos los clientes. */

SELECT
  dni,
  nombre
FROM cliente c;

/* 2. Consulta todos los datos de todos los programas. */

SELECT
  *
FROM programa;

/* 3. Listado con el nombre de todos los programas. */

SELECT
  nombre
FROM programa;

/* 4. Lista con todos los comercios. */

SELECT DISTINCT
  nombre
FROM comercio;

/* 5. Lista de las ciudades con establecimientos donde se venden programas, sin que aparezcan valores duplicados(utiliza DISTINCT). */

SELECT DISTINCT
  ciudad
FROM comercio;

/* 6. Lista con los nombres de programas, sin que aparezcan valores duplicados. */

SELECT DISTINCT
  nombre
FROM programa;

/* 7. Obtén el dni más 4 de todos los clientes. */

SELECT
  (dni + 4) Sumamos4
FROM cliente;

/* 8. Listado con los códigos de los programas multiplicados por 7. */

SELECT
  (codigo * 7) Multiplicamos7
FROM programa;

/* 9. Cuáles son los programas cuyo código es inferior o igual a 10. */

SELECT
  *
FROM programa
WHERE codigo < 7
OR codigo = 10;

/* 10. Programa cuyo código es 11. */

SELECT
  *
FROM programa
WHERE codigo LIKE 11;

/* 11. Fabricantes de EEUU. */

SELECT
  *
FROM fabricante
WHERE pais = 'Estados Unidos';

/* 12. Fabricantes no españoles, utilizando operador IN. */

SELECT
  *
FROM fabricante
WHERE pais NOT IN ('España');

/* 13. Listado con los códigos de las distintas versiones de Windows. */

SELECT
  codigo
FROM programa
WHERE nombre IN ('windows');

/* 14. En que ciudades comercializa programas el corte ingles. */

SELECT
  ciudad
FROM comercio
WHERE nombre IN ('El Corte Inglés');

/* 15. Que otros comercios hay, además de el corte ingles utilizar IN. */

SELECT
  *
FROM comercio
WHERE nombre NOT IN ('El Corte Inglés');

/* 16. Genera una lista con los códigos de las distintas versiones de windows y access, usando IN */

SELECT
  codigo
FROM programa
WHERE nombre IN ('windows', 'access');

/* 17. Obtén un listado que incluya los nombres de los clientes de edades comprendidas entre 10 y 25 y de los mayores 
  de 50 años usando Between y sin Between. */

SELECT
  *
FROM cliente c
WHERE (c.edad BETWEEN 10 AND 25)
AND edad > 50;

SELECT
  *
FROM cliente c
WHERE (edad >= 10
AND edad <= 25)
AND edad > 50;

/* 18. Lista los comercios de Sevilla y Madrid sin duplicar . */

SELECT DISTINCT
  nombre
FROM comercio
WHERE ciudad = 'Sevilla'
OR ciudad = 'Madrid';

/* 19. Clientes que terminan su nombre en la letra o. */

SELECT
  *
FROM cliente
WHERE nombre = '%o';

/* 20. Clientes que terminar su nombre en la letra o y ademas son mayores de 30 años. */

SELECT
  *
FROM cliente
WHERE nombre = '%o'
AND edad > 30;

/* 21. Listado en el que aparezcan los programas cuya versión finalice por una letra i o cuyo nombre comience por una A o por una W. */

SELECT
  *
FROM programa
WHERE version = '%i'
OR nombre = 'A%'
OR nombre = 'W%';

/* 22. Listado en el que aparezcan los programas cuya version finalice por una letra i o cuyo nombre comience por una A y termine por una S. */

SELECT
  *
FROM programa
WHERE version = '%i'
OR (nombre = 'A%'
AND nombre = '%s');

/* 23. Listado en el que aparezcan los programas cuya version finalice por una letra i y cuyo nombre comience por una A. */

SELECT
  *
FROM programa
WHERE version = '%i'
AND nombre = 'A%';

/* 24. Lista de empresas por orden alfabético ascendente. */

SELECT
  nombre
FROM fabricante
UNION
SELECT
  nombre
FROM comercio
ORDER BY nombre ASC;

/* 25. Lista de empresas por orden alfabético descendente. */

SELECT
  nombre
FROM fabricante
UNION
SELECT
  nombre
FROM comercio
ORDER BY nombre DESC;

/* 26. Lista de programas por orden de version. */

SELECT
  *
FROM programa
ORDER BY version;

/* 27. Lista de programas de Oracle.  */

SELECT
  *
FROM programa
  JOIN desarrolla USING (codigo)
  JOIN fabricante USING (id_fab)
WHERE fabricante.nombre = 'Oracle';

/* 28. Comercios que distribuyen Windows.  */

SELECT
  *
FROM programa
  JOIN distribuye USING (codigo)
  JOIN comercio USING (cif)
WHERE programa.nombre = 'Windows';

/* 29. Listado de programas y cantidades que se han distribuido a El Corte Inglés de Madrid. */

SELECT
  programa.nombre,
  cantidad
FROM programa
  JOIN distribuye USING (codigo)
  JOIN comercio USING (cif)
WHERE comercio.nombre = 'El Corte Inglés'
AND comercio.ciudad = 'Sevilla';

/* 30. Fabricante que ha desarrollado Freddy Hardest. */

SELECT
  fabricante.nombre
FROM programa
  JOIN desarrolla USING (codigo)
  JOIN fabricante USING (id_fab)
WHERE programa.nombre = 'Freddy Hardest';

/* 31. Selecciona el nombre de los programas que se registran por Internet. */

SELECT
  p.nombre
FROM programa p
  JOIN registra r
    ON p.codigo = r.codigo
WHERE r.medio = 'Internet';

/* 32. Selecciona el nombre de las personas que se registran por Internet. */

SELECT
  c.nombre
FROM cliente c
  JOIN registra r
    ON c.dni = r.dni
WHERE r.medio = 'Internet';

/* 33. Medios que ha utilizado para registrarse Pepe Pérez. */

SELECT
  r.medio
FROM registra r
  JOIN cliente c
    ON r.dni = c.dni
WHERE c.nombre = 'Pepe Pérez';

/* 34. Usuarios que han optado por Internet como medio de registro. */

SELECT
  c.nombre
FROM cliente c
  JOIN registra r
    ON c.dni = r.dni
WHERE r.medio = 'Internet';

/* 35. Programas que han recibido registros por tarjeta postal. */

SELECT
  p.nombre
FROM programa p
  JOIN registra r
    ON p.codigo = r.codigo
WHERE r.medio = 'Tarjeta Postal';

/* 36. Localidades en las que se han vendido productos que se han registrado por Internet. */

SELECT
  c.ciudad
FROM comercio c
  JOIN registra r
    ON c.cif = r.cif
WHERE r.medio = 'Internet';

/* 37. Listado de los nombres de las personas que se han registrado por internet, 
  junto al nombre de los programas para los que ha efectuado el registro */

SELECT
  c.nombre,
  p.nombre
FROM cliente c
  JOIN registra r
    ON c.dni = r.dni
  JOIN programa p
    ON r.codigo = p.codigo
WHERE r.medio = 'Internet';

/* 38. Listado en el que aparezca cada cliente junto al programa que ha registrado, el medio con el que
  lo ha hecho y el comercio en el que lo ha adquirido.*/

SELECT
  c.nombre,
  r.medio,
  c.nombre
FROM cliente c
  JOIN registra r
    ON c.dni = r.dni
  JOIN comercio c1
    ON r.cif = c1.cif;

/* 39. Listado con las ciudades en las que se pueden obtener los productos de Oracle. */

SELECT DISTINCT
  c.ciudad
FROM comercio c
  JOIN distribuye d
    ON c.cif = d.cif
  JOIN programa p
    ON d.codigo = p.codigo
  JOIN desarrolla d1
    ON p.codigo = d1.codigo
  JOIN fabricante f
    ON d1.id_fab = f.id_fab
WHERE f.nombre = 'Oracle';

/* 40. Obtén el nombre de los usuarios que han registrado Access Xp. */

SELECT
  c.nombre
FROM cliente c
  JOIN registra r
    ON c.dni = r.dni
  JOIN programa p
    ON r.codigo = p.codigo
WHERE p.nombre = 'Access'
AND p.version = 'Xp';

/* 41. Nombre de aquellos fabricantes cuyo pais es el mismo que Oracle. */

SELECT
  f.pais
FROM fabricante f
WHERE f.nombre = 'Oracle';

SELECT
  *
FROM fabricante f
WHERE f.pais = (SELECT
    f.pais
  FROM fabricante f
  WHERE f.nombre = 'Oracle')
AND f.nombre != 'Oracle';

/* 42. Nombre de aquellos clientes que tienen la misma edad que Pepe Pérez.*/

SELECT
  c.edad
FROM cliente c
WHERE c.nombre = 'Pepe Pérez';

SELECT
  *
FROM cliente c
WHERE c.edad = (SELECT
    c.edad
  FROM cliente c
  WHERE c.nombre = 'Pepe Pérez')
AND c.nombre <> 'Pepe Pérez';

/* 43. Listado con los comercios que tienen su sede en la misma ciudad que tiene el comercio FNAC. */

SELECT
  c.ciudad
FROM comercio c
WHERE c.nombre = 'FNAC';

SELECT
  *
FROM comercio c
WHERE c.ciudad LIKE (SELECT
    c.ciudad
  FROM comercio c
  WHERE c.nombre = 'FNAC');

/* 44. Nombre de aquellos clientes que han registrado un producto de la misma forma que el cliente Pepe Pérez. */

SELECT
  dni
FROM cliente c
WHERE c.nombre = 'Pepe Pérez';

CREATE OR REPLACE VIEW consulta44c1
AS
SELECT
  r.cif,r.dni,r.codigo,r.medio
FROM registra r
WHERE r.dni = (SELECT
    dni
  FROM cliente c
  WHERE c.nombre = 'Pepe Pérez');

CREATE OR REPLACE VIEW consulta44c2
AS
SELECT
  r.*
FROM registra r
  LEFT JOIN consulta44c1 c1 USING (dni)
WHERE c1.codigo IS NULL;

SELECT DISTINCT
  c.dni,
  c.nombre,
  c.edad
FROM cliente c
  JOIN consulta44c2 c1
    ON c.dni = c1.dni;

/* 45. Número de programas que hay en la tabla programas. */

SELECT
  COUNT(*)
FROM programa p;

/* 46. Número de clientes cuya edad es mayor de 40. */

SELECT
  COUNT(*)
FROM cliente c
WHERE c.edad > 40;

/* 47. Número de productos que ha vendido el establecimiento cuyo cif es 1. */

SELECT
  SUM(cantidad)
FROM comercio c
  JOIN distribuye d USING (cif)
WHERE d.cif = 1;


/* 48. Media de programas que se venden cuyo codigo es 7. */

SELECT
  AVG(cantidad)
FROM comercio c
  JOIN distribuye d USING (cif)
WHERE d.codigo = 7;


/* 49. Minima cantidad de programas de codigo 7 que se ha vendido. */

SELECT
  MIN(cantidad)
FROM comercio c
  JOIN distribuye d USING (cif)
WHERE d.codigo = 7;


/* 50. Maxima cantidad de programas de codigo 7 que se ha vendido. */

SELECT
  MAX(cantidad)
FROM comercio c
  JOIN distribuye d USING (cif)
WHERE d.codigo = 7;

/* 51. En cuantos establecimientos se vende el programa cuyo codigo es 7. */

SELECT
  COUNT(*)
FROM comercio c
  JOIN distribuye d USING (cif)
WHERE d.codigo = 7;

/* 52. Numero de registros que se han realizado por internet. */

SELECT
  COUNT(*)
FROM registra r
WHERE r.medio = 'Internet';

/* 53. Numero total de programas que se han vendido en Sevilla. */

SELECT
  *
FROM distribuye d
  JOIN comercio c USING (cif)
WHERE c.ciudad = 'Sevilla';

/* 54. Numero total de programas que han desarrollado los fabricantes cuyo pais es EEUU. */

SELECT
  COUNT(*)
FROM desarrolla d
  JOIN fabricante f USING (id_fab)
WHERE f.pais = 'Estados Unidos';

/* 55. Nombre de todos los clientes en mayusculas. en el resultado de la consulta debe aparecer tambien la longitud de la cadena nombre.*/

SELECT
  UPPER(c.nombre),
  CHAR_LENGTH(c.nombre)
FROM cliente c;

/* 56. Con una consulta concatena los campos de nombre y version de la tabla programa.*/

SELECT
  CONCAT(p.nombre, ' ', p.version) concatenados
FROM programa p;



