﻿-- consulta 1

SELECT
  c.nombre
FROM miembro m
  JOIN composicion c USING (dni)
WHERE c.cargo = 'Converted';

-- consulta 2

SELECT
  f.direccion
FROM federacion f
  JOIN composicion c USING (nombre)
WHERE c.cargo = 'Disqualified';

-- consulta 3

CREATE OR REPLACE VIEW consulta3c1
AS
SELECT
  *
FROM federacion f;

CREATE OR REPLACE VIEW consulta3c2
AS
SELECT
  *
FROM composicion c
WHERE c.cargo = 'Trial';

SELECT
  *
FROM consulta3c1 c1
  LEFT JOIN consulta3c2 c2 ON c1.nombre = c2.nombre
WHERE c2.nombre IS NULL;

-- consulta 4

CREATE OR REPLACE VIEW consulta4c1
AS
SELECT
  c.nombre, c.cargo
FROM composicion c;

CREATE OR REPLACE VIEW consulta4c2
AS
SELECT
  DISTINCT c.cargo
FROM composicion c;

-- division 

SELECT DISTINCT c1.nombre FROM 
consulta4c1 c1
WHERE c1.nombre NOT IN
  (SELECT
    c2.cargo
  FROM consulta4c2 c2);



-- consulta 5

CREATE OR REPLACE VIEW consulta5c1
AS
SELECT
  c.nombre
FROM composicion c
WHERE c.cargo = 'Trial';

CREATE OR REPLACE VIEW consulta5c2
AS
SELECT
  c.nombre
FROM composicion c
WHERE c.cargo = 'Disqualified';

-- intersección

SELECT DISTINCT
  nombre
FROM consulta5c1
  INNER JOIN consulta5c2 USING (nombre);