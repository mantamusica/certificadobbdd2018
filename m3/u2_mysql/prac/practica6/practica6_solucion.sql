﻿-- consulta 1

  CREATE OR REPLACE VIEW consulta1c1 AS
  SELECT * FROM tema t
    WHERE t.descripcion = 'Ea inventore iure ut quisquam a voluptatem.';

  CREATE OR REPLACE VIEW consulta1c2 AS
  SELECT * FROM articulo a
    WHERE a.año > 1990;

  SELECT c1.tituloArt FROM consulta1c1 c NATURAL JOIN consulta1c2 c1;

-- consulta 2 
  
  CREATE OR REPLACE VIEW consulta2c1 AS
  SELECT a.referencia,a.codTema FROM articulo a;

  CREATE OR REPLACE VIEW consulta2c2 AS
  SELECT t.codTema FROM tema t;

  CREATE OR REPLACE VIEW consulta2c3Division AS
  SELECT DISTINCT c1.referencia FROM 
    consulta2c1 c1
  WHERE c1.codTema NOT IN
    (SELECT
      c2.codTema
    FROM consulta2c2 c2);

  CREATE OR REPLACE VIEW consulta2c4Join AS
  SELECT r.tituloRev,r.referencia FROM revista r JOIN consulta2c3Division d USING(referencia);

  SELECT c.tituloRev FROM consulta2c4Join c;

-- consulta 3
  -- resta

CREATE OR REPLACE VIEW consulta3c1 AS
  SELECT * FROM articulo a;

CREATE OR REPLACE VIEW consulta3c2 AS
  SELECT * FROM tema t
       NATURAL JOIN consulta3c1
      WHERE t.descripcion <> 'Ea inventore iure ut quisquam a voluptatem.';

  CREATE OR REPLACE VIEW consulta3c3 AS
  SELECT r.referencia FROM revista r
    LEFT JOIN consulta3c2 c2
    ON r.referencia = c2.referencia;

  SELECT DISTINCT r.tituloRev FROM revista r NATURAL JOIN consulta3c3 ORDER BY r.tituloRev;

  -- consulta 4

CREATE OR REPLACE VIEW consulta4c1 AS
SELECT * FROM autor a;

CREATE OR REPLACE VIEW consulta4c2 AS
  SELECT a.dni FROM articulo a WHERE a.año=1991;

CREATE OR REPLACE VIEW consulta4c3 AS
  SELECT * FROM tema t WHERE t.descripcion = 'Ea inventore iure ut quisquam a voluptatem.';

SELECT c1.nombre FROM consulta4c1 c1 NATURAL JOIN consulta4c2 c2 NATURAL JOIN consulta4c3 c3;

-- consulta 5

  CREATE OR REPLACE VIEW consulta5c1 AS
  SELECT * FROM articulo a WHERE a.año = 1993;

  CREATE OR REPLACE VIEW consulta5c2 AS
  SELECT * FROM autor a WHERE a.universidad = 'Politécnica de Madrid';

  SELECT c1.tituloArt FROM consulta5c1 c1 JOIN consulta5c2 c USING(dni);