﻿/* 1 listar todos los lenguajes oficiales de España(España es el LocalName); */

CREATE OR REPLACE VIEW codigoEspaña
AS
SELECT
  c.code
FROM country c
WHERE c.localName = 'España'
AND c.is;

SELECT
  c.language
FROM countrylanguage c
  INNER JOIN codigoEspaña c1
    ON c.countryCode = c1.code
WHERE c.isOfficial = 'T';

-- 2 Indicar todos los lenguajes de España.

SELECT
  c.language
FROM countrylanguage c
  INNER JOIN codigoEspaña c1
    ON c.countryCode = c1.code;


/* 3 indicar la poblacion media por continente y región(ordenándolo por ambos campos) */

CREATE OR REPLACE VIEW pobContinent
AS
SELECT
  c.continent dato,
  SUM(c.population) poblaciónTotal
FROM country c
GROUP BY continent
ORDER BY c.continent;

CREATE OR REPLACE VIEW pobRegion
AS
SELECT
  c.region dato,
  SUM(c.population) poblaciónTotal
FROM country c
GROUP BY continent
ORDER BY c.region;

SELECT
  *
FROM (pobContinent)
UNION
SELECT
  *
FROM (pobRegion);

/* 4 Indicar el nombre de todas las ciudades del continente cuya población media es la mayor */

  CREATE OR REPLACE VIEW continentPobMedia AS
  SELECT continent, AVG(population) pobMedia FROM country GROUP BY continent;

  CREATE OR REPLACE VIEW continentMaxPobMedia AS
  SELECT pm.continent, MAX(pm.pobMedia) FROM continentPobMedia pm;

  CREATE OR REPLACE VIEW codeContinentMaxPobMedia AS
  SELECT DISTINCT c.code FROM country c JOIN continentMaxPobMedia mpm USING(continent);

  SELECT c.name FROM country c JOIN codeContinentMaxPobMedia ccmpb USING(code);


/* 5 Indicar la población media de las ciudades del continente cuyos paises tengan la esperanza de vida media mayor */

  CREATE OR REPLACE VIEW continenteEsperanzaVidaMedia AS
  SELECT c.continent, AVG(c.lifeExpectancy) mediaEsperanzaVida FROM country c GROUP BY c.continent;

  CREATE OR REPLACE VIEW continEVMediaMayor AS
  SELECT evm.continent, MAX(evm.mediaEsperanzaVida) maximoEV FROM continenteEsperanzaVidaMedia evm;

  CREATE OR REPLACE VIEW paisesContEVMediaMayor AS
  SELECT c.code FROM country c INNER JOIN continEVMediaMayor em ON c.continent= em.continent;

  CREATE OR REPLACE VIEW paisPobpaisesContEVMediaMayor AS
  SELECT c.name, c.population poblacion FROM city c INNER JOIN paisesContEVMediaMayor cem ON c.countryCode=cem.code;

  SELECT AVG(pcem.poblacion) PoblacionMedia FROM paisPobpaisesContEVMediaMayor pcem;

/* 6 Indicar la población y el nombre de las capitales de cada uno de los paises. Además, debemos indicar el país
  del cual es capital y el nombre de continente al que pertenece. */

  SELECT c.name País,c1.name Capital,c.population Población, c.continent Continente  FROM country c
  INNER JOIN city c1 ON c.capital=c1.id GROUP BY c.name;

/* 7 Indicar el lenguaje en cada pais cuyo porcentaje se el mayor(percentage). */

  SELECT * FROM countrylanguage c;

  CREATE OR REPLACE VIEW countryCodeMaxPercentaje AS
  SELECT c.countryCode codigo, MAX(c.percentage) maximo FROM countrylanguage c GROUP BY c.countryCode;
  
  CREATE OR REPLACE VIEW codigoPaisLenguajeMayorPais AS
  SELECT c.countryCode code, c.language FROM countrylanguage c JOIN countryCodeMaxPercentaje cmp ON c.countryCode = cmp.codigo
  WHERE c.percentage = cmp.maximo;

  SELECT c.name, plmp.language FROM country c JOIN codigoPaisLenguajeMayorPais plmp USING(code);

/* 8 Indicar los continentes que mayor número de lenguas se hablen. */

  CREATE OR REPLACE VIEW lenguajesPorContinentes As
  SELECT c.continent, COUNT(c1.language) numero FROM country c INNER JOIN countrylanguage  c1 ON c.code = c1.countryCode GROUP BY c.continent; 

  CREATE OR REPLACE VIEW maximoLenguajesContinente AS
  SELECT MAX(pc.numero) maximo FROM lenguajesPorContinentes pc;

  SELECT pc.continent FROM lenguajesPorContinentes pc 
    WHERE pc.numero = (SELECT lc.maximo FROM maximoLenguajesContinente lc);


/* 9 Indicar los paises que menor número de lenguas se hablen. */

CREATE OR REPLACE VIEW numeroLenguajes
AS
SELECT
  c.language lengua,
  COUNT(c.language) lenguajes
FROM countrylanguage c
GROUP BY c.language;

CREATE OR REPLACE VIEW minimo
AS
SELECT
  MIN(lenguajes) minimo
FROM numeroLenguajes l;

CREATE OR REPLACE VIEW lenguajesUnicoPais
AS
SELECT
  l.lengua language
FROM numeroLenguajes l
WHERE l.lenguajes = (SELECT
    m.minimo
  FROM minimo m);

CREATE OR REPLACE VIEW codigosPaisesLenguaUnica
AS
SELECT DISTINCT
  c.countryCode code
FROM countrylanguage c
  JOIN lenguajesUnicoPais up USING (language);

SELECT
  c.name
FROM country c
  JOIN codigosPaisesLenguaUnica plu USING (code);

/* 10 indicar los paises que tengan el mismo número de lenguas que España */

CREATE OR REPLACE VIEW numLenguasEspaña
AS
SELECT
  COUNT(c.language) numero
FROM countrylanguage c
  INNER JOIN codigoEspaña c1
    ON c.countryCode = c1.code;

CREATE OR REPLACE VIEW numLenguasXPais
AS
SELECT
  c.language,
  COUNT(c.language) numero
FROM countrylanguage c
GROUP BY c.language;

CREATE OR REPLACE VIEW lenguajes4lenguas
AS
SELECT
  language
FROM numLenguasXPais lx
WHERE lx.numero = (SELECT
    le.numero
  FROM numLenguasEspaña le);

CREATE OR REPLACE VIEW countryCode4lenguas
AS
SELECT DISTINCT
  c.countryCode code
FROM countrylanguage c
  JOIN lenguajes4lenguas l USING (language);

SELECT
  c.name
FROM country c
  JOIN countryCode4lenguas USING (code);
      
