﻿/* 1 Número de habitaciones por cada hotel (solo indicar el número del hotel) */

CREATE OR REPLACE VIEW numHabXHotel
AS
SELECT
  th.NUM_HOTEL,
  SUM(th.CANTIDAD) numeroHabitaciones
FROM tipo_habitacion th
GROUP BY th.NUM_HOTEL;

/* 2 Número de habitaciones por cada hotel (indicar el nombre y la dirección del hotel)  */

CREATE OR REPLACE VIEW hotelesHabitaciones
AS
SELECT
  h.nombre,
  h.DOMICILIO,
  hxh.numeroHabitaciones
FROM hotel h
  JOIN numHabXHotel hxh USING (NUM_HOTEL);

/* 3 Indicar el nombre del hotel con el mayor numero de habitaciones  */

CREATE OR REPLACE VIEW maximoHabitaciones
AS
SELECT
  MAX(hx.numeroHabitaciones) maximo
FROM numHabXHotel hx;

CREATE OR REPLACE VIEW hotesConMasHabitaciones
AS
SELECT
  h.nombre
FROM hotelesHabitaciones h
WHERE h.numeroHabitaciones = (SELECT
    h1.maximo
  FROM maximoHabitaciones h1);

/* 4 Indicar el nombre del hotel (o hoteles) con el segundo mayor numero de habitaciones (es 12 y no 15)  */

CREATE OR REPLACE VIEW segundoLugarMaxNumHab
AS
SELECT
  MAX(h.numeroHabitaciones) segundoLugar
FROM hotelesHabitaciones h
WHERE h.numeroHabitaciones <> (SELECT
    h1.maximo
  FROM maximoHabitaciones h1);

CREATE OR REPLACE VIEW hotelesSegMaxNumHabt
AS
SELECT
  h.nombre
FROM hotelesHabitaciones h
WHERE h.numeroHabitaciones = (SELECT
    lmnh.segundoLugar
  FROM segundoLugarMaxNumHab lmnh);

/* 5 Indicar el nombre de los hoteles que todavia no tenemos habitaciones introducidas  */

CREATE OR REPLACE VIEW codHotelHabitacionesCero
AS
SELECT
  *
FROM numHabXHotel h
WHERE h.numeroHabitaciones = 0;

CREATE OR REPLACE VIEW nombreHotelesHabCero
AS
SELECT
  h.nombre
FROM hotel h
  JOIN codHotelHabitacionesCero th
    ON h.NUM_HOTEL = th.NUM_HOTEL;

/* 6 Indicar el nombre del hotel del cual no se han reservado nunca habitaciones
  (teniendo en cuenta solamente los hoteles de los cuales hemos introducido sus habitaciones). . */

CREATE OR REPLACE VIEW hotHabtIntroducidas
AS
SELECT
  *
FROM numHabXHotel h
WHERE h.numeroHabitaciones <> 0;

CREATE OR REPLACE VIEW numHabhotHabtIntroducidas
AS
SELECT DISTINCT
  th.NUM_TIPOHAB
FROM tipo_habitacion th
  JOIN hotHabtIntroducidas hi USING (NUM_HOTEL);




/* 7 Indicar el mes que mas reservas han comenzado.  */

CREATE OR REPLACE VIEW numReservasPorFechaIni
AS
SELECT
  MONTH(r.FECHA_INI) mes,
  SUM(cantidad) reservas
FROM reserva r
GROUP BY MONTH(r.FECHA_INI);

CREATE OR REPLACE VIEW numMaxReservas
AS
SELECT
  MAX(nrpfi.reservas) maximo
FROM numReservasPorFechaIni nrpfi;

CREATE OR REPLACE VIEW mesMasReservas
AS
SELECT
  rpfi.mes
FROM numReservasPorFechaIni rpfi
WHERE rpfi.reservas = (SELECT
    nmr.maximo
  FROM numMaxReservas nmr);

/* 8 Listar el num_hotel y los distintos meses en que se han comenzado reservas . */

  CREATE OR REPLACE VIEW mesesReservas AS
  SELECT r.NUM_TIPOHAB, r.FECHA_INI FROM reserva r;

  SELECT DISTINCT th.NUM_HOTEL, DATE_FORMAT(r.FECHA_INI,'%M') meses FROM tipo_habitacion th JOIN mesesReservas r USING(NUM_TIPOHAB) ORDER BY th.NUM_HOTEL;


/* 9 Listar los nombres de los hoteles que han comenzado reservas en todos los meses
  que han comenzado reservas el hotel numero 4 . */

  CREATE OR REPLACE VIEW numHotelHotel4 AS
  SELECT * FROM tipo_habitacion th WHERE th.NUM_HOTEL = 4;

  CREATE OR REPLACE VIEW mesesHtel4 AS
  SELECT MONTH(r.fecha_INI) meses FROM reserva r JOIN numHotelHotel4 hh USING(NUM_TIPOHAB);

  CREATE OR REPLACE VIEW reservasYfechaIni AS
  SELECT r.NUM_RESERVA, MONTH(r.FECHA_INI) meses FROM reserva r; 

  CREATE OR REPLACE VIEW numResMesHotel4 AS
  SELECT yi.num_reserva FROM reservasYfechaIni yi JOIN mesesHtel4 h USING(meses);

  CREATE OR REPLACE VIEW numTipHabnumResMesHotel4 as
  SELECT * FROM reserva r JOIN numResMesHotel4 rmh USING(num_reserva);

  CREATE OR REPLACE VIEW numHotelnumTipHabnumResMesHotel4 as
  SELECT th.NUM_HOTEL FROM tipo_habitacion th JOIN numTipHabnumResMesHotel4 USING(NUM_TIPOHAB);

  SELECT DISTINCT h.NOMBRE FROM hotel h JOIN numHotelnumTipHabnumResMesHotel4 hthrmh USING(NUM_HOTEL);


/* 10 Listar los días totales que se han reservado cada una de las habitaciones.
  Indicar en el listado el nombre del hotel al que pertenece */

  CREATE OR REPLACE VIEW diasTotalesHab AS
  SELECT r.NUM_TIPOHAB,SUM(DATEDIFF(r.FECHA_FIN,r.FECHA_INI)) dias FROM reserva r GROUP BY r.NUM_TIPOHAB;

  CREATE OR REPLACE VIEW diasNumHotel AS
  SELECT th.NUM_HOTEL,th1.dias FROM tipo_habitacion th JOIN diasTotalesHab th1 USING(NUM_TIPOHAB);

  SELECT h.NOMBRE, nh.dias FROM hotel h JOIN diasNumHotel nh USING(NUM_HOTEL);

/* 11 Indicar el nombre de los 2 hoteles que mayor número de habitaciones
  se han reservado durante el primer trimestre de cualquier año  */

  CREATE OR REPLACE VIEW datosPrimerTrimestre AS
  SELECT * FROM reserva r WHERE QUARTER(r.FECHA_INI) = 1;

  CREATE OR REPLACE VIEW sumaNumHabt AS
  SELECT pt.num_tipohab, SUM(pt.cantidad) habitaciones FROM datosPrimerTrimestre pt GROUP BY pt.num_tipohab;

  CREATE OR REPLACE VIEW dosNumHotel AS
  SELECT th.NUM_HOTEL, SUM(nh.habitaciones) habitacionesHotel FROM tipo_habitacion th JOIN sumaNumHabt nh USING(NUM_TIPOHAB) GROUP BY th.NUM_HOTEL ORDER BY habitacionesHotel DESC LIMIT 2;

  SELECT h.NOMBRE,nh.habitacionesHotel FROM hotel h JOIN dosNumHotel nh USING(NUM_HOTEL);

  
   


