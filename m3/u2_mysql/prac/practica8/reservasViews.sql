﻿--
-- Script was generated by Devart dbForge Studio for MySQL, Version 8.0.40.0
-- Product home page: http://www.devart.com/dbforge/mysql/studio
-- Script date 26/01/2019 10:16:45
-- Server version: 5.5.5-10.1.30-MariaDB
-- Client version: 4.1
--

-- 
-- Disable foreign keys
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Set SQL mode
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Set character set the client will use to send SQL statements to the server
--
SET NAMES 'utf8';

--
-- Set default database
--
USE reservas;

--
-- Drop view `mesesreservas`
--
DROP VIEW IF EXISTS mesesreservas CASCADE;

--
-- Drop view `diasnumhotel`
--
DROP VIEW IF EXISTS diasnumhotel CASCADE;

--
-- Drop view `diastotaleshab`
--
DROP VIEW IF EXISTS diastotaleshab CASCADE;

--
-- Drop view `dosnumhotel`
--
DROP VIEW IF EXISTS dosnumhotel CASCADE;

--
-- Drop view `sumanumhabt`
--
DROP VIEW IF EXISTS sumanumhabt CASCADE;

--
-- Drop view `datosprimertrimestre`
--
DROP VIEW IF EXISTS datosprimertrimestre CASCADE;

--
-- Drop view `numhotelnumtiphabnumresmeshotel4`
--
DROP VIEW IF EXISTS numhotelnumtiphabnumresmeshotel4 CASCADE;

--
-- Drop view `numtiphabnumresmeshotel4`
--
DROP VIEW IF EXISTS numtiphabnumresmeshotel4 CASCADE;

--
-- Drop view `numresmeshotel4`
--
DROP VIEW IF EXISTS numresmeshotel4 CASCADE;

--
-- Drop view `reservasyfechaini`
--
DROP VIEW IF EXISTS reservasyfechaini CASCADE;

--
-- Drop view `meseshtel4`
--
DROP VIEW IF EXISTS meseshtel4 CASCADE;

--
-- Drop view `numhotelhotel4`
--
DROP VIEW IF EXISTS numhotelhotel4 CASCADE;

--
-- Set default database
--
USE reservas;

--
-- Create view `numhotelhotel4`
--
CREATE
DEFINER = 'root'@'localhost'
VIEW numhotelhotel4
AS
SELECT
  `th`.`NUM_TIPOHAB` AS `NUM_TIPOHAB`,
  `th`.`NUM_HOTEL` AS `NUM_HOTEL`,
  `th`.`NOMBRE` AS `NOMBRE`,
  `th`.`DESCRIPCION` AS `DESCRIPCION`,
  `th`.`CANTIDAD` AS `CANTIDAD`,
  `th`.`MAX_OCUPANTES` AS `MAX_OCUPANTES`,
  `th`.`PRECIO` AS `PRECIO`
FROM `tipo_habitacion` `th`
WHERE (`th`.`NUM_HOTEL` = 4);

--
-- Create view `meseshtel4`
--
CREATE
DEFINER = 'root'@'localhost'
VIEW meseshtel4
AS
SELECT
  MONTH(`r`.`FECHA_INI`) AS `meses`
FROM (`reserva` `r`
  JOIN `numhotelhotel4` `hh`
    ON ((`r`.`NUM_TIPOHAB` = `hh`.`NUM_TIPOHAB`)));

--
-- Create view `reservasyfechaini`
--
CREATE
DEFINER = 'root'@'localhost'
VIEW reservasyfechaini
AS
SELECT
  `r`.`NUM_RESERVA` AS `NUM_RESERVA`,
  MONTH(`r`.`FECHA_INI`) AS `meses`
FROM `reserva` `r`;

--
-- Create view `numresmeshotel4`
--
CREATE
DEFINER = 'root'@'localhost'
VIEW numresmeshotel4
AS
SELECT
  `yi`.`NUM_RESERVA` AS `num_reserva`
FROM (`reservasyfechaini` `yi`
  JOIN `meseshtel4` `h`
    ON ((`yi`.`meses` = `h`.`meses`)));

--
-- Create view `numtiphabnumresmeshotel4`
--
CREATE
DEFINER = 'root'@'localhost'
VIEW numtiphabnumresmeshotel4
AS
SELECT
  `r`.`NUM_RESERVA` AS `NUM_RESERVA`,
  `r`.`NUM_CLIENTE` AS `NUM_CLIENTE`,
  `r`.`NUM_TIPOHAB` AS `NUM_TIPOHAB`,
  `r`.`CANTIDAD` AS `CANTIDAD`,
  `r`.`FECHA_INI` AS `FECHA_INI`,
  `r`.`FECHA_FIN` AS `FECHA_FIN`
FROM (`reserva` `r`
  JOIN `numresmeshotel4` `rmh`
    ON ((`r`.`NUM_RESERVA` = `rmh`.`num_reserva`)));

--
-- Create view `numhotelnumtiphabnumresmeshotel4`
--
CREATE
DEFINER = 'root'@'localhost'
VIEW numhotelnumtiphabnumresmeshotel4
AS
SELECT
  `th`.`NUM_HOTEL` AS `NUM_HOTEL`
FROM (`tipo_habitacion` `th`
  JOIN `numtiphabnumresmeshotel4`
    ON ((`th`.`NUM_TIPOHAB` = `numtiphabnumresmeshotel4`.`NUM_TIPOHAB`)));

--
-- Create view `datosprimertrimestre`
--
CREATE
DEFINER = 'root'@'localhost'
VIEW datosprimertrimestre
AS
SELECT
  `r`.`NUM_RESERVA` AS `NUM_RESERVA`,
  `r`.`NUM_CLIENTE` AS `NUM_CLIENTE`,
  `r`.`NUM_TIPOHAB` AS `NUM_TIPOHAB`,
  `r`.`CANTIDAD` AS `CANTIDAD`,
  `r`.`FECHA_INI` AS `FECHA_INI`,
  `r`.`FECHA_FIN` AS `FECHA_FIN`
FROM `reserva` `r`
WHERE (QUARTER(`r`.`FECHA_INI`) = 1);

--
-- Create view `sumanumhabt`
--
CREATE
DEFINER = 'root'@'localhost'
VIEW sumanumhabt
AS
SELECT
  `pt`.`NUM_TIPOHAB` AS `num_tipohab`,
  SUM(`pt`.`CANTIDAD`) AS `habitaciones`
FROM `datosprimertrimestre` `pt`
GROUP BY `pt`.`NUM_TIPOHAB`;

--
-- Create view `dosnumhotel`
--
CREATE
DEFINER = 'root'@'localhost'
VIEW dosnumhotel
AS
SELECT
  `th`.`NUM_HOTEL` AS `NUM_HOTEL`,
  SUM(`nh`.`habitaciones`) AS `habitacionesHotel`
FROM (`tipo_habitacion` `th`
  JOIN `sumanumhabt` `nh`
    ON ((`th`.`NUM_TIPOHAB` = `nh`.`num_tipohab`)))
GROUP BY `th`.`NUM_HOTEL`
ORDER BY SUM(`nh`.`habitaciones`) DESC LIMIT 2;

--
-- Create view `diastotaleshab`
--
CREATE
DEFINER = 'root'@'localhost'
VIEW diastotaleshab
AS
SELECT
  `r`.`NUM_TIPOHAB` AS `NUM_TIPOHAB`,
  SUM((TO_DAYS(`r`.`FECHA_FIN`) - TO_DAYS(`r`.`FECHA_INI`))) AS `dias`
FROM `reserva` `r`
GROUP BY `r`.`NUM_TIPOHAB`;

--
-- Create view `diasnumhotel`
--
CREATE
DEFINER = 'root'@'localhost'
VIEW diasnumhotel
AS
SELECT
  `th`.`NUM_HOTEL` AS `NUM_HOTEL`,
  `th1`.`dias` AS `dias`
FROM (`tipo_habitacion` `th`
  JOIN `diastotaleshab` `th1`
    ON ((`th`.`NUM_TIPOHAB` = `th1`.`NUM_TIPOHAB`)));

--
-- Create view `mesesreservas`
--
CREATE
DEFINER = 'root'@'localhost'
VIEW mesesreservas
AS
SELECT
  `r`.`NUM_TIPOHAB` AS `NUM_TIPOHAB`,
  `r`.`FECHA_INI` AS `FECHA_INI`
FROM `reserva` `r`;

-- 
-- Restore previous SQL mode
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Enable foreign keys
-- 
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;