/*
4- Realizar las siguientes consultas
*/
-- a. Crear una consulta que muestre para cada g�nero el m�ximo y el m�nimo precio de las pel�culas.

CREATE OR REPLACE VIEW agruparGeneroMax
AS
SELECT
  p.CodGenero,
  MAX(p.Precio) precioMax
FROM peliculas p
GROUP BY p.CodGenero;

CREATE OR REPLACE VIEW agruparGeneroMin
AS
SELECT
  p.CodGenero,
  MIN(p.Precio) precioMin
FROM peliculas p
GROUP BY p.CodGenero;

CREATE OR REPLACE VIEW agruparGeneroMaxMin
AS
SELECT
  *
FROM agruparGeneroMax gm
  JOIN agruparGeneroMin gm1 USING (codGenero);

SELECT
  g.NomGenero,
  gmm.precioMax,
  gmm.precioMin
FROM generos g
  JOIN agruparGeneroMaxMin gmm USING (CodGenero);

-- b. Crear una consulta que muestre para cada g�nero la suma de los precios de las pel�culas. 

CREATE OR REPLACE VIEW agruparGeneroSuma
AS
SELECT
  p.CodGenero,
  SUM(p.Precio) suma
FROM peliculas p
GROUP BY p.CodGenero;

SELECT
  g.NomGenero,
  gs.suma
FROM generos g
  JOIN agruparGeneroSuma gs USING (CodGenero);

-- c. Crear una consulta que muestre para cada vendedor el n�mero de pel�culas diferentes que ha vendido. 

-- diferentes

SELECT
  f.CodVend,
  COUNT(DISTINCT lf.CodPelicula)
FROM facturas f
  INNER JOIN lineas_fac lf
    ON f.CodFac = lf.CdFac
GROUP BY f.CodVend;

-- totales

SELECT
  f.CodVend,
  COUNT(lf.CodPelicula)
FROM facturas f
  INNER JOIN lineas_fac lf
    ON f.CodFac = lf.CdFac
GROUP BY f.CodVend;

-- d. Crear una consulta que muestre para cada vendedor el n�mero total de pel�culas que ha vendido en 2005 y 2006. 

CREATE OR REPLACE VIEW peliculas2005Y2006
AS
SELECT
  *
FROM facturas f
WHERE YEAR(f.Fecha) = 2005
OR YEAR(f.Fecha) = 2006;

SELECT
  y.CodVend,
  COUNT(lf.CodPelicula)
FROM peliculas2005Y2006 y
  INNER JOIN lineas_fac lf
    ON lf.CdFac = y.CodFac
GROUP BY y.CodVend;

-- e. Crear una consulta que muestre para cada vendedor y por cada g�nero: el n�mero total de pel�culas que ha vendido,
-- el importe total de la venta y el importe medio vendido. 

SELECT
  v.NombVen,
  g.NomGenero,
  SUM(f.Importe) ImporteTotal,
  AVG(f.Importe) ImporteMedio
FROM facturas f
  JOIN vendedores v USING (CodVend)
  INNER JOIN lineas_fac lf
    ON lf.cdFac = f.codFac
  INNER JOIN peliculas p
    ON p.CodPelicula = lf.CodPelicula
  INNER JOIN generos g
    ON g.CodGenero = p.CodGenero
GROUP BY CodFac;

-- f. Crear una consulta que muestre para cada vendedor el n�mero total de pel�culas que ha vendido, 
-- el importe total de la venta y el importe medio vendido, en cada uno de los meses de 2006. 

CREATE OR REPLACE VIEW facturas2006
AS
SELECT
  f.CodFac,
  f.CodVend,
  f.importe,
  MONTH(f.Fecha) meses
FROM facturas f
WHERE YEAR(f.Fecha) = 2006;

SELECT
  f.codvend,
  f.meses,
  SUM(f.importe),
  AVG(f.importe)
FROM facturas2006 f
GROUP BY f.meses
ORDER BY f.codvend, f.meses;

-- g. Crear una consulta que muestre para cada vendedor el n�mero total de pel�culas que ha vendido, 
-- el importe total de la venta y el importe medio vendido, en cada uno de los trimestres de 2005. 

  CREATE OR REPLACE VIEW facturas2005
AS
SELECT
  f.CodFac,
  f.CodVend,
  f.importe,
  quarter(f.Fecha) trimestres
FROM facturas f
WHERE YEAR(f.Fecha) = 2005;

  SELECT
  f.codvend,
  f.trimestres,
  SUM(f.importe),
  AVG(f.importe)
FROM facturas2005 f
GROUP BY f.trimestres
ORDER BY f.codvend, f.trimestres;




-- h. Crear una consulta que calcule el n�mero de facturas y el total de dichas facturas por cliente, para todos los clientes.

CREATE OR REPLACE VIEW clienteFactura
AS
SELECT
  c.CodCli,
  f.CodFac CdFac,
  c.NombCli,
  f.Importe
FROM facturas f
  JOIN clientes c USING (CodCli);

SELECT
  c.CodCli,
  COUNT(c.CdFac) NumFacturas,
  SUM(c.Importe) ImporteTotalFacturas
FROM clientefactura c
GROUP BY c.CodCli;

-- i. Crear una consulta que calcule el n�mero de pel�culas y su importe total antes de impuestos y descuentos, por cliente, para todos los clientes.

SELECT
  f.NombCli,
  (SUM(Precio) * SUM(lf.Cant)) importeTotal,
  ((SUM(Precio) * SUM(lf.Cant)) * (5 / 100)) descuentoTotal
FROM lineas_fac lf
  JOIN clienteFactura f USING (CdFac)
GROUP BY f.CodCli;

/*
5- Crear las siguientes consultas de actualizaci�n
*/
-- a. Sumar a la comisi�n de los vendedores un 10% 

UPDATE vendedores v
SET v.Comisi�n = v.Comisi�n * 1.1;

-- b. Colocar en la tabla facturas un 10% mas en los gastos de envio las pel�culas enviadas en 2004 

UPDATE facturas f
SET f.GastoEnvio = f.GastoEnvio * 1.1
WHERE YEAR(f.Fecha) = 2004;

-- c. Actualizar los precios de las pel�culas en la tabla Lineas_Fac para que coincidan con los metidos en la tabla pel�culas 

CREATE OR REPLACE VIEW precioPeliculas
AS
SELECT
  p.CodPelicula,
  p.Precio
FROM peliculas p;

UPDATE lineas_fac lf
JOIN precioPeliculas pp
USING (CodPelicula)
SET lf.Precio = pp.Precio;

-- d. Colocar un IVA del 21 en todas las facturas de clientes que pertenezcan a una localidad de la provincia de Madrid 

CREATE OR REPLACE VIEW clientesMadrid
AS
SELECT
  c.CodCli
FROM clientes c
  JOIN localidades l
    ON c.CodLoc = l.CodLoc
  JOIN provincias p
    ON l.CodPro = p.CodPro
WHERE p.NombPro = 'Madrid';

CREATE OR REPLACE VIEW facturasClientesMadrid
AS
SELECT
  *
FROM facturas f
  JOIN clientesMadrid m USING (CodCli);

UPDATE facturasClientesMadrid cm
SET cm.IVA = 21;

/*
6- Realizar las siguientes consultas de creaci�n de tabla
*/
-- a. Crear una tabla llamada localidades_VIP que contenga el CodCli y el NombLoc de todos los clientes que tengan mas de 5 facturas 


CREATE TABLE IF NOT EXISTS localidades_vip (
  CodCli int(11) DEFAULT NULL,
  NombLoc varchar(255) DEFAULT NULL,
  PRIMARY KEY (CodCli)
);

ALTER TABLE localidades_vip
ADD CONSTRAINT FK_localidades_VIP_CodCli FOREIGN KEY (CodCli)
REFERENCES clientes (CodCli) ON DELETE CASCADE ON UPDATE CASCADE;

CREATE OR REPLACE VIEW contarFacturasCliente
AS
SELECT
  f.CodCli,
  COUNT(f.CodFac) numero
FROM facturas f
GROUP BY f.CodCli;

CREATE OR REPLACE VIEW clienteMas5Facturas
AS
SELECT
  fc.CodCli
FROM contarFacturasCliente fc
WHERE numero > 5;

CREATE OR REPLACE VIEW codYlocCliente
AS
SELECT
  c.CodCli,
  l.NombLoc
FROM clientes c
  JOIN clienteMas5Facturas mf USING (CodCli)
  JOIN localidades l
    ON c.CodLoc = l.CodLoc;

INSERT INTO localidades_vip (CodCli, NombLoc)
  SELECT
    codCli,
    NombLoc
  FROM codYlocCliente;


-- b. Crear una tabla llamada PeliculasCompradas donde se coloquen los campos CodPelicula y CodCli. Tienen que salir los clientes con las pel�culas compradas 

CREATE TABLE practica9.peliculascompradas (
  CodPelicula int(10) NOT NULL,
  CodCli int(11) NOT NULL,
  PRIMARY KEY (CodPelicula, CodCli)
);

ALTER TABLE peliculascompradas
ADD CONSTRAINT FK_PeliculasCompradas_CodCli FOREIGN KEY (CodCli)
REFERENCES clientes (CodCli) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE peliculascompradas
ADD CONSTRAINT FK_peliculascompradas_CodPelic FOREIGN KEY (CodPelicula)
REFERENCES peliculas (CodPelicula) ON DELETE CASCADE ON UPDATE CASCADE;

CREATE OR REPLACE VIEW peliculasCompradasEncontradas
AS
SELECT
  lf.CodPelicula,
  f.CodCli
FROM facturas f
  JOIN lineas_fac lf
    ON f.CodFac = lf.CdFac;

INSERT INTO peliculascompradas (CodPelicula, CodCli)
  SELECT
    *
  FROM peliculasCompradasEncontradas p;

-- c. Crear una tabla llamada VentasVendedores donde tengamos dos campos CodVen y cantidad que ha ganado (utilizando el campo importe de factura). 

CREATE TABLE ventasvendedores (
  CodVend int(11) NOT NULL,
  cantidad int(11) DEFAULT NULL,
  PRIMARY KEY (CodVend)
);

ALTER TABLE ventasvendedores
ADD CONSTRAINT FK_VentasVendedores_CodVend FOREIGN KEY (CodVend)
REFERENCES vendedores (CodVend) ON DELETE CASCADE ON UPDATE CASCADE;

CREATE OR REPLACE VIEW ventasVendedorCod
AS
SELECT
  f.CodVend,
  SUM(f.Importe) importe
FROM facturas f
GROUP BY f.CodVend;

INSERT INTO ventasvendedores (CodVend, cantidad)
  SELECT
    vc.CodVend,
    vc.importe
  FROM ventasVendedorCod vc;

/*
7- Realizar las siguientes consultas de datos anexados: 
*/
-- a. Insertar en la tabla localidades_VIP los clientes que tengan 4 facturas 

CREATE OR REPLACE VIEW cliente4Facturas
AS
SELECT
  fc.CodCli
FROM contarFacturasCliente fc
WHERE numero = 4;

CREATE OR REPLACE VIEW codYlocCliente4facturas
AS
SELECT
  c.CodCli,
  l.NombLoc
FROM clientes c
  JOIN cliente4Facturas mf USING (CodCli)
  JOIN localidades l
    ON c.CodLoc = l.CodLoc;

INSERT INTO localidades_vip (CodCli, NombLoc)
  SELECT
    codCli,
    NombLoc
  FROM codYlocCliente4facturas;


-- b. Crear una tabla (consulta de creaci�n de tabla LDD) llamada Peliculas_Bajas con un solo campo denominado CodPelicula. 

CREATE TABLE Peliculas_Bajas (
  CodPelicula int(11) NOT NULL,
  PRIMARY KEY (CodPelicula)
);

-- Con una consulta de datos anexados meter las pel�culas que no se han vendido

CREATE OR REPLACE VIEW todasLasPelis
AS
SELECT DISTINCT
  p.CodPelicula
FROM peliculas p;

CREATE OR REPLACE VIEW pelisCompradas
AS
SELECT DISTINCT
  p.CodPelicula
FROM peliculascompradas p;

CREATE OR REPLACE VIEW pelisNoVendidas
AS
SELECT
  lp.CodPelicula
FROM todasLasPelis lp
  LEFT JOIN pelisCompradas pc
    ON lp.CodPelicula = pc.CodPelicula
WHERE pc.CodPelicula IS NULL;

INSERT INTO peliculas_bajas
  SELECT
    *
  FROM pelisNoVendidas nv;
