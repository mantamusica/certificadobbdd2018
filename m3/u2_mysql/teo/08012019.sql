﻿-- consulta 1
-- Equipos que tengan ciclistas que hayan ganado alguna etapa que tenga puerto de montaña
/* realizamos la consulta en 1 paso */
  SELECT DISTINCT 
    equipo.* 
  FROM 
    equipo 
  JOIN ciclista c ON equipo.nomequipo = c.nomequipo 
  JOIN etapa e ON c.dorsal = e.dorsal
  JOIN puerto p ON e.numetapa = p.numetapa;

  /* vamos a realizarla por partes */
  -- c1
    SELECT DISTINCT e.numetapa, e.dorsal 
    FROM etapa e 
    JOIN puerto p ON e.numetapa = p.numetapa;

  -- c2
    SELECT DISTINCT c.nomequipo 
      FROM ciclista c 
      JOIN (
       SELECT DISTINCT e.numetapa, e.dorsal 
        FROM etapa e 
        JOIN puerto p ON e.numetapa = p.numetapa
        ) AS c1
      ON c.dorsal=c1.dorsal;

    -- consulta final
      SELECT 
        e.* 
        FROM equipo e
        JOIN (
          SELECT DISTINCT c.nomequipo 
          FROM ciclista c 
          JOIN (
           SELECT DISTINCT e.numetapa, e.dorsal 
            FROM etapa e 
            JOIN puerto p ON e.numetapa = p.numetapa
            ) AS c1
            ON c.dorsal=c1.dorsal
          ) AS c2
          ON c2.nomequipo=e.nomequipo;

CREATE OR REPLACE VIEW consulta1C1 AS 
  SELECT DISTINCT e.numetapa, e.dorsal 
    FROM etapa e 
    JOIN puerto p ON e.numetapa = p.numetapa;

CREATE OR REPLACE VIEW consulta1C2 AS 
  SELECT DISTINCT c.nomequipo 
        FROM ciclista c 
        JOIN consulta1C1 c1
        ON c.dorsal=c1.dorsal;

 CREATE OR REPLACE VIEW consulta1 AS 
 SELECT e.* 
        FROM equipo e
        JOIN consulta1C2 c2 ON c2.nomequipo=e.nomequipo;


-- consulta 2
-- Ciclistas (todos los campos) que hayan ganado puertos de mas de 1200 m
  
  SELECT 
    * 
  FROM ciclista c 
  JOIN puerto p ON c.dorsal = p.dorsal 
  WHERE p.altura>1200;

-- mejorar la consulta mediante las subconsultas
-- c1
  SELECT DISTINCT p.dorsal FROM puerto p WHERE p.altura>1200;
-- consulta completa
  SELECT 
    c.* 
    FROM (SELECT DISTINCT p.dorsal FROM puerto p WHERE p.altura>1200) AS c1
    JOIN ciclista c USING(dorsal);

  -- vamos a utilizar vistas
  -- creamos la primera vista
  CREATE OR REPLACE VIEW consulta2C1 AS
    SELECT DISTINCT p.dorsal FROM puerto p WHERE p.altura>1200;

  -- creamos la segunda vista
  CREATE OR REPLACE VIEW consulta2 AS 
    SELECT 
      c.* 
        FROM consulta2C1 AS c1
        JOIN ciclista c USING(dorsal);

  -- consulta 3
  -- ciclistas que no han ganado etapas

    SELECT 
      c.* 
    FROM ciclista c 
    LEFT JOIN etapa e ON c.dorsal = e.dorsal
    WHERE e.dorsal IS NULL;

    -- realizado con subconsultas
    -- c1 : ciclistas que han ganado alguna etapa
      SELECT DISTINCT dorsal FROM etapa;
    -- consulta completa
    -- aqui la resta con un left join
      SELECT 
        c.* 
      FROM ciclista c LEFT JOIN (SELECT DISTINCT dorsal FROM etapa) as c1
      ON c.dorsal=c1.dorsal WHERE c1.dorsal IS NULL;
    -- la resta pero con not in
      SELECT 
        * 
      FROM ciclista c 
      WHERE c.dorsal NOT IN (SELECT DISTINCT dorsal FROM etapa);

      -- realizar con vistas
      -- c1
        CREATE OR REPLACE VIEW consulta3C1 AS 
          SELECT DISTINCT dorsal FROM etapa;
       -- completa
          CREATE OR REPLACE VIEW consulta3 AS 
            SELECT 
              c.* 
            FROM ciclista c LEFT JOIN consulta3C1 as c1
            ON c.dorsal=c1.dorsal WHERE c1.dorsal IS NULL;
        -- completa con not in
            CREATE OR REPLACE VIEW consulta3P AS 
              SELECT 
                * 
              FROM ciclista c 
              WHERE c.dorsal NOT IN (SELECT DISTINCT dorsal FROM etapa);