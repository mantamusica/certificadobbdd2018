﻿/* 
  EJEMPLO NUMERO 4
  
  ciclistas que han ganado etapas y que no han ganado puertos

*/
-- sin utlizar vistas

  -- c1 : ciclistas que han ganado etapas
  SELECT DISTINCT e.dorsal FROM etapa e;

  -- c2: ciclistas que han ganado puertos
  SELECT DISTINCT p.dorsal FROM puerto p;

  -- c3: ciclistas que no han ganado puertos
  SELECT c.dorsal FROM ciclista c LEFT JOIN puerto p ON c.dorsal = p.dorsal
    WHERE p.dorsal IS NULL;


  -- utilizando vistas
  CREATE OR REPLACE VIEW consulta4C1 AS
    SELECT DISTINCT e.dorsal FROM etapa e;

  CREATE OR REPLACE VIEW consulta4C2 AS
    SELECT DISTINCT p.dorsal FROM puerto p;

  CREATE OR REPLACE VIEW consulta4C3 AS
    SELECT c.dorsal FROM ciclista c LEFT JOIN puerto p ON c.dorsal = p.dorsal
    WHERE p.dorsal IS NULL;

  -- C1-C3 : ciclistas que han ganado etapas y puertos
    SELECT c1.dorsal FROM consulta4C1 c1 LEFT JOIN consulta4C3 c3 USING(dorsal) 
      WHERE c3.dorsal IS NULL;

    SELECT c1.dorsal FROM consulta4C1 c1 
      WHERE c1.dorsal NOT IN (SELECT * FROM consulta4C3);

-- C1-C2 : ciclistas que han ganado etapas y no han ganado puertos
  SELECT c1.dorsal FROM consulta4C1 c1 LEFT JOIN consulta4C2 c2 USING(dorsal) WHERE 
    c2.dorsal IS NULL;

  SELECT * FROM consulta4C1 c1 
    WHERE c1.dorsal NOT IN (SELECT * FROM consulta4C2);

-- C1 UNION C2 : ciclistas que han ganado etapas mas los ciclistas que han ganado puertos
SELECT * FROM consulta4C1
  UNION  
SELECT * FROM consulta4C2;

-- C1 UNION C3: ciclistas que han ganado etapas mas los ciclistas que no han ganado puertos
SELECT * FROM consulta4C1 c
  UNION
SELECT * FROM consulta4C3 c;

-- C1 interseccion C3: ciclistas que han ganado etapas y ademas que no han ganado puertos
SELECT * FROM consulta4c1 c1 NATURAL JOIN consulta4C3 c3;

-- C1 interseccion C2: ciclistas que han ganado etapas y ademas han ganado puertos
SELECT * FROM consulta4c1 c1 NATURAL JOIN consulta4c2 c2;


/* 
  EJEMPLO NUMERO 5
*/

  -- C1 : ciclistas que han llevado maillot
  SELECT DISTINCT dorsal FROM lleva l;

  CREATE OR REPLACE VIEW consulta5C1 AS 
    SELECT DISTINCT dorsal FROM lleva l;

  -- C2: ciclistas que no han llevado maillot
  SELECT c.dorsal FROM ciclista c LEFT JOIN consulta5C1 c1 USING(dorsal)
    WHERE c1.dorsal IS NULL;

  SELECT * FROM ciclista c 
    WHERE c.dorsal NOT IN (SELECT * FROM consulta5C1);

  CREATE OR REPLACE VIEW consulta5C2 AS 
    SELECT c.dorsal FROM ciclista c LEFT JOIN consulta5C1 c1 USING(dorsal)
    WHERE c1.dorsal IS NULL;

  -- c3: ciclistas que han ganado etapas
    SELECT DISTINCT dorsal FROM etapa;

    CREATE OR REPLACE VIEW consulta5C3 AS
      SELECT DISTINCT dorsal FROM etapa;

    -- c4 : ciclistas que han ganado puertos
    SELECT DISTINCT dorsal FROM puerto;

    CREATE OR REPLACE VIEW consulta5C4 AS 
      SELECT DISTINCT dorsal FROM puerto;


-- c3 - c1 : ciclistas que han ganado etapa y ademas que no han llevado maillot
  SELECT c3.dorsal FROM consulta5C3 c3 LEFT JOIN consulta5C1 c1 USING(dorsal)
    WHERE c1.dorsal IS NULL;

  SELECT c3.dorsal FROM consulta5C3 c3 
    WHERE c3.dorsal NOT IN (SELECT * FROM consulta5C1);

  -- C3 interseccion C2
  SELECT * FROM consulta5C3 c3 NATURAL JOIN consulta5C2 c2;


  -- c3 - c2: ciclistas que han ganado etapas y ademas que han llevado maillot
  SELECT c3.dorsal FROM consulta5C3 c3 LEFT JOIN consulta5C2 c2 USING(dorsal) 
    WHERE c2.dorsal IS NULL;

  SELECT * FROM consulta5C3 c3
    WHERE c3.dorsal NOT IN (SELECT * FROM consulta5C2);

  -- c3 interseccion c1
  SELECT * FROM consulta5C3 c3 NATURAL JOIN consulta5C1 c1;


  -- c4 - c3 - c2 : ciclistas que han ganado puertos 
  -- y que no han ganado etapas y que han llevado maillot
    SELECT c4.dorsal FROM consulta5C4 c4 
      LEFT JOIN consulta5C3 c3 USING(dorsal) 
      LEFT JOIN consulta5C2 c2 USING(dorsal) 
      WHERE 
        c2.dorsal IS NULL AND c3.dorsal IS NULL;
-- c1 union c3: ciclistas que han llevado maillot mas los que han ganado alguna etapa
  SELECT * FROM consulta5C1 c1 
    UNION
  SELECT * FROM consulta5C3 c3;

-- c1 union c3 union c4
  SELECT * FROM consulta5C1 c1 
    UNION
  SELECT * FROM consulta5C3 c3
    UNION
  SELECT * FROM consulta5C4 c4;


