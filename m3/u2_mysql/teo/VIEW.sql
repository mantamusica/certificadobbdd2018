﻿DROP VIEW consulta1C1;

CREATE VIEW consulta1C1
AS
SELECT DISTINCT
  e.numetapa,
  e.dorsal
FROM etapa e
  JOIN puerto p
    ON e.numetapa = p.numetapa;

/* Creación de Vistas*/

DROP VIEW consulta1C1;

CREATE OR REPLACE VIEW consulta1C1
AS
SELECT DISTINCT
  e.numetapa,
  e.dorsal
FROM etapa e
  JOIN puerto p
    ON e.numetapa = p.numetapa;

SELECT
  *
FROM consulta1C1 c;

CREATE OR REPLACE VIEW consulta1C2
AS
SELECT DISTINCT
  c.nomequipo
FROM ciclista c
  JOIN consulta1C1 c1
    ON c.dorsal = c1.dorsal;

SELECT
  *
FROM consulta1C2 c;

/* Ciclistas que hayan ganado alguna etapa y que no hayan ganado ningún puerto*/

CREATE OR REPLACE VIEW CiclistaConEtapas
AS
SELECT
  *
FROM ciclista c
  JOIN etapa e USING (dorsal);

SELECT
  *
FROM CiclistaConEtapas cce;

CREATE OR REPLACE VIEW CiclistaConPuertos
AS
SELECT
  *
FROM ciclista c
  JOIN puerto p USING (dorsal);

SELECT
  *
FROM CiclistaConPuertos ccp;

CREATE OR REPLACE VIEW CiclistaSinPuertos
AS
SELECT
  *
FROM ciclista c
  LEFT JOIN puerto p USING (dorsal)
WHERE p.dorsal IS NULL;

SELECT
  *
FROM CiclistaSinPuertos csp;

/* Ciclistas con etapas y puertos*/
CREATE OR REPLACE VIEW CiclistaConEtapasYconPuertos
AS
SELECT
  cce.dorsal
FROM CiclistaConEtapas cce
  LEFT JOIN CiclistaSinPuertos csp
    ON cce.dorsal = csp.dorsal
WHERE csp.dorsal IS NULL;

SELECT DISTINCT
  dorsal
FROM CiclistaConEtapasYconPuertos cceyp;

/*Ciclistas con etapas y sin puertos*/
SELECT DISTINCT
  dorsal
FROM CiclistaConEtapas cce
  LEFT JOIN CiclistaSinPuertos csp
  USING (dorsal)
WHERE csp.dorsal IS NULL;

/* Ciclistas con etapas y ciclistas con puertos*/
SELECT
  cce.dorsal
FROM CiclistaConEtapas cce
UNION
SELECT
  ccp.dorsal
FROM CiclistaConPuertos ccp;

/* Ciclistas con etapas y ciclistas sin puertos*/
SELECT
  cce.dorsal
FROM CiclistaConEtapas cce
UNION
SELECT
  csp.dorsal
FROM CiclistaSinPuertos csp;

/*Ciclistas con etapas y con puertos*/
/* Intersección*/
SELECT DISTINCT
  dorsal
FROM CiclistaConEtapas cce
  NATURAL JOIN CiclistaConEtapasYconPuertos cceyp;

/*Ciclistas con etapas y sin puertos*/
/* Intersección*/
SELECT DISTINCT
  dorsal
FROM CiclistaConEtapas cce
  NATURAL JOIN CiclistaConPuertos ccp;
