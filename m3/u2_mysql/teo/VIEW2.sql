﻿/* c1 Ciclistas que han llevado algún maillot*/

CREATE OR REPLACE VIEW consulta5C1
AS
SELECT
  dorsal
FROM lleva l;

/* c2 Ciclistas que no han llevado algún maillot*/

CREATE OR REPLACE VIEW consulta5C2
AS
SELECT DISTINCT
  c.dorsal
FROM ciclista c
  LEFT JOIN consulta5C1 C1
    ON c.dorsal = C1.dorsal
WHERE C1.dorsal IS NULL;

SELECT
  *
FROM consulta5C2 c;

/* c2 otra manera */

SELECT
  *
FROM ciclista c
WHERE c.dorsal NOT IN (SELECT
    *
  FROM consulta5C1);


/* c3 Ciclistas que han ganado alguna etapa*/

CREATE OR REPLACE VIEW consulta5C3
AS
SELECT DISTINCT
  dorsal
FROM etapa e;

/* c4 Ciclistas que han ganado algún puerto*/

CREATE OR REPLACE VIEW consulta5C4
AS
SELECT DISTINCT
  dorsal
FROM puerto p;

/* Ciclistas que han ganado etapa y  */
/* c3-c1 o c3intersecc1*/

SELECT DISTINCT
  c3.dorsal
FROM consulta5C3 c3
  LEFT JOIN consulta5C1 c1
  USING (dorsal)
  WHERE c1.dorsal IS NULL;

SELECT * FROM consulta5C3 c WHERE c.dorsal NOT IN (SELECT * FROM  consulta5C1);

/*  */
/* c3-c2 o c3intersecc2 */
SELECT DISTINCT
  c3.dorsal
FROM consulta5C3 c3
  LEFT JOIN consulta5C2 c2
  USING (dorsal)
  WHERE c2.dorsal IS NULL;

SELECT * FROM consulta5C3 c WHERE c.dorsal NOT IN (SELECT * FROM  consulta5C2);

SELECT * FROM consulta5C3 c3 NATURAL JOIN consulta5C2 c2;

  /* Ciclistas que han ganado etapas y que no han ganado etapas y que han llevado maillot */
/* c4-c3-c2 */

SELECT DISTINCT
  c4.dorsal
FROM consulta5C4 c4
  LEFT JOIN consulta5C3 c3  USING (dorsal)
  WHERE c3.dorsal IS NULL;

SELECT DISTINCT c.dorsal FROM (
    SELECT DISTINCT c4.dorsal
      FROM consulta5C4 c4
      LEFT JOIN consulta5C3 c3  USING (dorsal)
      WHERE c3.dorsal IS NULL) c
    LEFT JOIN consulta5C2 c2 USING (dorsal)
    WHERE c2.dorsal IS NULL;

SELECT * FROM consulta5C4 c4
  LEFT JOIN consulta5C3 c3 USING(dorsal)
  LEFT JOIN consulta5C2 c2 USING(dorsal)
  WHERE
  c2.dorsal IS NULL AND c3.dorsal IS NULL;
  
  /* Ciclistas que han llevado maillot y ciclistas que han ganado etapas */
  /* c1 U c3 */
    SELECT DISTINCT dorsal FROM consulta5C1 c1
      NATURAL JOIN consulta5C3 c3;
/* Ciclistas que han llevado maillot y ciclistas que han ganado etapas y ciclistas que han ganado un puerto */
/* c1 U c3 u c4 */
    SELECT DISTINCT dorsal FROM consulta5C1 c1
      UNION
    SELECT DISTINCT dorsal FROM consulta5C3 c3
      UNION
    SELECT DISTINCT dorsal FROM consulta5C4 c4;

