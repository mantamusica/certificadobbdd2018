create table camionero(
cedula varchar(10) primary key,
nombre varchar(25),
telefono varchar(15),
direccion varchar(20),
salario decimal(8,2),
poblacion varchar(15)
);

create table camion(
matricula varchar(10) primary key,
modelo varchar(25),
tipo varchar(25),
potencia varchar(10)
);

create table conduce(
cedula_camionerio varchar(10),
matricula_camion varchar(10),
fecha date, 
foreign key(cedula_camionero) references camionero(cedula),
foreign key(matricula_camion) references camionero(matricula),
primary key(cedula_camionero, matricula_camion, fecha)
);

create table provincia(
cod_provincia varchar(15) primary key, 
nombre varchar(30)
);

create table paquete(
cod_paquete varchar(10) primary key,
descripcion varchar(50),
destinatario varchar(50),
direccion_destinatario varchar(20),
cedula_camionero varchar(10),
provincia_destino varchar(10),
foreign key(cedula_camionero) references camionero(cedula),
foreign key(provincia_destino) references provincia(cod_provincia)
);

----Inserccion de datos

insert into camionero values('123456789','Alejandro P�rez', 
'456789123','calle anton 21',9000000,'Pereira');

insert into camion values('123645fd', 'dfsk�j','alto','600cv');

insert into conduce values('123456789','123645fd','16/12/2012');

insert into provincia values('8080', 'Galicia');

insert into paquete values('dfs654','pqte456','enrique cifuentes',
'alli al fondo','123456789','8080');

select * from camion;

select * from camionero;

select * from conduce

select * from provincia;

select * from paquete;