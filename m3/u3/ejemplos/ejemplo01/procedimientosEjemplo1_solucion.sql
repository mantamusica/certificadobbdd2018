﻿--
-- Create procedure `ejercicio01`
--

CALL ejercicio01a(5,7);

CALL ejercicio01c(5,7);

CALL ejercicio01c(5,7);

--
-- Create procedure `ejercicio02`
--

CALL ejercicio02a(5,7,9);

CALL ejercicio02b(5,7,9);

CALL ejercicio02c(5,7,9);

--
-- Create procedure `ejercicio03`
--

CALL ejercicio03(5,7,9,@mayor,@menor);
SELECT @mayor;
SELECT @menor;

--
-- Create procedure `ejercicio04`
--

CALL ejercicio04("2017-06-25", "2017-06-15");

--
-- Create procedure `ejercicio05`
--

CALL ejercicio05("2018-06-25", "2017-06-15");

--
-- Create procedure `ejercicio06`
--

CALL ejercicio06("2018-06-25", "2020-06-15",@dias,@meses,@años);
SELECT @dias;
SELECT @meses;
SELECT @años;

--
-- Create procedure `ejercicio07`
--

CALL ejercicio07("Los domingos por la tarde");