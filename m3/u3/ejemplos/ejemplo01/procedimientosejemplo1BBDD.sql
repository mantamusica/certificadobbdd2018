﻿--
-- Script was generated by Devart dbForge Studio for MySQL, Version 8.0.40.0
-- Product home page: http://www.devart.com/dbforge/mysql/studio
-- Script date 08/02/2019 17:26:47
-- Server version: 5.5.5-10.1.30-MariaDB
-- Client version: 4.1
--

-- 
-- Disable foreign keys
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Set SQL mode
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Set character set the client will use to send SQL statements to the server
--
SET NAMES 'utf8';

--
-- Set default database
--
USE procedimientosejemplo1;

--
-- Drop procedure `e2_1`
--
DROP PROCEDURE IF EXISTS e2_1;

--
-- Drop procedure `e2_2`
--
DROP PROCEDURE IF EXISTS e2_2;

--
-- Drop procedure `ejercicio01a`
--
DROP PROCEDURE IF EXISTS ejercicio01a;

--
-- Drop procedure `ejercicio01c`
--
DROP PROCEDURE IF EXISTS ejercicio01c;

--
-- Drop procedure `ejercicio02a`
--
DROP PROCEDURE IF EXISTS ejercicio02a;

--
-- Drop procedure `ejercicio02c`
--
DROP PROCEDURE IF EXISTS ejercicio02c;

--
-- Drop procedure `ejercicio03`
--
DROP PROCEDURE IF EXISTS ejercicio03;

--
-- Drop procedure `ejercicio04`
--
DROP PROCEDURE IF EXISTS ejercicio04;

--
-- Drop procedure `ejercicio05`
--
DROP PROCEDURE IF EXISTS ejercicio05;

--
-- Drop procedure `ejercicio06`
--
DROP PROCEDURE IF EXISTS ejercicio06;

--
-- Drop procedure `ejercicio07`
--
DROP PROCEDURE IF EXISTS ejercicio07;

--
-- Drop procedure `ejercicio01b`
--
DROP PROCEDURE IF EXISTS ejercicio01b;

--
-- Drop procedure `ejercicio02b`
--
DROP PROCEDURE IF EXISTS ejercicio02b;

--
-- Drop table `numeros`
--
DROP TABLE IF EXISTS numeros;

--
-- Drop procedure `e2_4`
--
DROP PROCEDURE IF EXISTS e2_4;

--
-- Drop procedure `e2_5`
--
DROP PROCEDURE IF EXISTS e2_5;

--
-- Drop procedure `e2_6`
--
DROP PROCEDURE IF EXISTS e2_6;

--
-- Drop procedure `e2_6_null`
--
DROP PROCEDURE IF EXISTS e2_6_null;

--
-- Drop procedure `e2_7`
--
DROP PROCEDURE IF EXISTS e2_7;

--
-- Drop procedure `e2_8`
--
DROP PROCEDURE IF EXISTS e2_8;

--
-- Drop procedure `e2_8_null`
--
DROP PROCEDURE IF EXISTS e2_8_null;

--
-- Drop table `datos`
--
DROP TABLE IF EXISTS datos;

--
-- Set default database
--
USE procedimientosejemplo1;

--
-- Create table `datos`
--
CREATE TABLE datos (
  id int(11) NOT NULL AUTO_INCREMENT,
  numero1 int(11) NOT NULL,
  numero2 int(11) NOT NULL,
  suma varchar(255) DEFAULT NULL,
  resta varchar(255) DEFAULT NULL,
  rango varchar(5) DEFAULT NULL,
  texto1 varchar(25) DEFAULT NULL,
  texto2 varchar(25) DEFAULT NULL,
  junto varchar(255) DEFAULT NULL,
  longitud varchar(255) DEFAULT NULL,
  tipo int(11) NOT NULL,
  numero int(11) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 30,
AVG_ROW_LENGTH = 564,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create index `numero_index` on table `datos`
--
ALTER TABLE datos
ADD INDEX numero_index (numero);

--
-- Create index `numero2_index` on table `datos`
--
ALTER TABLE datos
ADD INDEX numero2_index (numero2);

--
-- Create index `tipo_index` on table `datos`
--
ALTER TABLE datos
ADD INDEX tipo_index (tipo);

DELIMITER $$

--
-- Create procedure `e2_8_null`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE e2_8_null ()
BEGIN
  DECLARE total int;
  DECLARE contador int DEFAULT 0;
  DECLARE txt1 varchar(50);
  DECLARE txt2 varchar(50);
  DECLARE rng char(3);
  SET total = (SELECT
      COUNT(*)
    FROM datos);
  REPEAT
    SET contador = contador + 1;
    UPDATE datos
    SET junto = NULL
    WHERE id = contador;
  UNTIL contador = total
  END REPEAT;
END
$$

--
-- Create procedure `e2_8`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE e2_8 ()
BEGIN
  DECLARE total int;
  DECLARE contador int DEFAULT 0;
  DECLARE txt1 varchar(50);
  DECLARE txt2 varchar(50);
  DECLARE rng char(3);

  CALL e2_8_null();

  SET total = (SELECT
      COUNT(*)
    FROM datos);
  REPEAT
    SET contador = contador + 1;
    SET txt1 = (SELECT
        d.texto1
      FROM datos d
      WHERE d.id = contador);
    SET txt2 = (SELECT
        d.texto2
      FROM datos d
      WHERE d.id = contador);
    SET rng = (SELECT
        d.rango
      FROM datos d
      WHERE d.id = contador);
    IF rng = 'A' THEN
      UPDATE datos
      SET junto = CONCAT_WS('-', txt1, txt2)
      WHERE id = contador;
    ELSEIF rng = 'B' THEN
      UPDATE datos
      SET junto = CONCAT_WS('+', txt1, txt2)
      WHERE id = contador;
    ELSE
      UPDATE datos
      SET junto = txt1
      WHERE id = contador;
    END IF;
  UNTIL contador = total
  END REPEAT;
END
$$

--
-- Create procedure `e2_7`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE e2_7 ()
BEGIN
  DECLARE total int;
  DECLARE contador int DEFAULT 0;
  DECLARE txt1 varchar(50);
  DECLARE txt2 varchar(50);
  SET total = (SELECT
      COUNT(*)
    FROM datos);
  REPEAT
    SET contador = contador + 1;
    SET txt1 = (SELECT
        d.texto1
      FROM datos d
      WHERE d.id = contador);
    SET txt2 = (SELECT
        d.texto2
      FROM datos d
      WHERE d.id = contador);
    UPDATE datos
    SET junto = CONCAT_WS(',', txt1, txt2)
    WHERE id = contador;
  UNTIL contador = total
  END REPEAT;
END
$$

--
-- Create procedure `e2_6_null`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE e2_6_null ()
BEGIN
  DECLARE total int;
  DECLARE contador int DEFAULT 0;
  DECLARE txt1 varchar(50);
  DECLARE txt2 varchar(50);
  DECLARE rng char(3);
  SET total = (SELECT
      COUNT(*)
    FROM datos);
  REPEAT
    SET contador = contador + 1;
    UPDATE datos
    SET suma = NULL,
        resta = NULL
    WHERE id = contador;
  UNTIL contador = total
  END REPEAT;
END
$$

--
-- Create procedure `e2_6`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE e2_6 ()
BEGIN
  DECLARE total int;
  DECLARE contador int DEFAULT 0;
  DECLARE num1 int;
  DECLARE num2 int;

  SET total = (SELECT
      COUNT(*)
    FROM datos);
  CALL e2_6_null();
  REPEAT
    SET contador = contador + 1;
    SET num1 = (SELECT
        d.numero1
      FROM datos d
      WHERE d.id = contador);
    SET num2 = (SELECT
        d.numero2
      FROM datos d
      WHERE d.id = contador);

    IF num1 > num2 THEN
      UPDATE datos
      SET suma = num1 + num2,
          resta = num2 - num1
      WHERE id = contador;
    ELSE
      UPDATE datos
      SET suma = num1 + num2,
          resta = num1 - num2
      WHERE id = contador;
    END IF;
  UNTIL contador = total
  END REPEAT;
END
$$

--
-- Create procedure `e2_5`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE e2_5 ()
BEGIN
  DECLARE total int;
  DECLARE contador int DEFAULT 0;
  DECLARE num1 int DEFAULT 0;
  DECLARE num2 int DEFAULT 0;
  SET total = (SELECT
      COUNT(*)
    FROM datos);
  REPEAT
    SET contador = contador + 1;
    SET num1 = (SELECT
        d.numero1
      FROM datos d
      WHERE d.id = contador);
    SET num2 = (SELECT
        d.numero2
      FROM datos d
      WHERE d.id = contador);
    UPDATE datos
    SET suma = num1 + num2,
        resta = num1 - num2
    WHERE id = contador;
  UNTIL contador = total
  END REPEAT;
END
$$

--
-- Create procedure `e2_4`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE e2_4 ()
BEGIN

  DECLARE numeros1 int;
  DECLARE numeros2 int;
  SET numeros1 = (SELECT
      COUNT(*)
    FROM datos
    WHERE numero1 > 50);
  SET numeros2 = (SELECT
      COUNT(*)
    FROM datos
    WHERE numero2 > 50);
  SELECT
    numeros1 + numeros2;

END
$$

DELIMITER ;

--
-- Create table `numeros`
--
CREATE TABLE numeros (
  id int(11) NOT NULL AUTO_INCREMENT,
  numero int(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 4,
AVG_ROW_LENGTH = 5461,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

DELIMITER $$

--
-- Create procedure `ejercicio02b`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE ejercicio02b (arg1 int, arg2 int, arg3 int)
BEGIN

  CREATE OR REPLACE TABLE numeros (
    id int AUTO_INCREMENT,
    numero int,
    PRIMARY KEY (id)
  );

  INSERT INTO numeros (numero)
    VALUES (arg1), (arg2), (arg3);

  SELECT
    MAX(numero)
  FROM numeros n;

END
$$

--
-- Create procedure `ejercicio01b`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE ejercicio01b (arg1 int, arg2 int)
BEGIN


  CREATE OR REPLACE TABLE numeros (
    id int AUTO_INCREMENT,
    numero int,
    PRIMARY KEY (id)
  );

  INSERT INTO numeros (numero)
    VALUES (arg1), (arg2);

  SELECT
    MAX(numero)
  FROM numeros n;

END
$$

--
-- Create procedure `ejercicio07`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE ejercicio07 (frase varchar(50))
BEGIN

  SELECT
    CHAR_LENGTH(frase);

END
$$

--
-- Create procedure `ejercicio06`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE ejercicio06 (fecha1 date, fecha2 date, OUT dias int, OUT meses int, OUT años int)
BEGIN

  SET dias = TIMESTAMPDIFF(DAY, fecha1, fecha2);
  SET meses = TIMESTAMPDIFF(MONTH, fecha1, fecha2);
  SET años = TIMESTAMPDIFF(year, fecha1, fecha2);

END
$$

--
-- Create procedure `ejercicio05`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE ejercicio05 (fecha1 date, fecha2 date)
BEGIN
  SELECT
    TIMESTAMPDIFF(MONTH, fecha1, fecha2);
END
$$

--
-- Create procedure `ejercicio04`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE ejercicio04 (fecha1 date, fecha2 date)
BEGIN

  SELECT
    DATEDIFF(fecha1, fecha2);

END
$$

--
-- Create procedure `ejercicio03`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE ejercicio03 (arg1 int, arg2 int, arg3 int, OUT mayor int, OUT menor int)
BEGIN

  SET mayor = GREATEST(arg1, arg2, arg3);
  SET menor = LEAST(arg1, arg2, arg3);

END
$$

--
-- Create procedure `ejercicio02c`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE ejercicio02c (arg1 int, arg2 int, arg3 int)
BEGIN

  DECLARE mayor int;

  SET mayor = GREATEST(arg1, arg2, arg3);

  SELECT
    mayor;

END
$$

--
-- Create procedure `ejercicio02a`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE ejercicio02a (arg1 int, arg2 int, arg3 int)
BEGIN

  IF (arg1 > arg2)
    AND (arg1 > arg3) THEN
    SELECT
      arg1;
  ELSEIF (arg2 > arg1)
    AND (arg2 > arg3) THEN
    SELECT
      arg2;
  ELSE
    SELECT
      arg3;
  END IF;

END
$$

--
-- Create procedure `ejercicio01c`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE ejercicio01c (arg1 int, arg2 int)
BEGIN

  SELECT
    GREATEST(arg1, arg2);

END
$$

--
-- Create procedure `ejercicio01a`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE ejercicio01a (arg1 int, arg2 int)
BEGIN

  DECLARE salida int DEFAULT 0;

  IF (arg1 > arg2) THEN
    SET salida = arg1;
  ELSE
    SET salida = arg2;
  END IF;

  SELECT
    salida;

END
$$

--
-- Create procedure `e2_2`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE e2_2 (texto varchar(50), letra char(1))
BEGIN

  DECLARE valor int;
  SET valor = POSITION(letra IN texto);

  IF valor > 0 THEN
    SELECT
      SUBSTRING(texto, valor, 1);
  ELSE
    SELECT
      'La letra no está';
  END IF;

END
$$

--
-- Create procedure `e2_1`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE e2_1 (texto varchar(50), letra char(1))
BEGIN

  DECLARE valor int;
  SET valor = LOCATE(letra, texto);

  IF valor > 0 THEN
    SELECT
      'Está incluido';
  ELSE
    SELECT
      'No está incluido';
  END IF;
END
$$

DELIMITER ;

-- 
-- Dumping data for table numeros
--
INSERT INTO numeros VALUES
(1, 5),
(2, 7),
(3, 9);

-- 
-- Dumping data for table datos
--
INSERT INTO datos VALUES
(1, 66, 1, '67', '-65', 'A', 'realizarlo ', 'ejemplo', 'realizarlo -ejemplo', NULL, 3, 1),
(2, 80, 7, '87', '-73', 'B', 'reciba ', 'clase', 'reciba +clase', NULL, 4, 2),
(3, 39, 89, '128', '-50', 'C', 'carácter', 'objeto', 'carácter', NULL, 1, 1),
(4, 41, 37, '78', '-4', 'D', 'procedimiento', 'tipo', 'procedimiento', NULL, 1, 1),
(5, 70, 79, '149', '-9', 'E', 'almacenado', 'persona', 'almacenado', NULL, 3, 1),
(6, 74, 63, '137', '-11', 'A', 'disparador', 'perro', 'disparador-perro', NULL, 5, 1),
(7, 4, 43, '47', '-39', 'B', 'enlace', 'casa', 'enlace+casa', NULL, 4, 3),
(8, 51, 10, '61', '-41', 'C', 'pagina', 'fotos', 'pagina', NULL, 1, 3),
(9, 23, 8, '31', '-15', 'D', 'web', 'wes', 'web', NULL, 5, 23),
(10, 91, 77, '168', '-14', 'A', 'direccion', 'enlaces', 'direccion-enlaces', NULL, 2, 2),
(11, 32, 86, '118', '-54', 'A', 'url', 'carácter', 'url-carácter', NULL, 5, 1),
(12, 94, 20, '114', '-74', 'A', 'realizarlo ', 'procedimiento', 'realizarlo -procedimiento', NULL, 1, 1),
(13, 100, 96, '196', '-4', 'B', 'reciba ', 'almacenado', 'reciba +almacenado', NULL, 4, 2),
(14, 61, 34, '95', '-27', 'B', 'carácter', 'disparador', 'carácter+disparador', NULL, 5, 2),
(15, 23, 56, '79', '-33', 'B', 'procedimiento', 'enlace', 'procedimiento+enlace', NULL, 4, 1),
(16, 97, 29, '126', '-68', 'B', 'almacenado', 'pagina', 'almacenado+pagina', NULL, 1, 1),
(17, 27, 43, '70', '-16', 'B', 'disparador', 'web', 'disparador+web', NULL, 3, 1),
(18, 44, 86, '130', '-42', 'B', 'enlace', 'direccion', 'enlace+direccion', NULL, 1, 1),
(19, 13, 85, '98', '-72', 'B', 'pagina', 'procedimiento', 'pagina+procedimiento', NULL, 3, 1),
(20, 15, 58, '73', '-43', 'A', 'web', 'almacenado', 'web-almacenado', NULL, 2, 2),
(21, 68, 47, '115', '-21', 'A', 'direccion', 'disparador', 'direccion-disparador', NULL, 5, 2),
(22, 82, 10, '92', '-72', 'A', 'url', 'enlace', 'url-enlace', NULL, 5, 1),
(23, 73, 53, '126', '-20', 'A', 'realizarlo ', 'pagina', 'realizarlo -pagina', NULL, 3, 1),
(24, 35, 29, '64', '-6', 'A', 'reciba ', 'web', 'reciba -web', NULL, 3, 1),
(25, 76, 32, '108', '-44', 'A', 'carácter', 'listado', 'carácter-listado', NULL, 4, 3),
(26, 86, 22, '108', '-64', 'B', 'procedimiento', 'lotes', 'procedimiento+lotes', NULL, 2, 2),
(27, 81, 47, '128', '-34', 'B', 'almacenado', 'golosinas', 'almacenado+golosinas', NULL, 3, 1),
(28, 65, 71, '136', '-6', 'C', 'disparador', 'gominolas', 'disparador', NULL, 2, 1),
(29, 71, 41, '112', '-30', 'D', 'enlace', 'pintores', 'enlace', NULL, 4, 1);

-- 
-- Restore previous SQL mode
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Enable foreign keys
-- 
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;