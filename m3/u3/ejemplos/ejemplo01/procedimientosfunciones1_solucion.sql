﻿
SET NAMES 'utf8';

--
-- Set default database
--
USE procedimientosfunciones1;



--
-- Create procedure `ej1`
--
DROP PROCEDURE IF EXISTS ej1;
DELIMITER $$
CREATE PROCEDURE ej1 ()
BEGIN
  SELECT
    NOW();
END$$
DELIMITER ;

CALL ej1();

--
-- Create procedure `ej2`
--
DROP PROCEDURE IF EXISTS ej2; 
DELIMITER $$
CREATE PROCEDURE ej2 ()
BEGIN
  DECLARE fecha date;
  SET fecha = NOW();
  SELECT fecha;
END $$
DELIMITER;

CALL ej2();

--
-- Create procedure `ej31`
--
DROP PROCEDURE IF EXISTS ej31; 
DELIMITER $$
CREATE PROCEDURE ej31 (arg1 int, arg2 int)
BEGIN
  DECLARE suma int;
  SET suma = arg1 + arg2;
  SELECT suma;
END $$
DELIMITER;

CALL ej31(3,4);

--
-- Create procedure `ej3b`
--
DROP PROCEDURE IF EXISTS ej32b; 
DELIMITER $$
CREATE PROCEDURE ej32b (arg1 int, arg2 int)
BEGIN
  DECLARE resta int;
  SET resta = arg1 - arg2;
  SELECT resta;
END $$
DELIMITER;

CALL ej32b(3,4);

--
-- Create procedure `ej3c`
--
DROP PROCEDURE IF EXISTS ej32c; 
DELIMITER $$
CREATE PROCEDURE ej32c (arg1 int, arg2 int)
BEGIN

  DECLARE suma int DEFAULT 0;
  SET suma = arg1 + arg2;

  CREATE TABLE IF NOT EXISTS datos(
    id int AUTO_INCREMENT,
    dato1 int,
    dato2 int,
    dato3 int,
    PRIMARY KEY(id)
  );

  INSERT INTO datos VALUES(DEFAULT,arg1,arg2,suma);

END $$
DELIMITER;

CALL ej32c(7,9);
CALL ej32c(7,7);
CALL ej32c(11,9);

SELECT * FROM datos;


--
-- Create procedure `ej3d`
--
DROP PROCEDURE IF EXISTS ej32d; 
DELIMITER $$
CREATE PROCEDURE ej32d (arg1 int, arg2 int)
BEGIN

  DECLARE 
  producto int DEFAULT 0;
  DECLARE
  suma int DEFAULT 0;
  SET suma = arg1 + arg2, producto = arg1 * arg2;

  CREATE TABLE IF NOT EXISTS procedimiento(
    id int AUTO_INCREMENT,
    dato1 int,
    dato2 int,
    suma int,
    producto int,
    PRIMARY KEY(id)
  );

  INSERT INTO procedimiento VALUES(DEFAULT,arg1,arg2,suma,producto);

END $$
DELIMITER;

CALL ej32d(7,9);
CALL ej32d(7,7);
CALL ej32d(11,9);

SELECT * FROM procedimiento p;

--
-- Create procedure `ej33`
--
DROP PROCEDURE IF EXISTS ej33; 
DELIMITER $$
CREATE PROCEDURE ej33 ()
BEGIN

  DROP TABLE IF EXISTS ej33;
  CREATE TABLE IF NOT EXISTS ej33(
    id int AUTO_INCREMENT,
    dato1 int,
    dato2 int,
    resultado decimal,
    PRIMARY KEY(id)
  );

END $$
DELIMITER;

CALL ej33();

SELECT * FROM ej33 p;

--
-- Create procedure `ej33a`
--
DROP PROCEDURE IF EXISTS ej33a; 
DELIMITER $$
CREATE PROCEDURE ej33a (arg1 int, arg2 int)
BEGIN

  DECLARE
  potencia int DEFAULT 0;
  SET potencia = POW(arg1,arg2);

  INSERT INTO ej33 VALUES(DEFAULT,arg1,arg2,potencia);

END $$
DELIMITER;

CALL ej33a(7,9);
CALL ej33a(7,7);
CALL ej33a(11,9);

SELECT * FROM ej33 p;

--
-- Create procedure `ej33b`
--
DROP PROCEDURE IF EXISTS ej33b; 
DELIMITER $$
CREATE PROCEDURE ej33b (arg1 int)
BEGIN

  DECLARE
  raizCuadrada decimal DEFAULT 0;
  SET raizCuadrada = SQRT(arg1);

  INSERT INTO ej33 VALUES(DEFAULT,arg1,DEFAULT,raizCuadrada);

END $$
DELIMITER;

CALL ej33b(79);
CALL ej33b(77);
CALL ej33b(119);

SELECT * FROM ej33 p;

--
-- Create procedure `ej34`
-- Creamos tabla con id, varchar(50),int,varchar(50)
--

DROP PROCEDURE IF EXISTS ej34; 
DELIMITER $$
CREATE PROCEDURE ej34 ()
BEGIN

  DROP TABLE IF EXISTS ej34;
  CREATE TABLE IF NOT EXISTS ej34(
    id int AUTO_INCREMENT,
    texto char(50),
    longitud int,
    caracteres char(50),
    PRIMARY KEY(id)
  );

END $$
DELIMITER;

CALL ej34();

SELECT * FROM ej34;

--
-- Create procedure `ej34a`
-- Argumentos: Nada
-- Objetivo: Actualizar Longitud
--

DROP PROCEDURE IF EXISTS ej34a; 
DELIMITER $$
CREATE PROCEDURE ej34a ()
BEGIN

  UPDATE ej34 SET longitud = CHAR_LENGTH(texto);

END $$
DELIMITER;

CALL ej34a();

SELECT * FROM ej34 p;

--
-- Create procedure `ej34b`
-- Argumentos: Texto
-- Objetivo: Insertar Registro
--

DROP PROCEDURE IF EXISTS ej34b; 
DELIMITER $$
CREATE PROCEDURE ej34b (nombre varchar(50))
BEGIN

  INSERT INTO ej34 VALUES(DEFAULT,nombre,DEFAULT,DEFAULT);

END $$
DELIMITER;

CALL ej34b('Pepe');
CALL ej34b('Juan Carlos');
CALL ej34b('Luis Manuel');

SELECT * FROM ej34;
--
-- Create procedure `ej34c`
-- Argumentos: Numero
-- Objetivo: Coge texto y pone ese número de caracteres
--

DROP PROCEDURE IF EXISTS ej34c; 
DELIMITER $$
CREATE PROCEDURE ej34c (numeroCaracteres int,registro int)
BEGIN

  UPDATE ej34 e JOIN (SELECT * FROM ej34 e LIMIT registro,1000) c1 
  USING (id)
  SET e.caracteres = SUBSTRING(e.texto, 1, numeroCaracteres);

END $$
DELIMITER;

CALL ej34c(1,2);
CALL ej34c(3,2);
CALL ej34c(4,4);

SELECT * FROM ej34;

--
-- Create procedure `ej35`
-- Argumentos: Numero
-- Objetivo: Si mayor de 10 grande y si menor pequeño
--

DROP PROCEDURE IF EXISTS ej35; 
DELIMITER $$
CREATE PROCEDURE ej35 ()
BEGIN

  ALTER TABLE ej34 ADD COLUMN tamaño char(15) DEFAULT 'opción';

END $$
DELIMITER;

CALL ej35();

DROP PROCEDURE IF EXISTS ej35a; 
DELIMITER $$
CREATE PROCEDURE ej35a (numero int)
BEGIN

  IF (numero > 10) THEN 
    INSERT INTO ej34 VALUES(DEFAULT,DEFAULT,DEFAULT,DEFAULT,'grande');
  ELSE
    INSERT INTO ej34 VALUES(DEFAULT,DEFAULT,DEFAULT,DEFAULT,'pequeño');
  END IF;

  END $$
DELIMITER;

CALL ej35a(7);

SELECT * FROM ej34 e;


--
-- Create procedure `ej4`
-- Argumentos: Dos numeros
-- Objetivo: los guardar en una tabla temporal y la lista
--

DROP PROCEDURE IF EXISTS ej4; 
DELIMITER $$
CREATE PROCEDURE ej4 (numero1 int, numero2 int)
BEGIN

  CREATE OR REPLACE  TABLE datosTemporary (
    id int AUTO_INCREMENT,
    numero int,
    PRIMARY KEY(id)
    );

  INSERT INTO datosTemporary (numero) VALUES(numero1),(numero2);

  SELECT * FROM datosTemporary; 
  END $$
DELIMITER;

CALL ej4(5,7);

-- Create procedure `ej5`
-- Argumentos: Tres argumentos
-- Objetivo: explicación in, out e inout
--

  DELIMITER //
  DROP PROCEDURE IF EXISTS ej5 //

  CREATE PROCEDURE ej5(arg1 int, OUT arg2 int, INOUT arg3 int)
    BEGIN
      SELECT arg1,arg2,arg3;
    END //
  DELIMITER ;

SET @numero3=10;
CALL ej5(1,@numero2,@numero3);

-- Create procedure `ej5Suma`
-- Argumentos: Tres argumentos
-- Objetivo: explicación in, out e inout
--

  DELIMITER //
  DROP PROCEDURE IF EXISTS ej5Suma //

  CREATE PROCEDURE ej5Suma(arg1 int, OUT arg2 int, INOUT arg3 int)
    BEGIN
      SELECT arg1,arg2,arg3;
    END //
  DELIMITER ;

SET @numero3=10;
CALL ej5(1,2,@numero3);