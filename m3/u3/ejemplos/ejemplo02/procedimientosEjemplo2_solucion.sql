﻿--
-- Create procedure `ejercicio01` reciba texto y carácter. Muestra si está o no está.
--

CALL e2_1('La casa esta aparcada', '<');
CALL e2_1('La casa esta aparcada', 'p');

--
-- Create procedure `ejercicio02` reciba texto y carácter. Te debe indicar 
-- todo el texto que haya antes de la primera vez que aparece ese carácter
--

CALL e2_2('La casa esta aparcada', 'p');

--
-- Create procedure `ejercicio03` reciba tres números y dos argumentos de tipo salida donde devuelva el 
-- número más grande y el número más pequeño de los tres números pasados
--

CALL e2_3(5, 7, 9, @mayor, @menor);
SELECT
  @mayor;
SELECT
  @menor;

--
-- Create procedure `ejercicio04` Cuantos números1 y numeros2 son mayores de 50
--

CALL e2_4();

--
-- Create procedure `ejercicio05` calcula la suma y la resta de numero1 y numero2
--

CALL e2_5();

SELECT
  *
FROM datos;
--
-- Create procedure `ejercicio06` Primero ponga todos los valores de suma y resta a null despues calcule la suma solamente si el numero
-- 1 es mayor que el numero 2 y calcule la resta de numero2-numero1 si el numero2 es mayor o numero1-numero2 si es mayor el numero 1
--
CALL e2_6();

SELECT
  *
FROM datos;
--
-- Create procedure `ejercicio07` coloque en el campo junto el texto1, texto2
--

CALL e2_7();

SELECT
  *
FROM datos;

--
-- Create procedure `ejercicio08` coloque en el campo junto el valor null. despues debe colocar en el campo junto el texto1-texto2 si el rango
-- es A y si es rango B debe colocar texto1+texto. si el rango es distinto debe colocar texto1 nada más. 2
--

CALL e2_8();

SELECT
  *
FROM datos;