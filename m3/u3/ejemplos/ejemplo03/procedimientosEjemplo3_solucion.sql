﻿﻿/**
  Ejemplos de programacion 3
**/

USE procedimientosejemplo3;

/**
  Ejercicio 2
  Realizar una función que reciba como argumentos:
    -	Base de un triangulo
    -	Altura de un triangulo
  Debe devolver el cálculo del área del triángulo.

-- probando la funcion
SELECT areaTriangulo(12,2);

/**
  Ejercicio 3
  Realizar una función que reciba como argumentos:
    -	Base de un triangulo
    -	Lado2 de un triangulo
    -	Lado3 de un triangulo
  Debe devolver el cálculo del perimetro del triángulo.

  Para probar la funcion:
  
  select perimetroTriangulo(23,4);
**/

-- probando la funcion
SELECT perimetroTriangulo(12,2,2);

/**
  Ejercicio 4
  Realizar un procedimiento almacenado que cuando le llames 
  como argumentos:
    -	Id1: id inicial
    -	Id2: id final
  Actualice el área y el perímetro de los triángulos (utilizando las funciones realizadas) que estén comprendidos entre los id pasados.


  Para probar el procedimiento:
  
  call actualizarTriangulos(1,5);
**/

-- probando el procedimiento
CALL actualizarTriangulos(1,5);
SELECT * FROM triangulos t;

/**
  Ejercicio 11
  Realizar una función que reciba como argumentos:
    -	radio
  Debe devolver el cálculo del área
 
**/

SELECT areaCirculo(2);

/**
  Ejercicio 12
  Realizar una función que reciba como argumentos:
    -	radio
  Debe devolver el perimetro del circulo
 
**/
SELECT perimetroCirculo(2);

/** 
  Ejercicio 13
**/

CALL actualizarCirculos(1,20,NULL);
SELECT * FROM circulo c;

/**
 Ejercicio 14

 Realizar una función que reciba como argumentos 4 notas.
 Debe devolver el cálculo de la nota media.
 
 **/

/**
 Ejercicio 15

 Realizar una función que reciba como argumentos 4 notas.
 Debe devolver el cálculo de la nota minima
 
 **/

  
SELECT minimo(4,12,8,-9);

/**
 Ejercicio 16

 Realizar una función que reciba como argumentos 4 notas.
 Debe devolver el cálculo de la nota maxima
 
 **/

  
SELECT maximo(4,12,8,-9);

