﻿--
-- Script was generated by Devart dbForge Studio for MySQL, Version 8.0.40.0
-- Product home page: http://www.devart.com/dbforge/mysql/studio
-- Script date 14/02/2019 18:12:54
-- Server version: 5.5.5-10.1.30-MariaDB
-- Client version: 4.1
--

-- 
-- Disable foreign keys
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Set SQL mode
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Set character set the client will use to send SQL statements to the server
--
SET NAMES 'utf8';

--
-- Set default database
--
USE procedimientosejemplo3;

--
-- Drop table `conversion`
--
DROP TABLE IF EXISTS conversion;

--
-- Drop table `cuadrados`
--
DROP TABLE IF EXISTS cuadrados;

--
-- Drop table `rectangulo`
--
DROP TABLE IF EXISTS rectangulo;

--
-- Drop procedure `actualizarAlumnos`
--
DROP PROCEDURE IF EXISTS actualizarAlumnos;

--
-- Drop table `alumnos`
--
DROP TABLE IF EXISTS alumnos;

--
-- Drop function `media`
--
DROP FUNCTION IF EXISTS media;

--
-- Drop function `minimo`
--
DROP FUNCTION IF EXISTS minimo;

--
-- Drop function `maximo`
--
DROP FUNCTION IF EXISTS maximo;

--
-- Drop table `grupos`
--
DROP TABLE IF EXISTS grupos;

--
-- Drop procedure `actualizarCirculos`
--
DROP PROCEDURE IF EXISTS actualizarCirculos;

--
-- Drop table `circulo`
--
DROP TABLE IF EXISTS circulo;

--
-- Drop function `areaCirculo`
--
DROP FUNCTION IF EXISTS areaCirculo;

--
-- Drop function `perimetroCirculo`
--
DROP FUNCTION IF EXISTS perimetroCirculo;

--
-- Drop procedure `actualizarTriangulos`
--
DROP PROCEDURE IF EXISTS actualizarTriangulos;

--
-- Drop procedure `ejercicio04`
--
DROP PROCEDURE IF EXISTS ejercicio04;

--
-- Drop table `triangulos`
--
DROP TABLE IF EXISTS triangulos;

--
-- Drop function `areaTriangulo`
--
DROP FUNCTION IF EXISTS areaTriangulo;

--
-- Drop function `perimetroTriangulo`
--
DROP FUNCTION IF EXISTS perimetroTriangulo;

--
-- Set default database
--
USE procedimientosejemplo3;

DELIMITER $$

--
-- Create function `perimetroTriangulo`
--
CREATE DEFINER = 'root'@'localhost'
FUNCTION perimetroTriangulo (base int, lado2 int, lado3 int)
RETURNS float
COMMENT ' Calcula el perimetro de un triangulo. 
  Recibe como argumentos la base y los otros dos lados'
BEGIN
  -- creamos las variables
  DECLARE r float;

  -- realizamos los calculos
  SET r = base + lado2 + lado3;

  -- retornamos el resultado
  RETURN r;
END
$$

--
-- Create function `areaTriangulo`
--
CREATE DEFINER = 'root'@'localhost'
FUNCTION areaTriangulo (base int, altura int)
RETURNS float
COMMENT ' Calcula el area de un triangulo. Recibe como argumentos la base y la altura'
BEGIN
  -- creamos las variables
  DECLARE area float;

  -- realizamos los calculos
  SET area = base * altura / 2;

  -- retornamos el resultado
  RETURN area;
END
$$

DELIMITER ;

--
-- Create table `triangulos`
--
CREATE TABLE triangulos (
  id int(11) NOT NULL,
  base int(11) NOT NULL,
  altura int(11) NOT NULL,
  lado2 int(11) NOT NULL,
  lado3 int(11) NOT NULL,
  area varchar(255) DEFAULT NULL,
  perimetro varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 862,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create index `altura_index` on table `triangulos`
--
ALTER TABLE triangulos
ADD INDEX altura_index (altura);

--
-- Create index `base_index` on table `triangulos`
--
ALTER TABLE triangulos
ADD INDEX base_index (base);

--
-- Create index `lado2_index` on table `triangulos`
--
ALTER TABLE triangulos
ADD INDEX lado2_index (lado2);

--
-- Create index `lado3_index` on table `triangulos`
--
ALTER TABLE triangulos
ADD INDEX lado3_index (lado3);

DELIMITER $$

--
-- Create procedure `ejercicio04`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE ejercicio04 (IN id1 int, IN id2 int)
COMMENT 'Actualiza la tabla triangulos con el area y el perimetro'
BEGIN
  UPDATE triangulos
  SET area = areaTriangulo(id1, id2),
      perimetro = perimetroTriangulo(base, lado, lado);
END
$$

--
-- Create procedure `actualizarTriangulos`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE actualizarTriangulos (id1 int, id2 int)
COMMENT 'Actualiza la tabla triangulos con el area y el perimetro'
BEGIN

  UPDATE triangulos
  SET area = areaTriangulo(base, altura),
      perimetro = perimetroTriangulo(base, lado2, lado3)
  WHERE id BETWEEN id1 AND id2;

END
$$

--
-- Create function `perimetroCirculo`
--
CREATE DEFINER = 'root'@'localhost'
FUNCTION perimetroCirculo (radio int)
RETURNS float
COMMENT 'calcula el perimetro de un circulo de radio dado'
BEGIN
  DECLARE r float;

  SET r = 2 * PI() * radio;

  RETURN r;
END
$$

--
-- Create function `areaCirculo`
--
CREATE DEFINER = 'root'@'localhost'
FUNCTION areaCirculo (radio int)
RETURNS float
COMMENT 'calcula el area de un circulo de radio dado'
BEGIN
  DECLARE r float;

  SET r = PI() * POW(radio, 2);

  RETURN r;
END
$$

DELIMITER ;

--
-- Create table `circulo`
--
CREATE TABLE circulo (
  id int(11) NOT NULL,
  radio int(11) NOT NULL,
  tipo varchar(5) DEFAULT NULL,
  area varchar(255) DEFAULT NULL,
  perimetro varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 819,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create index `radio_index` on table `circulo`
--
ALTER TABLE circulo
ADD INDEX radio_index (radio);

DELIMITER $$

--
-- Create procedure `actualizarCirculos`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE actualizarCirculos (id1 int, id2 int, t char(1))
BEGIN

  IF (t IS NULL) THEN
    UPDATE circulo
    SET area = areaCirculo(radio),
        perimetro = perimetroCirculo(radio)
    WHERE id BETWEEN id1 AND id2;
  ELSE
    UPDATE circulo
    SET area = areaCirculo(radio),
        perimetro = perimetroCirculo(radio)
    WHERE id BETWEEN id1 AND id2
    AND tipo = t;
  END IF;
END
$$

DELIMITER ;

--
-- Create table `grupos`
--
CREATE TABLE grupos (
  id int(11) NOT NULL,
  media varchar(255) DEFAULT NULL,
  max varchar(255) DEFAULT NULL,
  min varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 8192,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

DELIMITER $$

--
-- Create function `maximo`
--
CREATE DEFINER = 'root'@'localhost'
FUNCTION maximo (n1 int, n2 int, n3 int, n4 int)
RETURNS int(11)
COMMENT 'Realizar una función que reciba como argumentos 4 notas. Debe devolver el cálculo de la nota maxima'
BEGIN
  DECLARE r int;
  SET r = GREATEST(n1, n2, n3, n4);
  RETURN r;
END
$$

--
-- Create function `minimo`
--
CREATE DEFINER = 'root'@'localhost'
FUNCTION minimo (n1 int, n2 int, n3 int, n4 int)
RETURNS int(11)
COMMENT 'Realizar una función que reciba como argumentos 4 notas. Debe devolver el cálculo de la nota minima'
BEGIN
  DECLARE r int;
  SET r = LEAST(n1, n2, n3, n4);
  RETURN r;
END
$$

--
-- Create function `media`
--
CREATE DEFINER = 'root'@'localhost'
FUNCTION media (n1 int, n2 int, n3 int, n4 int)
RETURNS float
COMMENT 'Realizar una función que reciba como argumentos 4 notas. Debe devolver el cálculo de la nota media.'
BEGIN
  DECLARE r float;
  SET r = (n1 + n2 + n3 + n4) / 4;
  RETURN r;
END
$$

DELIMITER ;

--
-- Create table `alumnos`
--
CREATE TABLE alumnos (
  id int(11) NOT NULL,
  nota1 int(11) NOT NULL,
  nota2 int(11) NOT NULL,
  nota3 int(11) NOT NULL,
  nota4 int(11) NOT NULL,
  grupo int(11) NOT NULL,
  media varchar(255) DEFAULT NULL,
  max varchar(255) DEFAULT NULL,
  min varchar(255) DEFAULT NULL,
  moda varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 819,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create index `grupo_index` on table `alumnos`
--
ALTER TABLE alumnos
ADD INDEX grupo_index (grupo);

--
-- Create index `nota1_index` on table `alumnos`
--
ALTER TABLE alumnos
ADD INDEX nota1_index (nota1);

--
-- Create index `nota2_index` on table `alumnos`
--
ALTER TABLE alumnos
ADD INDEX nota2_index (nota2);

--
-- Create index `nota3_index` on table `alumnos`
--
ALTER TABLE alumnos
ADD INDEX nota3_index (nota3);

--
-- Create index `nota4_index` on table `alumnos`
--
ALTER TABLE alumnos
ADD INDEX nota4_index (nota4);

DELIMITER $$

--
-- Create procedure `actualizarAlumnos`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE actualizarAlumnos (IN id1 int,
IN id2 int)
COMMENT ' Realizar un procedimiento almacenado que cuando le llames como argumentos:
-	Id1: id inicial
-	Id2: id final
Actualice las notas mínima, máxima, media y moda de los alumnos (utilizando las funciones realizadas) que estén comprendidos entre los id pasados.
Además, el mismo procedimiento debe actualizar la tabla grupos colocando la nota media de los alumnos que estén comprendidos entre los id pasados y por grupo.'
BEGIN
  UPDATE alumnos
  SET media = media(nota1, nota2, nota3, nota4),
      minimo = minimo(nota1, nota2, nota3, nota4),
      maximo = maximo(nota1, nota2, nota3, nota4),
      moda = moda(nota1, nota2, nota3, nota4)
  WHERE id BETWEEN id1 AND id2;

  UPDATE grupos
  SET media = (SELECT
      AVG(media)
    FROM alumnos
    WHERE grupo = 1)
  WHERE id = 1;

  UPDATE grupos
  SET media = (SELECT
      AVG(media)
    FROM alumnos
    WHERE grupo = 2)
  WHERE id = 2;

END
$$

DELIMITER ;

--
-- Create table `rectangulo`
--
CREATE TABLE rectangulo (
  id int(11) NOT NULL,
  lado1 int(11) NOT NULL,
  lado2 int(11) NOT NULL,
  area varchar(255) DEFAULT NULL,
  perimetro varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 862,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create index `lado1_index` on table `rectangulo`
--
ALTER TABLE rectangulo
ADD INDEX lado1_index (lado1);

--
-- Create index `lado2_index` on table `rectangulo`
--
ALTER TABLE rectangulo
ADD INDEX lado2_index (lado2);

--
-- Create table `cuadrados`
--
CREATE TABLE cuadrados (
  id int(11) NOT NULL,
  lado int(11) NOT NULL,
  area varchar(255) DEFAULT NULL,
  perimetro varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 862,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create index `lado_index` on table `cuadrados`
--
ALTER TABLE cuadrados
ADD INDEX lado_index (lado);

--
-- Create table `conversion`
--
CREATE TABLE conversion (
  id int(11) NOT NULL,
  cm double DEFAULT NULL,
  m double DEFAULT NULL,
  km double DEFAULT NULL,
  pulgadas double DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 819,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

-- 
-- Dumping data for table triangulos
--
INSERT INTO triangulos VALUES
(1, 292, 162, 99, 24, '23652', '415'),
(2, 791, 858, 91, 67, '339339', '949'),
(3, 826, 428, 66, 97, '176764', '989'),
(4, 899, 566, 50, 41, '254417', '990'),
(5, 984, 702, 85, 13, '345384', '1082'),
(6, 317, 541, 30, 40, NULL, NULL),
(7, 697, 912, 54, 10, NULL, NULL),
(8, 727, 232, 6, 15, NULL, NULL),
(9, 376, 920, 1, 31, NULL, NULL),
(10, 9, 210, 76, 98, NULL, NULL),
(11, 185, 196, 57, 39, NULL, NULL),
(12, 757, 861, 77, 25, NULL, NULL),
(13, 911, 834, 21, 43, NULL, NULL),
(14, 354, 264, 15, 60, NULL, NULL),
(15, 261, 890, 38, 92, NULL, NULL),
(16, 63, 324, 56, 12, NULL, NULL),
(17, 990, 772, 98, 55, NULL, NULL),
(18, 923, 723, 60, 63, NULL, NULL),
(19, 957, 307, 33, 9, NULL, NULL);

-- 
-- Dumping data for table rectangulo
--
INSERT INTO rectangulo VALUES
(1, 456, 148, NULL, NULL),
(2, 45, 439, NULL, NULL),
(3, 260, 65, NULL, NULL),
(4, 59, 218, NULL, NULL),
(5, 274, 165, NULL, NULL),
(6, 291, 402, NULL, NULL),
(7, 280, 266, NULL, NULL),
(8, 457, 451, NULL, NULL),
(9, 142, 463, NULL, NULL),
(10, 381, 15, NULL, NULL),
(11, 41, 477, NULL, NULL),
(12, 325, 196, NULL, NULL),
(13, 274, 135, NULL, NULL),
(14, 102, 37, NULL, NULL),
(15, 392, 100, NULL, NULL),
(16, 232, 134, NULL, NULL),
(17, 142, 143, NULL, NULL),
(18, 12, 389, NULL, NULL),
(19, 36, 309, NULL, NULL);

-- 
-- Dumping data for table grupos
--
INSERT INTO grupos VALUES
(1, NULL, NULL, NULL),
(2, NULL, NULL, NULL);

-- 
-- Dumping data for table cuadrados
--
INSERT INTO cuadrados VALUES
(1, 216, NULL, NULL),
(2, 757, NULL, NULL),
(3, 587, NULL, NULL),
(4, 887, NULL, NULL),
(5, 83, NULL, NULL),
(6, 231, NULL, NULL),
(7, 662, NULL, NULL),
(8, 184, NULL, NULL),
(9, 513, NULL, NULL),
(10, 596, NULL, NULL),
(11, 635, NULL, NULL),
(12, 48, NULL, NULL),
(13, 381, NULL, NULL),
(14, 331, NULL, NULL),
(15, 457, NULL, NULL),
(16, 368, NULL, NULL),
(17, 637, NULL, NULL),
(18, 150, NULL, NULL),
(19, 918, NULL, NULL);

-- 
-- Dumping data for table conversion
--
INSERT INTO conversion VALUES
(1, 81.513511741433, NULL, NULL, NULL),
(2, NULL, 51.3788410351706, NULL, NULL),
(3, NULL, NULL, 5.10417664997839, NULL),
(4, 5.48361018093111, NULL, NULL, NULL),
(5, NULL, 69.7091631613809, NULL, NULL),
(6, NULL, NULL, NULL, 41.1153845605197),
(7, NULL, NULL, NULL, 16.2408669413507),
(8, 53.6235111726343, NULL, NULL, NULL),
(9, 74.6842039655531, NULL, NULL, NULL),
(10, NULL, 26.4297552968334, NULL, NULL),
(11, NULL, NULL, 38.4555759267129, NULL),
(12, 84.4923199061632, NULL, NULL, NULL),
(13, 62.2827541354844, NULL, NULL, NULL),
(14, NULL, NULL, NULL, 26.6014194084289),
(15, 82.8207116404137, NULL, NULL, NULL),
(16, NULL, 6.67961165612131, NULL, NULL),
(17, 42.883919362304, NULL, NULL, NULL),
(18, NULL, NULL, 35.3131325356947, NULL),
(19, NULL, 94.4614963941879, NULL, NULL),
(20, 78.778543721034, NULL, NULL, NULL);

-- 
-- Dumping data for table circulo
--
INSERT INTO circulo VALUES
(1, 21, 'a', NULL, NULL),
(2, 23, 'a', NULL, NULL),
(3, 12, 'b', NULL, NULL),
(4, 6, 'a', NULL, NULL),
(5, 23, 'a', NULL, NULL),
(6, 2, 'a', NULL, NULL),
(7, 20, 'a', NULL, NULL),
(8, 11, 'a', NULL, NULL),
(9, 19, 'b', NULL, NULL),
(10, 2, 'a', NULL, NULL),
(11, 11, 'a', NULL, NULL),
(12, 8, 'a', NULL, NULL),
(13, 20, 'a', NULL, NULL),
(14, 23, 'a', NULL, NULL),
(15, 9, 'a', NULL, NULL),
(16, 16, 'a', NULL, NULL),
(17, 10, 'b', NULL, NULL),
(18, 5, 'b', NULL, NULL),
(19, 19, 'b', NULL, NULL),
(20, 2, 'b', NULL, NULL);

-- 
-- Dumping data for table alumnos
--
INSERT INTO alumnos VALUES
(1, 5, 3, 8, 6, 1, NULL, NULL, NULL, NULL),
(2, 8, 8, 4, 3, 2, NULL, NULL, NULL, NULL),
(3, 4, 5, 9, 6, 1, NULL, NULL, NULL, NULL),
(4, 7, 2, 5, 9, 1, NULL, NULL, NULL, NULL),
(5, 6, 6, 1, 3, 1, NULL, NULL, NULL, NULL),
(6, 1, 4, 9, 5, 2, NULL, NULL, NULL, NULL),
(7, 1, 1, 1, 8, 2, NULL, NULL, NULL, NULL),
(8, 9, 8, 7, 4, 2, NULL, NULL, NULL, NULL),
(9, 4, 4, 5, 7, 2, NULL, NULL, NULL, NULL),
(10, 1, 9, 8, 6, 1, NULL, NULL, NULL, NULL),
(11, 2, 5, 9, 7, 1, NULL, NULL, NULL, NULL),
(12, 1, 7, 9, 1, 1, NULL, NULL, NULL, NULL),
(13, 7, 3, 6, 2, 1, NULL, NULL, NULL, NULL),
(14, 2, 1, 8, 8, 1, NULL, NULL, NULL, NULL),
(15, 1, 6, 7, 3, 2, NULL, NULL, NULL, NULL),
(16, 9, 6, 3, 7, 2, NULL, NULL, NULL, NULL),
(17, 7, 4, 7, 3, 2, NULL, NULL, NULL, NULL),
(18, 4, 6, 3, 9, 2, NULL, NULL, NULL, NULL),
(19, 6, 9, 9, 8, 1, NULL, NULL, NULL, NULL),
(20, 4, 5, 7, 5, 1, NULL, NULL, NULL, NULL);

-- 
-- Restore previous SQL mode
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Enable foreign keys
-- 
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;