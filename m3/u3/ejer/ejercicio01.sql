﻿DROP DATABASE IF EXISTS m3ejercicio01;
CREATE DATABASE IF NOT EXISTS m3ejercicio01;

USE m3ejercicio01;

DELIMITER $$
DROP PROCEDURE IF EXISTS ejemplo01$$
CREATE PROCEDURE ejemplo01 (IN argumento1 integer)
BEGIN
  DECLARE variable1 char(10);
  IF (argumento1 = 17) THEN
    SET variable1 = 'pajaros';
  ELSE
    SET variable1 = 'leones';
  END IF;

  CREATE TABLE IF NOT EXISTS table1 (
    nombre varchar(20)
  );
  INSERT INTO table1
    VALUES (variable1);

END$$
DELIMITER;

-- ejecutar procedimiento

CALL ejemplo01(10);

DELIMITER $$
DROP PROCEDURE IF EXISTS ejemplo02$$
CREATE PROCEDURE ejemplo02 (IN argumento1 integer)
BEGIN
  DECLARE variable1 char(10);
  IF (argumento1 <> 17) THEN
    SET variable1 = 'pajaros';
  ELSE
    SET variable1 = 'leones';
  END IF;

  CREATE TABLE IF NOT EXISTS table1 (
    nombre varchar(20)
  );
  INSERT INTO table1
    VALUES (variable1);

END$$
DELIMITER;

-- ejecutar procedimiento

CALL ejemplo02(15);

DELIMITER $$
DROP PROCEDURE IF EXISTS ejemplo03$$
CREATE PROCEDURE ejemplo03 (IN argumento1 integer)
BEGIN
  DECLARE variable1 char(10);
  IF (argumento1 = 10) THEN
    SET variable1 = 'pajaros';
  ELSE
    SET variable1 = 'leones';
  END IF;

  CREATE TABLE IF NOT EXISTS table1 (
    nombre varchar(20)
  );
  INSERT INTO table1
    VALUES (variable1);

END$$
DELIMITER ;


-- ejecutar procedimiento

CALL ejemplo03(10);

-- Crear un procedimiento denominado ejemploProcedimiento3 que muestre en pantalla “Hola mundo”. 
-- El siguiente procedimiento es la solución del ejercicio anterior. Modificarle para que grabe en la tabla ‘table1’ el mensaje colocado en pantalla 

DELIMITER $$
DROP PROCEDURE IF EXISTS ejemplo06$$
CREATE DEFINER = 'root'@'localhost'
PROCEDURE m3ejercicio01.ejemplo06 ()
BEGIN

  DECLARE texto char(50) DEFAULT '';
  SET texto = (SELECT
      'Hola Mundo');

  INSERT INTO table1
    VALUE (texto);


END$$
DELIMITER ;

CALL ejemplo06();

SELECT * FROM table1 t;

--  ejercicio 07

  DELIMITER $$
DROP PROCEDURE IF EXISTS ejemplo07$$
CREATE DEFINER = 'root'@'localhost'
PROCEDURE m3ejercicio01.ejemplo07 ()
BEGIN

  DECLARE contador int DEFAULT 0;

  REPEAT 
    SET contador=contador+1;
    INSERT INTO table1 VALUES ('Ejercicio07');
    UNTIL (contador>10)
    END REPEAT;


END$$
DELIMITER ;

CALL ejemplo07();


SELECT * FROM table1 t;


--  ejercicio 08

  DELIMITER $$
DROP PROCEDURE IF EXISTS ejemplo07$$
CREATE DEFINER = 'root'@'localhost'
PROCEDURE m3ejercicio01.ejemplo08 ()
BEGIN

  DECLARE contador int DEFAULT 0;

  REPEAT 
    SET contador=contador+1;
    INSERT INTO table1 VALUES ('Ejercicio07');
    UNTIL (contador>=10)
    END REPEAT;


END$$
DELIMITER ;

CALL ejemplo08();


SELECT * FROM table1 t;