﻿/* Realizar un procedimiento almacenado que visualice todas las personas cuyo salario sea superior a un valor que se
pasara como parámetro de entrada.
*/

DELIMITER $$
DROP PROCEDURE IF EXISTS PersonasSalarioSuperiorA$$
CREATE PROCEDURE PersonasSalarioSuperiorA (IN valor integer)
BEGIN
  SELECT
    *
  FROM personas p
  WHERE p.salario >= valor;
END$$
DELIMITER;

CALL PersonasSalarioSuperiorA(1500);

/* Realizar un procedimiento almacenado que visualice todas las personas cuyo salario este entre dos valores que se
pasaran como parámetros de entrada.
*/

DELIMITER $$
DROP PROCEDURE IF EXISTS PersonasSalarioEntreDosValores$$
CREATE PROCEDURE PersonasSalarioEntreDosValores (IN valor integer, IN valor1 integer)
BEGIN
  SELECT
    *
  FROM personas p
  WHERE p.salario BETWEEN valor AND valor1;
END$$
DELIMITER;

CALL PersonasSalarioEntreDosValores(1500, 2000);

/* Realizar un procedimiento almacenado que indique cuantos médicos trabajan en un hospital cuyo código se pase
como parámetro de entrada. Además al procedimiento se le pasara un argumento donde se debe almacenar el
número de plazas de ese hospital.
*/

DELIMITER $$
DROP PROCEDURE IF EXISTS ContarMedicosConCodigo$$
CREATE PROCEDURE ContarMedicosConCodigo (IN valor integer, INOUT valor1 integer)
BEGIN
  SELECT
    COUNT(*) medicos
  FROM medicos
  WHERE cod_hospital = valor;

  SET valor1 = (SELECT
      num_plazas
    FROM hospitales
    WHERE cod_hospital = valor);
END$$
DELIMITER;

CALL ContarMedicosConCodigo(2, @a);
SELECT
  @a;

/* Crear una función que calcule el volumen de una esfera cuyo radio de tipo FLOAT se pasara como parámetro. Realizar
después una consulta para calcular el volumen de una esfera de radio 5.
*/

DELIMITER $$
CREATE FUNCTION VolumenRadio (radio float)
RETURNS float
BEGIN

  DECLARE volumen float;
  SET volumen = (4 / 3) * PI() * POW(radio, 3);
  RETURN volumen;
END$$
DELIMITER;

SELECT
  VolumenRadio(5);

/* Crear un procedimiento almacenado que cree una tabla denominada esferas. Esta tabla tendrá dos campos de tipo
float. Estos campos son radio y volumen. El procedimiento debe crear la tabla aunque esta exista.
*/

DELIMITER $$
DROP PROCEDURE IF EXISTS esferas;
CREATE PROCEDURE esferas ()
BEGIN
  DROP TABLE IF EXISTS esferas;
  CREATE TABLE IF NOT EXISTS esferas (
    di int AUTO_INCREMENT,
    radio float DEFAULT 0,
    volumen float DEFAULT 0,
    PRIMARY KEY (di)
  );
END$$
DELIMITER;

CALL
esferas();

SELECT
  *
FROM esferas e;

/* Crear un procedimiento almacenado que me permita introducir 100 radios de forma automática en la tabla anterior.
Estos radios deben estar entre dos valores pasados al procedimiento como argumentos de entrada.
*/

DELIMITER $$
DROP PROCEDURE IF EXISTS esferas;
CREATE PROCEDURE esferas ()
BEGIN

  DECLARE contador int DEFAULT 0;
  REPEAT
    SET contador = contador + 1;
    INSERT INTO esferas
      VALUES (DEFAULT, ROUND(RAND() * 10), DEFAULT);
  UNTIL (contador >= 100)
  END REPEAT;

END$$
DELIMITER;

CALL
esferas();

SELECT
  *
FROM esferas e;

/* Crear un procedimiento almacenado que me permita calcular los volúmenes de todos los registros de la tabla esferas
creada anteriormente. Debéis utilizar la función creada para este propósito
*/

DELIMITER $$
DROP PROCEDURE IF EXISTS esferas;
CREATE PROCEDURE esferasVolumenes ()
BEGIN

  ALTER TABLE esferas ADD COLUMN IF NOT EXISTS volumen float DEFAULT 0;
  UPDATE esferas e
  SET e.volumen = (SELECT
      volumenRadio(e.radio));

END$$
DELIMITER;

CALL
esferasVolumenes();

SELECT
  *
FROM esferas e;

/* Crear un procedimiento que reciba una palabra como argumento y la devuelva en ese mismo argumento escrita al
revés. 
*/

DELIMITER $$
DROP PROCEDURE IF EXISTS palabraAlReves;
CREATE PROCEDURE palabraAlReves (palabra varchar(50))
BEGIN

  SELECT
    REVERSE(palabra);

END$$
DELIMITER;

CALL
palabraAlReves('apetitoso');

/* Realizar el ejercicio anterior utilizando una función y el parámetro solamente de entrada
*/

DELIMITER $$
DROP FUNCTION IF EXISTS palabraAlReves;
CREATE FUNCTION palabraAlReves (palabra varchar(50))
RETURNS varchar(50)
BEGIN

  DECLARE reverso varchar(50);
  SET reverso = REVERSE(palabra);
  RETURN reverso;
END$$
DELIMITER;

SELECT
  palabraAlReves('apetitoso');

/* Realizar una función que calcule el factorial de un número.
*/

DELIMITER $$
DROP FUNCTION IF EXISTS factorial;
CREATE FUNCTION factorial (numero int)
RETURNS varchar(50)
BEGIN
  DECLARE factorial int DEFAULT 1;
  REPEAT

    SET factorial = factorial * numero;
    SET numero = numero - 1;

  UNTIL (numero = 0)
  END REPEAT;

  RETURN factorial;
END$$
DELIMITER;

SELECT
  factorial(5);