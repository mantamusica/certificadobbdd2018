﻿/* Crear un procedimiento almacenado denominado e1, que cree una tabla con dos campos denominados número y valor. 
  El campo número debe ser entero y clave principal. El campo valor debe ser texto de 10 e indexado sin duplicados y requerido si
*/

DELIMITER $$
DROP PROCEDURE IF EXISTS e1;
CREATE PROCEDURE e1 ()
BEGIN
  DROP TABLE IF EXISTS tabla;
  CREATE TABLE IF NOT EXISTS tabla (
    numero int AUTO_INCREMENT,
    valor varchar(10) NOT NULL,
    PRIMARY KEY (numero),
    UNIQUE INDEX(valor)
  );
END$$
DELIMITER;

CALL
e1();

SELECT
  *
FROM tabla;

/* Llamar al procedimiento anterior y después intentar volver a llamarlo. Se produce un error. ¿Cuál es su código y su SQLSTATE? 
*/

  -- La tabla ya existe.

-- En el siguiente enlace podemos ver los errores de mysql 
-- https://www.briandunning.com/error-codes/?source=MySQL

/* Vamos a añadir la siguiente línea a nuestro procedimiento almacenado para gestionar el error. Lo llamamos e4 al nuevo procedimiento. 
*/

DELIMITER $$
DROP PROCEDURE IF EXISTS e4$$
CREATE PROCEDURE e4 ()
BEGIN
  DECLARE EXIT HANDLER FOR 1050 SELECT 'ya existe la tabla' AS error;
  CREATE TABLE tabla (
    numero int AUTO_INCREMENT,
    valor varchar(10) NOT NULL,
    PRIMARY KEY (numero),
    UNIQUE INDEX(valor)
  )
    ENGINE = INNODB;
END$$
DELIMITER;

CALL
e4();

/* Modificar el procedimiento almacenado anterior para controlar el error por su SQLSTATE. Llamarlo e5. 
*/

DELIMITER $$
DROP PROCEDURE IF EXISTS e5$$
CREATE PROCEDURE e5 ()
BEGIN
  DECLARE EXIT HANDLER FOR SQLSTATE VALUE '42S01' SELECT 'ya existe la tabla' AS error;
  CREATE TABLE tabla (
    numero int AUTO_INCREMENT,
    valor varchar(10) NOT NULL,
    PRIMARY KEY (numero),
    UNIQUE INDEX(valor)
  )
    ENGINE = INNODB;
END$$
DELIMITER;

CALL
e5();

/* Hemos modificado el procedimiento almacenado anterior para controlar el error de otra forma. Lo llamo e6. ¿Qué es lo que realiza? 
*/

DELIMITER $$
DROP PROCEDURE IF EXISTS e6$$
CREATE PROCEDURE e6 ()
BEGIN
  DECLARE EXIT HANDLER FOR SQLEXCEPTION SELECT 'ya existe la tabla' AS error;
  CREATE TABLE tabla (
    numero int AUTO_INCREMENT,
    valor varchar(10) NOT NULL,
    PRIMARY KEY (numero),
    UNIQUE INDEX(valor)
  )
    ENGINE = INNODB;
END$$
DELIMITER;

CALL
e6();

/* Explicar el código del siguiente procedimiento almacenado
*/

DELIMITER $$
DROP PROCEDURE IF EXISTS e7$$
CREATE PROCEDURE e7 ()
BEGIN
  DECLARE CONTINUE HANDLER FOR 1051 SELECT 'No encuentro la tabla' AS error;
  -- controla el error de tabla si esta no existe
  DECLARE EXIT HANDLER FOR SQLEXCEPTION SELECT 'ya existe la tabla' AS error;
  DROP TABLE ejemplo1;
  CREATE TABLE ejemplo1 (
    numero int AUTO_INCREMENT,
    valor varchar(10) NOT NULL,
    PRIMARY KEY (numero),
    UNIQUE INDEX(valor)
  )
    ENGINE = INNODB;
END$$
DELIMITER;

CALL
e7();

/* Analizar el siguiente procedimiento almacenado. Corregir el error que se produce. 
*/

DELIMITER $$
DROP PROCEDURE IF EXISTS e8$$
CREATE PROCEDURE e8 ()
BEGIN

  DECLARE bien int DEFAULT 1;
  DECLARE codigo int DEFAULT 0;

  DECLARE error1 CONDITION FOR 1050;
  DECLARE error2 CONDITION FOR 1051;

  DECLARE CONTINUE HANDLER FOR error1 set bien=0,codigo=1;
  DECLARE CONTINUE HANDLER FOR error2 set bien=0,codigo=2;

  DROP TABLE ejemplo31;
  CREATE TABLE ejemplo2 (
    numero int AUTO_INCREMENT,
    valor varchar(10) NOT NULL,
    PRIMARY KEY (numero),
    UNIQUE INDEX(valor)
  )
    ENGINE = INNODB;

  IF(bien=0) THEN
  SELECT 'algo mal' AS errores;
  IF(codigo=1) THEN 
    DROP TABLE ejemplo2;
  ELSEIF (codigo=2) THEN
    CREATE TABLE ejemplo31 (
    numero int AUTO_INCREMENT,
    valor varchar(10) NOT NULL,
    PRIMARY KEY (numero),
    UNIQUE INDEX(valor)
  )
    ENGINE = INNODB; 
    END IF;
  END IF;
END$$
DELIMITER;

CALL
e8();

/* Crear un procedimiento almacenado que permita introducir valores a la tabla ejemplo1. 
  Se pasaran dos argumentos que es el numero a introducir y el texto. 
  El procedimiento almacenado debe controlar los siguientes errores presentando el mensaje ‘hay errores’ y continuando con la ejecución.
  Para ello quiero que utilicéis CONDITION y después solamente HANDLERs
*/

DELIMITER $$
DROP PROCEDURE IF EXISTS ej9$$
CREATE PROCEDURE ej9 (numero integer, texto varchar(50))
BEGIN

  DECLARE bien int DEFAULT 1;
  DECLARE codigo int DEFAULT 0;

  DECLARE error1 CONDITION FOR 1061; -- repetimos key
  DECLARE error2 CONDITION FOR 1051; -- tabla desconocida
  DECLARE error3 CONDITION FOR 1048; -- no puede ser null


  DECLARE CONTINUE HANDLER FOR error1 set bien=0,codigo=1;
  DECLARE CONTINUE HANDLER FOR error2 set bien=0,codigo=2;
  DECLARE CONTINUE HANDLER FOR error3 set bien=0,codigo=3;


  INSERT INTO ejemplo1 VALUES(numero,texto);

  IF(bien=0) AND (codigo=1) THEN SELECT 'algo va mal revise los datos la tabla no existe.' AS errores;
  END IF;
  IF(bien=0) AND (codigo=2) THEN SELECT 'algo va mal repetimos codigo primary.' AS errores;
  END IF;
  IF(bien=0) AND (codigo=2) THEN SELECT 'algo va mal valor null.' AS errores;
  END IF;

END$$
DELIMITER;

CALL ej9(3,null);

SELECT * FROM ejemplo1 e;

/* En caso de no poder introducir el valor debe colocar en pantalla los siguientes mensajes personalizados.
  a. El campo número es clave principal y no admite valores repetidos 
  b. El campo número es clave principal y no admite valores nulos .
  c. El campo valor es requerido y no admite nulos 
  d. El campo valor es indexado sin duplicados y no admite nulos
*/

