﻿/* ¿Qué es lo que realiza el siguiente script?. ¿Cuál es la salida? 
*/

DELIMITER $$
DROP PROCEDURE IF EXISTS e1;
CREATE PROCEDURE e1 ()
BEGIN
  DECLARE variable1 varchar(50);
  DECLARE cursor1 CURSOR FOR

  SELECT c.NombCli FROM clientes c ORDER BY c.NombCli;

  OPEN cursor1;
  FETCH cursor1 INTO variable1;
  CLOSE cursor1;
  SELECT variable1;
END$$
DELIMITER;

CALL
e1();

/* Analizar el siguiente procedimiento almacenado. ¿Qué salida produce? 
*/

DELIMITER $$
DROP PROCEDURE IF EXISTS e2;
CREATE PROCEDURE e2 ()
BEGIN
  DECLARE variable1 varchar(50);

  DECLARE cursor1 CURSOR FOR
  SELECT c.NombCli FROM clientes c ORDER BY c.NombCli;

    DECLARE cursor2 CURSOR FOR
  SELECT l.NombLoc FROM localidades l ORDER BY l.CodLoc;

  OPEN cursor1;
  FETCH cursor1 INTO variable1;
  OPEN cursor2;
  FETCH cursor2 INTO variable1;
  CLOSE cursor1;
  CLOSE cursor2;
  SELECT variable1;
END$$
DELIMITER;

CALL
e2();

/* Analizar el siguiente procedimiento almacenado e indicar que salida produce. 
*/

DELIMITER $$
DROP PROCEDURE IF EXISTS e3;
CREATE PROCEDURE e3 ()
BEGIN
  DECLARE variable1 varchar(50);
  DECLARE numero int;

  DECLARE cursor1 CURSOR FOR
  SELECT c.NombCli FROM clientes c ORDER BY c.NombCli;

  SELECT COUNT(*) INTO numero FROM clientes c;

  OPEN cursor1;
  while(numero>0) DO
  FETCH cursor1 INTO variable1;
  SET variable1=CONCAT_WS(',',variable1);
  SET numero=numero -1;
  END WHILE;
  CLOSE cursor1;

  SELECT variable1;
END$$
DELIMITER;

CALL
e3();

/* Modificar el procedimiento almacenado anterior para controlar el error por su SQLSTATE. Llamarlo e5. 
*/

DELIMITER $$
DROP PROCEDURE IF EXISTS e5$$
CREATE PROCEDURE e5 ()
BEGIN
  DECLARE EXIT HANDLER FOR SQLSTATE VALUE '42S01' SELECT 'ya existe la tabla' AS error;
  CREATE TABLE tabla (
    numero int AUTO_INCREMENT,
    valor varchar(10) NOT NULL,
    PRIMARY KEY (numero),
    UNIQUE INDEX(valor)
  )
    ENGINE = INNODB;
END$$
DELIMITER;

CALL
e5();

/* Hemos modificado el procedimiento almacenado anterior para controlar el error de otra forma. Lo llamo e6. ¿Qué es lo que realiza? 
*/

DELIMITER $$
DROP PROCEDURE IF EXISTS e6$$
CREATE PROCEDURE e6 ()
BEGIN
  DECLARE EXIT HANDLER FOR SQLEXCEPTION SELECT 'ya existe la tabla' AS error;
  CREATE TABLE tabla (
    numero int AUTO_INCREMENT,
    valor varchar(10) NOT NULL,
    PRIMARY KEY (numero),
    UNIQUE INDEX(valor)
  )
    ENGINE = INNODB;
END$$
DELIMITER;

CALL
e6();

/* Explicar el código del siguiente procedimiento almacenado
*/

DELIMITER $$
DROP PROCEDURE IF EXISTS e7$$
CREATE PROCEDURE e7 ()
BEGIN
  DECLARE CONTINUE HANDLER FOR 1051 SELECT 'No encuentro la tabla' AS error;
  -- controla el error de tabla si esta no existe
  DECLARE EXIT HANDLER FOR SQLEXCEPTION SELECT 'ya existe la tabla' AS error;
  DROP TABLE ejemplo1;
  CREATE TABLE ejemplo1 (
    numero int AUTO_INCREMENT,
    valor varchar(10) NOT NULL,
    PRIMARY KEY (numero),
    UNIQUE INDEX(valor)
  )
    ENGINE = INNODB;
END$$
DELIMITER;

CALL
e7();

/* Analizar el siguiente procedimiento almacenado. Corregir el error que se produce. 
*/

DELIMITER $$
DROP PROCEDURE IF EXISTS e8$$
CREATE PROCEDURE e8 ()
BEGIN

  DECLARE bien int DEFAULT 1;
  DECLARE codigo int DEFAULT 0;

  DECLARE error1 CONDITION FOR 1050; -- La tabla '%s' ya existe
  DECLARE error2 CONDITION FOR 1051; -- Tabla '%s' desconocida

  DECLARE CONTINUE HANDLER FOR error1 set bien=0,codigo=1;
  DECLARE CONTINUE HANDLER FOR error2 set bien=0,codigo=2;

  DROP TABLE ejemplo31;
  CREATE TABLE ejemplo2 (
    numero int AUTO_INCREMENT,
    valor varchar(10) NOT NULL,
    PRIMARY KEY (numero),
    UNIQUE INDEX(valor)
  )
    ENGINE = INNODB;

  IF((bien=0) AND (codigo=1)) THEN 
    DROP TABLE IF EXISTS ejemplo2;
    SELECT 'Tabla desconocida' AS errores;
    END IF;
  IF((bien=0) AND (codigo=2)) THEN
    SELECT 'La tabla ya existe' AS errores;
    END IF;
END$$
DELIMITER;

CALL
e8();

/* Crear un procedimiento almacenado que permita introducir valores a la tabla ejemplo1. 
  Se pasaran dos argumentos que es el numero a introducir y el texto. 
  El procedimiento almacenado debe controlar los siguientes errores presentando el mensaje ‘hay errores’ y continuando con la ejecución.
  Para ello quiero que utilicéis CONDITION y después solamente HANDLERs
*/

DELIMITER $$
DROP PROCEDURE IF EXISTS ej9$$
CREATE PROCEDURE ej9 (numero integer, texto varchar(50))
BEGIN

  DECLARE bien int DEFAULT 1;
  DECLARE codigo int DEFAULT 0;

  DECLARE error0 CONDITION FOR 1062; -- Entrada duplicada '%s' para la clave %d
  DECLARE error1 CONDITION FOR 1061; -- Nombre de clave duplicado '%s'
  DECLARE error2 CONDITION FOR 1051; -- Tabla '%s' desconocida
  DECLARE error3 CONDITION FOR 1048; -- La columna '%s' no puede ser nula

  DECLARE CONTINUE HANDLER FOR error0 set bien=0;
  DECLARE CONTINUE HANDLER FOR error1 set bien=0;
  DECLARE CONTINUE HANDLER FOR error2 set bien=0;
  DECLARE CONTINUE HANDLER FOR error3 set bien=0;

  INSERT INTO ejemplo VALUES(numero,texto);

  IF(bien=0) THEN SELECT 'algo mal' AS errores;
  END IF;

END$$
DELIMITER;

CALL ej9(3,'sdgf');

SELECT * FROM ejemplo1 e;

/* En caso de no poder introducir el valor debe colocar en pantalla los siguientes mensajes personalizados.
  a. El campo número es clave principal y no admite valores repetidos 
  b. El campo número es clave principal y no admite valores nulos .
  c. El campo valor es requerido y no admite nulos 
  d. El campo valor es indexado sin duplicados y no admite nulos
*/

