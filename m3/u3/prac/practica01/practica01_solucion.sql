﻿DROP DATABASE IF EXISTS practica01apoyo;

CREATE DATABASE IF NOT EXISTS practica01apoyo;

SET NAMES 'utf8';

--
-- Ejericicio02
--
USE practica01apoyo;

CALL ejercicio01(5);

SELECT * FROM ejercicio01;

CALL ejercicio02(5);

SELECT * FROM ejercicio02;

CALL ejercicio03(5);

SELECT * FROM ejercicio03;

SELECT ejercicio01(5);

DROP PROCEDURE IF EXISTS practica01apoyo.ejercicio10;

CALL ejercicio10(10,21);
CALL ejercicio10(9,21);
CALL ejercicio10(10,11);

SELECT * FROM ejercicio10;


SELECT ejercicio07(5,5);

SELECT ejercicio08(5,5);

SELECT ejercicio09(5,5);

