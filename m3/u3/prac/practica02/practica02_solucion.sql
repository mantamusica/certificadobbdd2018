﻿-- DROP DATABASE IF EXISTS practica02apoyo;

-- CREATE DATABASE IF NOT EXISTS practica02apoyo;

SET NAMES 'utf8';

USE practica02apoyo;


--
-- Ejericicio02
--

CALL ejercicio02();

--
-- Ejericicio07
--

CALL ejercicio07();



--
-- Ejericicio08
--

SELECT edad('1984-02-09');

CALL ejercicio08();


--
-- Ejericicio09
--

SELECT * FROM menores18;
SELECT * FROM mayores18;

CALL ejercicio09();

SELECT * FROM menores;
SELECT * FROM no_menores;


--
-- Ejericicio10
--

  CALL ejercicio10_numeros();
  SELECT * FROM numeros;
  CALL ejercicio10_alfabeto();
  SELECT * FROM abecedario;