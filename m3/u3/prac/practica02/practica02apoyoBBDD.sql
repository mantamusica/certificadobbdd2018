﻿--
-- Script was generated by Devart dbForge Studio for MySQL, Version 8.0.40.0
-- Product home page: http://www.devart.com/dbforge/mysql/studio
-- Script date 13/02/2019 9:23:45
-- Server version: 5.5.5-10.1.36-MariaDB
-- Client version: 4.1
--

-- 
-- Disable foreign keys
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Set SQL mode
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Set character set the client will use to send SQL statements to the server
--
SET NAMES 'utf8';

--
-- Set default database
--
USE practica02apoyo;

--
-- Drop procedure `ejercicio10_alfabeto`
--
DROP PROCEDURE IF EXISTS ejercicio10_alfabeto;

--
-- Drop table `abecedario`
--
DROP TABLE IF EXISTS abecedario;

--
-- Drop procedure `ejercicio10_numeros`
--
DROP PROCEDURE IF EXISTS ejercicio10_numeros;

--
-- Drop table `numeros`
--
DROP TABLE IF EXISTS numeros;

--
-- Drop procedure `ejercicio09`
--
DROP PROCEDURE IF EXISTS ejercicio09;

--
-- Drop procedure `borrarTablas`
--
DROP PROCEDURE IF EXISTS borrarTablas;

--
-- Drop procedure `rellenarMenores18`
--
DROP PROCEDURE IF EXISTS rellenarMenores18;

--
-- Drop table `menores`
--
DROP TABLE IF EXISTS menores;

--
-- Drop procedure `rellenarMayores18`
--
DROP PROCEDURE IF EXISTS rellenarMayores18;

--
-- Drop table `no_menores`
--
DROP TABLE IF EXISTS no_menores;

--
-- Drop view `mayores18`
--
DROP VIEW IF EXISTS mayores18 CASCADE;

--
-- Drop view `menores18`
--
DROP VIEW IF EXISTS menores18 CASCADE;

--
-- Drop procedure `ejercicio02`
--
DROP PROCEDURE IF EXISTS ejercicio02;

--
-- Drop procedure `ejercicio08`
--
DROP PROCEDURE IF EXISTS ejercicio08;

--
-- Drop function `edad`
--
DROP FUNCTION IF EXISTS edad;

--
-- Drop view `nombresapellidos`
--
DROP VIEW IF EXISTS nombresapellidos CASCADE;

--
-- Drop procedure `ejercicio07`
--
DROP PROCEDURE IF EXISTS ejercicio07;

--
-- Drop table `personas`
--
DROP TABLE IF EXISTS personas;

--
-- Set default database
--
USE practica02apoyo;

--
-- Create table `personas`
--
CREATE TABLE personas (
  id int(11) NOT NULL AUTO_INCREMENT,
  dni varchar(9) DEFAULT NULL,
  nombre varchar(30) DEFAULT NULL,
  apellidos varchar(30) DEFAULT NULL,
  edad int(11) DEFAULT NULL,
  fecha_nacimiento date DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 5,
AVG_ROW_LENGTH = 4096,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create index `apellidos` on table `personas`
--
ALTER TABLE personas
ADD UNIQUE INDEX apellidos (apellidos);

--
-- Create index `apellidos_2` on table `personas`
--
ALTER TABLE personas
ADD UNIQUE INDEX apellidos_2 (apellidos);

--
-- Create index `apellidos_3` on table `personas`
--
ALTER TABLE personas
ADD UNIQUE INDEX apellidos_3 (apellidos);

--
-- Create index `dni` on table `personas`
--
ALTER TABLE personas
ADD UNIQUE INDEX dni (dni);

--
-- Create index `dni_2` on table `personas`
--
ALTER TABLE personas
ADD UNIQUE INDEX dni_2 (dni);

--
-- Create index `dni_3` on table `personas`
--
ALTER TABLE personas
ADD UNIQUE INDEX dni_3 (dni);

--
-- Create index `fecha_nacimiento` on table `personas`
--
ALTER TABLE personas
ADD INDEX fecha_nacimiento (fecha_nacimiento);

--
-- Create index `fecha_nacimiento_2` on table `personas`
--
ALTER TABLE personas
ADD INDEX fecha_nacimiento_2 (fecha_nacimiento);

--
-- Create index `fecha_nacimiento_3` on table `personas`
--
ALTER TABLE personas
ADD INDEX fecha_nacimiento_3 (fecha_nacimiento);

--
-- Create index `nombre` on table `personas`
--
ALTER TABLE personas
ADD UNIQUE INDEX nombre (nombre);

--
-- Create index `nombre_2` on table `personas`
--
ALTER TABLE personas
ADD UNIQUE INDEX nombre_2 (nombre);

--
-- Create index `nombre_3` on table `personas`
--
ALTER TABLE personas
ADD UNIQUE INDEX nombre_3 (nombre);

DELIMITER $$

--
-- Create procedure `ejercicio07`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE ejercicio07 ()
COMMENT 'Creación de claves para la tabla personas'
BEGIN
  ALTER TABLE `personas` ADD UNIQUE (`dni`);
  ALTER TABLE `personas` ADD UNIQUE (`nombre`);
  ALTER TABLE `personas` ADD UNIQUE (`apellidos`);
  ALTER TABLE `personas` ADD INDEX (`fecha_nacimiento`);
END
$$

DELIMITER ;

--
-- Create view `nombresapellidos`
--
CREATE
DEFINER = 'root'@'localhost'
VIEW nombresapellidos
AS
SELECT
  CONCAT_WS(', ', `p`.`apellidos`, `p`.`nombre`) AS `nombre_completo`
FROM `personas` `p`;

DELIMITER $$

--
-- Create function `edad`
--
CREATE DEFINER = 'root'@'localhost'
FUNCTION edad (fechaNacimiento date)
RETURNS int(11)
COMMENT 'Función para calcular la edad'
BEGIN
  DECLARE numero int;
  SET numero = TIMESTAMPDIFF(year, fechaNacimiento, NOW());
  RETURN numero;
END
$$

--
-- Create procedure `ejercicio08`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE ejercicio08 ()
COMMENT 'Procedimiento para la insercción de la edad en la tabla personas utilizando la función edad'
BEGIN
  DECLARE contador int DEFAULT 0;
  DECLARE totalRegistros int DEFAULT 0;
  DECLARE fecha date DEFAULT NULL;
  SET totalRegistros = (SELECT
      COUNT(*)
    FROM personas);

  REPEAT
    SET contador = contador + 1;
    SET fecha = (SELECT
        fecha_nacimiento
      FROM personas d
      WHERE d.id = contador);

    UPDATE personas
    SET edad = edad(fecha)
    WHERE id = contador;
  UNTIL contador = totalRegistros
  END REPEAT;

END
$$

--
-- Create procedure `ejercicio02`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE ejercicio02 ()
COMMENT 'Creación de tabla personas'
BEGIN

  CREATE TABLE IF NOT EXISTS personas (
    id int PRIMARY KEY AUTO_INCREMENT,
    dni varchar(9),
    nombre varchar(30),
    apellidos varchar(30),
    edad int,
    fecha_nacimiento date
  )
  ;
END
$$

DELIMITER ;

--
-- Create view `menores18`
--
CREATE
DEFINER = 'root'@'localhost'
VIEW menores18
AS
SELECT
  `personas`.`id` AS `id`,
  `personas`.`dni` AS `dni`,
  `personas`.`nombre` AS `nombre`,
  `personas`.`apellidos` AS `apellidos`,
  `personas`.`edad` AS `edad`,
  `personas`.`fecha_nacimiento` AS `fecha_nacimiento`
FROM `personas`
WHERE (`personas`.`edad` < 18);

--
-- Create view `mayores18`
--
CREATE
DEFINER = 'root'@'localhost'
VIEW mayores18
AS
SELECT
  `p`.`id` AS `id`,
  `p`.`nombre` AS `nombre`,
  `p`.`apellidos` AS `apellidos`,
  `p`.`edad` AS `edad`
FROM (`personas` `p`
  LEFT JOIN `menores18` `m`
    ON ((`p`.`id` = `m`.`id`)))
WHERE ISNULL(`m`.`id`);

--
-- Create table `no_menores`
--
CREATE TABLE no_menores (
  id int(11) NOT NULL AUTO_INCREMENT,
  nombre varchar(30) DEFAULT NULL,
  apellidos varchar(30) DEFAULT NULL,
  edad int(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 3,
AVG_ROW_LENGTH = 8192,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

DELIMITER $$

--
-- Create procedure `rellenarMayores18`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE rellenarMayores18 ()
COMMENT 'Procedimiento que crea tabla menores e inserta los que haya en la tabla personas'
BEGIN
  CREATE TABLE no_menores (
    id int AUTO_INCREMENT PRIMARY KEY,
    nombre varchar(30),
    apellidos varchar(30),
    edad int
  );

  INSERT INTO no_menores (nombre, apellidos, edad)
    SELECT
      nombre,
      apellidos,
      edad
    FROM mayores18;

END
$$

DELIMITER ;

--
-- Create table `menores`
--
CREATE TABLE menores (
  id int(11) NOT NULL AUTO_INCREMENT,
  nombre varchar(30) DEFAULT NULL,
  apellidos varchar(30) DEFAULT NULL,
  edad int(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 3,
AVG_ROW_LENGTH = 8192,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

DELIMITER $$

--
-- Create procedure `rellenarMenores18`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE rellenarMenores18 ()
COMMENT 'Procedimiento que crea tabla  no menores e inserta los que haya en la tabla personas'
BEGIN
  CREATE TABLE menores (
    id int AUTO_INCREMENT PRIMARY KEY,
    nombre varchar(30),
    apellidos varchar(30),
    edad int
  );

  INSERT INTO menores (nombre, apellidos, edad)
    SELECT
      nombre,
      apellidos,
      edad
    FROM menores18;

END
$$

--
-- Create procedure `borrarTablas`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE borrarTablas ()
COMMENT 'Procedimiento para el borrado de tablas menores y no_menores'
BEGIN
  DROP TABLE IF EXISTS menores;
  DROP TABLE IF EXISTS no_menores;
END
$$

--
-- Create procedure `ejercicio09`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE ejercicio09 ()
COMMENT 'Procedimiento para insertar los datos de personas en dos tablas, menores de 18 y no menores'
BEGIN

  CALL borrarTablas();

  CALL rellenarMenores18();
  CALL rellenarMayores18();

END
$$

DELIMITER ;

--
-- Create table `numeros`
--
CREATE TABLE numeros (
  numeros int(11) DEFAULT NULL
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 65,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

DELIMITER $$

--
-- Create procedure `ejercicio10_numeros`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE ejercicio10_numeros ()
COMMENT 'Procedimiento para el listado de 1000 números'
BEGIN
  DECLARE contador int DEFAULT 0;
  CREATE TABLE IF NOT EXISTS numeros (
    numeros int
  );
  REPEAT
    SET contador = contador + 1;
    INSERT INTO numeros
    SET numeros = contador;
  UNTIL contador = 1000
  END REPEAT;
END
$$

DELIMITER ;

--
-- Create table `abecedario`
--
CREATE TABLE abecedario (
  letras char(1) DEFAULT NULL
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 606,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

DELIMITER $$

--
-- Create procedure `ejercicio10_alfabeto`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE ejercicio10_alfabeto ()
COMMENT 'Procedimiento para el listado del abecedario letra a letra'
BEGIN
  DECLARE contador int DEFAULT 0;
  DECLARE alfabeto varchar(50) DEFAULT 'abcdefghijklmnñopqrstuvwxyz';
  DECLARE registros int DEFAULT 0;
  SET registros = LENGTH(alfabeto);

  CREATE TABLE IF NOT EXISTS abecedario (
    letras char(1)
  );
  REPEAT
    SET contador = contador + 1;
    INSERT INTO abecedario
    SET letras = SUBSTRING(alfabeto, contador, 1);
  UNTIL contador = registros
  END REPEAT;
END
$$

DELIMITER ;

-- 
-- Dumping data for table personas
--
INSERT INTO personas VALUES
(1, '20202020d', 'carlos', 'cifuentes', 31, '1987-12-12'),
(2, '30303030d', 'raul', 'maria', 16, '2002-11-12'),
(3, '35353535g', 'eva', 'manrique', 17, '2001-11-20'),
(4, '78451233g', 'maria', 'martinez', 28, '1991-01-12');

-- 
-- Dumping data for table numeros
--
INSERT INTO numeros VALUES
(1),
(2),
(3),
(4),
(5),
(6),
(7),
(8),
(9),
(10),
(11),
(12),
(13),
(14),
(15),
(16),
(17),
(18),
(19),
(20),
(21),
(22),
(23),
(24),
(25),
(26),
(27),
(28),
(29),
(30),
(31),
(32),
(33),
(34),
(35),
(36),
(37),
(38),
(39),
(40),
(41),
(42),
(43),
(44),
(45),
(46),
(47),
(48),
(49),
(50),
(51),
(52),
(53),
(54),
(55),
(56),
(57),
(58),
(59),
(60),
(61),
(62),
(63),
(64),
(65),
(66),
(67),
(68),
(69),
(70),
(71),
(72),
(73),
(74),
(75),
(76),
(77),
(78),
(79),
(80),
(81),
(82),
(83),
(84),
(85),
(86),
(87),
(88),
(89),
(90),
(91),
(92),
(93),
(94),
(95),
(96),
(97),
(98),
(99),
(100),
(101),
(102),
(103),
(104),
(105),
(106),
(107),
(108),
(109),
(110),
(111),
(112),
(113),
(114),
(115),
(116),
(117),
(118),
(119),
(120),
(121),
(122),
(123),
(124),
(125),
(126),
(127),
(128),
(129),
(130),
(131),
(132),
(133),
(134),
(135),
(136),
(137),
(138),
(139),
(140),
(141),
(142),
(143),
(144),
(145),
(146),
(147),
(148),
(149),
(150),
(151),
(152),
(153),
(154),
(155),
(156),
(157),
(158),
(159),
(160),
(161),
(162),
(163),
(164),
(165),
(166),
(167),
(168),
(169),
(170),
(171),
(172),
(173),
(174),
(175),
(176),
(177),
(178),
(179),
(180),
(181),
(182),
(183),
(184),
(185),
(186),
(187),
(188),
(189),
(190),
(191),
(192),
(193),
(194),
(195),
(196),
(197),
(198),
(199),
(200),
(201),
(202),
(203),
(204),
(205),
(206),
(207),
(208),
(209),
(210),
(211),
(212),
(213),
(214),
(215),
(216),
(217),
(218),
(219),
(220),
(221),
(222),
(223),
(224),
(225),
(226),
(227),
(228),
(229),
(230),
(231),
(232),
(233),
(234),
(235),
(236),
(237),
(238),
(239),
(240),
(241),
(242),
(243),
(244),
(245),
(246),
(247),
(248),
(249),
(250),
(251),
(252),
(253),
(254),
(255),
(256),
(257),
(258),
(259),
(260),
(261),
(262),
(263),
(264),
(265),
(266),
(267),
(268),
(269),
(270),
(271),
(272),
(273),
(274),
(275),
(276),
(277),
(278),
(279),
(280),
(281),
(282),
(283),
(284),
(285),
(286),
(287),
(288),
(289),
(290),
(291),
(292),
(293),
(294),
(295),
(296),
(297),
(298),
(299),
(300),
(301),
(302),
(303),
(304),
(305),
(306),
(307),
(308),
(309),
(310),
(311),
(312),
(313),
(314),
(315),
(316),
(317),
(318),
(319),
(320),
(321),
(322),
(323),
(324),
(325),
(326),
(327),
(328),
(329),
(330),
(331),
(332),
(333),
(334),
(335),
(336),
(337),
(338),
(339),
(340),
(341),
(342),
(343),
(344),
(345),
(346),
(347),
(348),
(349),
(350),
(351),
(352),
(353),
(354),
(355),
(356),
(357),
(358),
(359),
(360),
(361),
(362),
(363),
(364),
(365),
(366),
(367),
(368),
(369),
(370),
(371),
(372),
(373),
(374),
(375),
(376),
(377),
(378),
(379),
(380),
(381),
(382),
(383),
(384),
(385),
(386),
(387),
(388),
(389),
(390),
(391),
(392),
(393),
(394),
(395),
(396),
(397),
(398),
(399),
(400),
(401),
(402),
(403),
(404),
(405),
(406),
(407),
(408),
(409),
(410),
(411),
(412),
(413),
(414),
(415),
(416),
(417),
(418),
(419),
(420),
(421),
(422),
(423),
(424),
(425),
(426),
(427),
(428),
(429),
(430),
(431),
(432),
(433),
(434),
(435),
(436),
(437),
(438),
(439),
(440),
(441),
(442),
(443),
(444),
(445),
(446),
(447),
(448),
(449),
(450),
(451),
(452),
(453),
(454),
(455),
(456),
(457),
(458),
(459),
(460),
(461),
(462),
(463),
(464),
(465),
(466),
(467),
(468),
(469),
(470),
(471),
(472),
(473),
(474),
(475),
(476),
(477),
(478),
(479),
(480),
(481),
(482),
(483),
(484),
(485),
(486),
(487),
(488),
(489),
(490),
(491),
(492),
(493),
(494),
(495),
(496),
(497),
(498),
(499),
(500),
(501),
(502),
(503),
(504),
(505),
(506),
(507),
(508),
(509),
(510),
(511),
(512),
(513),
(514),
(515),
(516),
(517),
(518),
(519),
(520),
(521),
(522),
(523),
(524),
(525),
(526),
(527),
(528),
(529),
(530),
(531),
(532),
(533),
(534),
(535),
(536),
(537),
(538),
(539),
(540),
(541),
(542),
(543),
(544),
(545),
(546),
(547),
(548),
(549),
(550),
(551),
(552),
(553),
(554),
(555),
(556),
(557),
(558),
(559),
(560),
(561),
(562),
(563),
(564),
(565),
(566),
(567),
(568),
(569),
(570),
(571),
(572),
(573),
(574),
(575),
(576),
(577),
(578),
(579),
(580),
(581),
(582),
(583),
(584),
(585),
(586),
(587),
(588),
(589),
(590),
(591),
(592),
(593),
(594),
(595),
(596),
(597),
(598),
(599),
(600),
(601),
(602),
(603),
(604),
(605),
(606),
(607),
(608),
(609),
(610),
(611),
(612),
(613),
(614),
(615),
(616),
(617),
(618),
(619),
(620),
(621),
(622),
(623),
(624),
(625),
(626),
(627),
(628),
(629),
(630),
(631),
(632),
(633),
(634),
(635),
(636),
(637),
(638),
(639),
(640),
(641),
(642),
(643),
(644),
(645),
(646),
(647),
(648),
(649),
(650),
(651),
(652),
(653),
(654),
(655),
(656),
(657),
(658),
(659),
(660),
(661),
(662),
(663),
(664),
(665),
(666),
(667),
(668),
(669),
(670),
(671),
(672),
(673),
(674),
(675),
(676),
(677),
(678),
(679),
(680),
(681),
(682),
(683),
(684),
(685),
(686),
(687),
(688),
(689),
(690),
(691),
(692),
(693),
(694),
(695),
(696),
(697),
(698),
(699),
(700),
(701),
(702),
(703),
(704),
(705),
(706),
(707),
(708),
(709),
(710),
(711),
(712),
(713),
(714),
(715),
(716),
(717),
(718),
(719),
(720),
(721),
(722),
(723),
(724),
(725),
(726),
(727),
(728),
(729),
(730),
(731),
(732),
(733),
(734),
(735),
(736),
(737),
(738),
(739),
(740),
(741),
(742),
(743),
(744),
(745),
(746),
(747),
(748),
(749),
(750),
(751),
(752),
(753),
(754),
(755),
(756),
(757),
(758),
(759),
(760),
(761),
(762),
(763),
(764),
(765),
(766),
(767),
(768),
(769),
(770),
(771),
(772),
(773),
(774),
(775),
(776),
(777),
(778),
(779),
(780),
(781),
(782),
(783),
(784),
(785),
(786),
(787),
(788),
(789),
(790),
(791),
(792),
(793),
(794),
(795),
(796),
(797),
(798),
(799),
(800),
(801),
(802),
(803),
(804),
(805),
(806),
(807),
(808),
(809),
(810),
(811),
(812),
(813),
(814),
(815),
(816),
(817),
(818),
(819),
(820),
(821),
(822),
(823),
(824),
(825),
(826),
(827),
(828),
(829),
(830),
(831),
(832),
(833),
(834),
(835),
(836),
(837),
(838),
(839),
(840),
(841),
(842),
(843),
(844),
(845),
(846),
(847),
(848),
(849),
(850),
(851),
(852),
(853),
(854),
(855),
(856),
(857),
(858),
(859),
(860),
(861),
(862),
(863),
(864),
(865),
(866),
(867),
(868),
(869),
(870),
(871),
(872),
(873),
(874),
(875),
(876),
(877),
(878),
(879),
(880),
(881),
(882),
(883),
(884),
(885),
(886),
(887),
(888),
(889),
(890),
(891),
(892),
(893),
(894),
(895),
(896),
(897),
(898),
(899),
(900),
(901),
(902),
(903),
(904),
(905),
(906),
(907),
(908),
(909),
(910),
(911),
(912),
(913),
(914),
(915),
(916),
(917),
(918),
(919),
(920),
(921),
(922),
(923),
(924),
(925),
(926),
(927),
(928),
(929),
(930),
(931),
(932),
(933),
(934),
(935),
(936),
(937),
(938),
(939),
(940),
(941),
(942),
(943),
(944),
(945),
(946),
(947),
(948),
(949),
(950),
(951),
(952),
(953),
(954),
(955),
(956),
(957),
(958),
(959),
(960),
(961),
(962),
(963),
(964),
(965),
(966),
(967),
(968),
(969),
(970),
(971),
(972),
(973),
(974),
(975),
(976),
(977),
(978),
(979),
(980),
(981),
(982),
(983),
(984),
(985),
(986),
(987),
(988),
(989),
(990),
(991),
(992),
(993),
(994),
(995),
(996),
(997),
(998),
(999),
(1000);

-- 
-- Dumping data for table no_menores
--
INSERT INTO no_menores VALUES
(1, 'carlos', 'cifuentes', 31),
(2, 'maria', 'martinez', 28);

-- 
-- Dumping data for table menores
--
INSERT INTO menores VALUES
(1, 'raul', 'maria', 16),
(2, 'eva', 'manrique', 17);

-- 
-- Dumping data for table abecedario
--
INSERT INTO abecedario VALUES
('a'),
('b'),
('c'),
('d'),
('e'),
('f'),
('g'),
('h'),
('i'),
('j'),
('k'),
('l'),
('m'),
('n'),
('ñ'),
('o'),
('p'),
('q'),
('r'),
('s'),
('t'),
('u'),
('v'),
('w'),
('x'),
('y'),
('z');

-- 
-- Restore previous SQL mode
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Enable foreign keys
-- 
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;