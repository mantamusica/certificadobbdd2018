$(document).ready(function () {
var data = [{"text":"Cover Page","icon":"images/folder.svg","href":"startpage.html","target":"DATA"},{"text":"Servers","icon":"images/folder.svg","href":"Servers\\Servers.html","target":"DATA"},{"text":"localhost","icon":"images/folder.svg","href":"Servers\\localhost\\localhost.html","target":"DATA","nodes":[{"text":"Databases","icon":"images/folder.svg","href":"Servers\\localhost\\Databases\\Databases.html","target":"DATA","nodes":[{"text":"practica02apoyo","icon":"images/database.svg","href":"Servers\\localhost\\Databases\\practica02apoyo\\practica02apoyo.html","target":"DATA","nodes":[{"text":"Tables","icon":"images/folder.svg","href":"Servers\\localhost\\Databases\\practica02apoyo\\Tables\\Tables.html","target":"DATA","nodes":[{"text":"abecedario","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica02apoyo\\Tables\\abecedario.html","target":"DATA"},{"text":"menores","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica02apoyo\\Tables\\menores.html","target":"DATA"},{"text":"no_menores","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica02apoyo\\Tables\\no_menores.html","target":"DATA"},{"text":"numeros","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica02apoyo\\Tables\\numeros.html","target":"DATA"},{"text":"personas","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica02apoyo\\Tables\\personas.html","target":"DATA"}]},{"text":"Views","icon":"images/folder.svg","href":"Servers\\localhost\\Databases\\practica02apoyo\\Views\\Views.html","target":"DATA","nodes":[{"text":"mayores18","icon":"images/view.svg","href":"Servers\\localhost\\Databases\\practica02apoyo\\Views\\mayores18.html","target":"DATA"},{"text":"menores18","icon":"images/view.svg","href":"Servers\\localhost\\Databases\\practica02apoyo\\Views\\menores18.html","target":"DATA"},{"text":"nombresapellidos","icon":"images/view.svg","href":"Servers\\localhost\\Databases\\practica02apoyo\\Views\\nombresapellidos.html","target":"DATA"}]},{"text":"Procedures","icon":"images/folder.svg","href":"Servers\\localhost\\Databases\\practica02apoyo\\Procedures\\Procedures.html","target":"DATA","nodes":[{"text":"borrarTablas","icon":"images/procedure.svg","href":"Servers\\localhost\\Databases\\practica02apoyo\\Procedures\\borrarTablas.html","target":"DATA"},{"text":"ejercicio02","icon":"images/procedure.svg","href":"Servers\\localhost\\Databases\\practica02apoyo\\Procedures\\ejercicio02.html","target":"DATA"},{"text":"ejercicio07","icon":"images/procedure.svg","href":"Servers\\localhost\\Databases\\practica02apoyo\\Procedures\\ejercicio07.html","target":"DATA"},{"text":"ejercicio08","icon":"images/procedure.svg","href":"Servers\\localhost\\Databases\\practica02apoyo\\Procedures\\ejercicio08.html","target":"DATA"},{"text":"ejercicio09","icon":"images/procedure.svg","href":"Servers\\localhost\\Databases\\practica02apoyo\\Procedures\\ejercicio09.html","target":"DATA"},{"text":"ejercicio10_alfabeto","icon":"images/procedure.svg","href":"Servers\\localhost\\Databases\\practica02apoyo\\Procedures\\ejercicio10_alfabeto.html","target":"DATA"},{"text":"ejercicio10_numeros","icon":"images/procedure.svg","href":"Servers\\localhost\\Databases\\practica02apoyo\\Procedures\\ejercicio10_numeros.html","target":"DATA"},{"text":"rellenarMayores18","icon":"images/procedure.svg","href":"Servers\\localhost\\Databases\\practica02apoyo\\Procedures\\rellenarMayores18.html","target":"DATA"},{"text":"rellenarMenores18","icon":"images/procedure.svg","href":"Servers\\localhost\\Databases\\practica02apoyo\\Procedures\\rellenarMenores18.html","target":"DATA"}]},{"text":"Functions","icon":"images/folder.svg","href":"Servers\\localhost\\Databases\\practica02apoyo\\Functions\\Functions.html","target":"DATA","nodes":[{"text":"edad","icon":"images/function.svg","href":"Servers\\localhost\\Databases\\practica02apoyo\\Functions\\edad.html","target":"DATA"}]}]}]}]}];
$('#tree').treeview({levels: 3,data: data,enableLinks: true,injectStyle: false,highlightSelected: true,collapseIcon: 'images/tree-node-expanded.svg',expandIcon: 'images/tree-node-collapsed.svg'});
});
var loadEvent = function () {

  $('#btn-expand-nodes').on('click', function (e) {
    $('#tree').treeview('expandAll', { silent: true });
  });
  $('#btn-collapse-nodes').on('click', function (e) {
    $('#tree').treeview('collapseAll', { levels:3, silent: true });
  });
  
  var searchTimeOut;
  $('#input-search').on('input', function() {
    if(searchTimeOut != null)
      clearTimeout(searchTimeOut);
    searchTimeOut = setTimeout(function(){
      var pattern = $('#input-search').val();
      var tree = $('#tree');
      tree.treeview('collapseAll', { levels:3, silent: true });
      var options = { ignoreCase: true, exactMatch: false, revealResults: true };
      var results = tree.treeview('search', [pattern, options]);
    }, 500);
  });
  
  $('#tree').on('nodeSelected', function(event, data) {
    // navigate to link
    window.open (data.href, 'DATA', false)
  });
  // select first node.
  $('#tree').treeview('selectNode', [0, { silent: false }]);
}

if (window.addEventListener) {
  window.addEventListener('load', loadEvent, false);
}
else if (window.attachEvent) {
  window.attachEvent('onload', loadEvent);
}