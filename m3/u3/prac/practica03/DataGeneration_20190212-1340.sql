﻿--
-- Script was generated by Devart dbForge Studio for MySQL, Version 8.0.40.0
-- Product Home Page: http://www.devart.com/dbforge/mysql/studio
-- Script date 12/02/2019 13:40:46
-- Target server version: 5.5.5-10.1.36-MariaDB
-- Target connection string: User Id=root;Host=localhost;Character Set=utf8
--



SET NAMES 'utf8';
USE practica03apoyo;
--
-- Delete data from the table 'bloquecasas'
--
TRUNCATE TABLE bloquecasas;
--
-- Delete data from the table 'zonaurbana'
--
DELETE FROM zonaurbana;

--
-- Inserting data into table zonaurbana
--
INSERT INTO zonaurbana(nombrezona, categoria) VALUES
(1, 'GLV'),
(2, 'SWA'),
(3, 'KAN'),
(4, 'LIN'),
(5, 'BEL'),
(6, 'SMO'),
(7, 'GER'),
(8, 'KON'),
(9, 'IBO'),
(10, 'SWE');

--
-- Inserting data into table bloquecasas
--
INSERT INTO bloquecasas(Calle, Numero, Npisos, NombreZona) VALUES
('Kaufingerstraße 8', 79, 3, 9),
('Sendlinger Straße 21-29', 191, 3, 4),
('Zillertalstraße 1f', 175, 7, 6),
('Tassiloplatz 7b', 92, 7, 2),
('Gravelottestraße 12', 146, 2, 3),
('Hofenfelsstraße 1', 157, 8, 9),
('Schackstraße 1', 180, 6, 1),
('Auerfeldstraße 8', 63, 6, 10),
('Neustätterstraße 4', 13, 4, 6),
('Ackermannstraße 3', 50, 1, 6),
('Hochmeierstraße 2', 4, 5, 3),
('Residenzstraße 2', 69, 5, 10),
('Belgradstraße 1', 138, 4, 9),
('Lerchenfeldstraße 2', 147, 3, 4),
('Hirtenstraße 2', 25, 5, 4),
('Adi-Maislinger-Straße 1', 98, 3, 5),
('Arberstraße 2', 187, 4, 10),
('Neustätterstraße 2', 212, 5, 6),
('Am Feuerbächl 12-17', 4, 10, 6),
('Lerchenfeldstraße 7f', 20, 6, 6),
('Ridlerstraße 2a', 21, 9, 4),
('Hedwigstraße 5', 103, 8, 9),
('Küchelbäckerstraße 28', 85, 7, 3),
('Neustätterstraße 36', 101, 8, 10),
('Ungererstraße 2', 7, 1, 10),
('Schönbergstraße 78e', 146, 4, 8),
('Adalbert-Stifter-Straße 2', 128, 5, 4),
('Adelheidstraße 4d', 93, 10, 9),
('Adelgundenstraße 8b', 2, 3, 3),
('Piusstraße 2b', 113, 9, 7),
('Trogerstraße 1', 130, 3, 3),
('Sattlerstraße 59', 88, 1, 10),
('Ackerstraße 6', 199, 9, 5),
('Adi-Maislinger-Straße 20a', 172, 6, 6),
('Bayerstraße 9e', 190, 9, 1),
('Denninger Straße 7', 35, 5, 10),
('Radlkoferstraße 12', 35, 3, 5),
('Hedwigstraße 2', 6, 7, 2),
('Tivolistraße 18', 191, 6, 4),
('Saarstraße 2', 102, 4, 3),
('Amiraplatz 2b', 22, 4, 3),
('Ursulastraße 8', 95, 1, 9),
('Senefelderstraße 1', 56, 4, 2),
('Heimeranstraße 5e', 1, 8, 2),
('Geibelstraße 4e', 183, 1, 9),
('Jahnstraße 25-28', 88, 10, 4),
('Leonrodplatz 3f', 44, 5, 2),
('Adi-Maislinger-Straße 1', 158, 7, 6),
('Radlsteg 6', 85, 2, 2),
('Altenhofstraße 22-29', 95, 8, 9),
('Obermaierstraße 2a', 167, 2, 3),
('Schwarzmannstraße 2e', 94, 7, 2),
('Am Neudeck 8a', 41, 6, 1),
('Bahnhofplatz 4e', 157, 9, 3),
('Sankt-Bonifatius-Straße 1', 82, 6, 5),
('Donaustraße 5', 123, 4, 10),
('Rainer-Werner-Fassbinder-Platz 1e', 157, 9, 5),
('Aberlestraße 21-29', 169, 5, 1),
('Gewürzmühlstraße 15', 103, 9, 7),
('Ackerstraße 8', 156, 5, 8),
('Karmeliterstraße 41c', 103, 1, 1),
('Höchlstraße 87', 105, 6, 6),
('Beichstraße 96e', 223, 1, 9),
('Adamstraße 6e', 140, 2, 9),
('Kopernikusstraße 1f', 109, 8, 1),
('Kreuzstraße 2c', 131, 10, 8),
('Barbarastraße 14-18', 4, 6, 5),
('Pixisstraße 1', 9, 3, 9),
('Adi-Maislinger-Straße 6', 63, 1, 7),
('Adelgundenstraße 1', 127, 9, 3),
('Waltherstraße 3', 178, 8, 3),
('Palmstraße 1', 8, 10, 7),
('Aberlestraße 8e', 102, 9, 4),
('Schornstraße 7', 11, 8, 2),
('Flemingstraße 82', 59, 10, 10),
('Roßmarkt 28', 4, 7, 1),
('Sonnenstraße 2c', 97, 9, 1),
('Gyßlingstraße 25-27', 215, 5, 7),
('Regerstraße 2b', 216, 9, 10),
('Ledererstraße 23a', 186, 7, 4),
('Theresienhöhe 73', 171, 2, 2),
('Bonselsstraße 1', 119, 10, 4),
('Adelheidstraße 3', 136, 6, 3),
('Am Nockherberg 6', 192, 5, 9),
('Lily-Braun-Weg 4', 199, 8, 4),
('Lenaustraße 7', 165, 8, 8),
('Hochbrückenstraße 1', 81, 1, 8),
('Zündappbogen 26', 111, 10, 4),
('Merianstraße 2c', 3, 4, 4),
('Adams-Lehmann-Straße 18d', 198, 7, 3),
('Bavariastraße 2', 120, 1, 4),
('Paradiesstraße 1', 147, 5, 4),
('Baldestraße 1', 179, 1, 9),
('Amberger Straße 1', 75, 8, 2),
('Beichstraße 160', 154, 10, 4),
('Schwalbenstraße 2', 19, 8, 6),
('Ackermannstraße 1', 81, 6, 5),
('Ackerstraße 21-28', 51, 1, 3),
('Pirkheimerstraße 120', 90, 10, 5),
('Morassistraße 72', 181, 4, 4);