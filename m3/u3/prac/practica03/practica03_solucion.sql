﻿--
-- ejercicio 02
--

  CALL ejercicio02(2);

--
-- ejercicio 03
--

  CALL ejercicio03('Camping');
  CALL ejercicio03('Lover');
  CALL ejercicio03('Heart');
  CALL ejercicio03('Silent');
  CALL ejercicio03('Pacience');
  CALL ejercicio03('People');

--
-- ejercicio 04
--

  SELECT ejercicio04();

--
-- ejercicio 05
--

  SELECT * FROM casaparticular;
  CALL ejercicio05(3,95);
  SELECT * FROM casaparticular;
  CALL ejercicio05(5,105);
  SELECT * FROM casaparticular;
  CALL ejercicio05(9,105);
  SELECT * FROM casaparticular;

--
-- ejercicio 08
--

  SELECT * FROM casaparticular;
  CALL ejercicio08();
  SELECT * FROM casaparticular;
  CALL ejercicio08();
  SELECT * FROM casaparticular;
  CALL ejercicio08();
  SELECT * FROM casaparticular;

--
-- ejercicio 09 Cursor
--

--
-- ejercicio 11
--

-- ejercicio 12

  CALL ejercicio12('2000-10-10', @dia,@mes,@año);
  SELECT @dia;
  SELECT @mes;
  SELECT @año;

-- ejercicio 13

  CALL ejercicio13('2000-10-10', @edad);
  SELECT @edad;
  SELECT edad('2000-10-10');

-- ejercicio 14

  SELECT ejercicio14('         2000-10-10           ');