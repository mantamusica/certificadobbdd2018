$(document).ready(function () {
var data = [{"text":"Cover Page","icon":"images/folder.svg","href":"startpage.html","target":"DATA"},{"text":"Servers","icon":"images/folder.svg","href":"Servers\\Servers.html","target":"DATA"},{"text":"localhost","icon":"images/folder.svg","href":"Servers\\localhost\\localhost.html","target":"DATA","nodes":[{"text":"Databases","icon":"images/folder.svg","href":"Servers\\localhost\\Databases\\Databases.html","target":"DATA","nodes":[{"text":"practica03apoyo","icon":"images/database.svg","href":"Servers\\localhost\\Databases\\practica03apoyo\\practica03apoyo.html","target":"DATA","nodes":[{"text":"Tables","icon":"images/folder.svg","href":"Servers\\localhost\\Databases\\practica03apoyo\\Tables\\Tables.html","target":"DATA","nodes":[{"text":"bloquecasas","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica03apoyo\\Tables\\bloquecasas.html","target":"DATA"},{"text":"casaparticular","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica03apoyo\\Tables\\casaparticular.html","target":"DATA"},{"text":"persona","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica03apoyo\\Tables\\persona.html","target":"DATA"},{"text":"piso","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica03apoyo\\Tables\\piso.html","target":"DATA"},{"text":"poseec","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica03apoyo\\Tables\\poseec.html","target":"DATA"},{"text":"poseep","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica03apoyo\\Tables\\poseep.html","target":"DATA"},{"text":"zonaurbana","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica03apoyo\\Tables\\zonaurbana.html","target":"DATA"}]},{"text":"Views","icon":"images/folder.svg","href":"Servers\\localhost\\Databases\\practica03apoyo\\Views\\Views.html","target":"DATA","nodes":[{"text":"ejercicio06","icon":"images/view.svg","href":"Servers\\localhost\\Databases\\practica03apoyo\\Views\\ejercicio06.html","target":"DATA"}]},{"text":"Procedures","icon":"images/folder.svg","href":"Servers\\localhost\\Databases\\practica03apoyo\\Procedures\\Procedures.html","target":"DATA","nodes":[{"text":"ejercicio02","icon":"images/procedure.svg","href":"Servers\\localhost\\Databases\\practica03apoyo\\Procedures\\ejercicio02.html","target":"DATA"},{"text":"ejercicio03","icon":"images/procedure.svg","href":"Servers\\localhost\\Databases\\practica03apoyo\\Procedures\\ejercicio03.html","target":"DATA"},{"text":"ejercicio05","icon":"images/procedure.svg","href":"Servers\\localhost\\Databases\\practica03apoyo\\Procedures\\ejercicio05.html","target":"DATA"},{"text":"ejercicio08","icon":"images/procedure.svg","href":"Servers\\localhost\\Databases\\practica03apoyo\\Procedures\\ejercicio08.html","target":"DATA"},{"text":"ejercicio11","icon":"images/procedure.svg","href":"Servers\\localhost\\Databases\\practica03apoyo\\Procedures\\ejercicio11.html","target":"DATA"},{"text":"ejercicio12","icon":"images/procedure.svg","href":"Servers\\localhost\\Databases\\practica03apoyo\\Procedures\\ejercicio12.html","target":"DATA"},{"text":"ejercicio13","icon":"images/procedure.svg","href":"Servers\\localhost\\Databases\\practica03apoyo\\Procedures\\ejercicio13.html","target":"DATA"}]},{"text":"Functions","icon":"images/folder.svg","href":"Servers\\localhost\\Databases\\practica03apoyo\\Functions\\Functions.html","target":"DATA","nodes":[{"text":"edad","icon":"images/function.svg","href":"Servers\\localhost\\Databases\\practica03apoyo\\Functions\\edad.html","target":"DATA"},{"text":"ejercicio04","icon":"images/function.svg","href":"Servers\\localhost\\Databases\\practica03apoyo\\Functions\\ejercicio04.html","target":"DATA"},{"text":"ejercicio14","icon":"images/function.svg","href":"Servers\\localhost\\Databases\\practica03apoyo\\Functions\\ejercicio14.html","target":"DATA"},{"text":"valor_valido","icon":"images/function.svg","href":"Servers\\localhost\\Databases\\practica03apoyo\\Functions\\valor_valido.html","target":"DATA"}]}]}]}]}];
$('#tree').treeview({levels: 3,data: data,enableLinks: true,injectStyle: false,highlightSelected: true,collapseIcon: 'images/tree-node-expanded.svg',expandIcon: 'images/tree-node-collapsed.svg'});
});
var loadEvent = function () {

  $('#btn-expand-nodes').on('click', function (e) {
    $('#tree').treeview('expandAll', { silent: true });
  });
  $('#btn-collapse-nodes').on('click', function (e) {
    $('#tree').treeview('collapseAll', { levels:3, silent: true });
  });
  
  var searchTimeOut;
  $('#input-search').on('input', function() {
    if(searchTimeOut != null)
      clearTimeout(searchTimeOut);
    searchTimeOut = setTimeout(function(){
      var pattern = $('#input-search').val();
      var tree = $('#tree');
      tree.treeview('collapseAll', { levels:3, silent: true });
      var options = { ignoreCase: true, exactMatch: false, revealResults: true };
      var results = tree.treeview('search', [pattern, options]);
    }, 500);
  });
  
  $('#tree').on('nodeSelected', function(event, data) {
    // navigate to link
    window.open (data.href, 'DATA', false)
  });
  // select first node.
  $('#tree').treeview('selectNode', [0, { silent: false }]);
}

if (window.addEventListener) {
  window.addEventListener('load', loadEvent, false);
}
else if (window.attachEvent) {
  window.attachEvent('onload', loadEvent);
}