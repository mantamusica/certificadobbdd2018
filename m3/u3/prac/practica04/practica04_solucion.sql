﻿-- ejercicio 01

  CALL ejercicio01('La ca0de3na de palabras con números como el 0, el 1.');

-- ejercicio 02

  SELECT ejercicio02('la cadena de palabras.');

-- ejercicio 03

  SELECT ejercicio03(now(),'1978-08-07');

-- ejercicio 04

  SELECT ejercicio04(now(),'1978-08-07');

-- ejercicio 05

  CALL ejercicio05();

-- ejercicio 06

  SELECT ejercicio06(5,1.5,6,5.9,12);

-- ejercicio 07

  CALL ejercicio07();

-- ejercicio 08

  CALL ejercicio08();

-- ejercicio 09

  CALL ejercicio09();

-- ejercicio 10

  CALL ejercicio10(5);

-- ejercicio 11

  CALL ejercicio11(0);

