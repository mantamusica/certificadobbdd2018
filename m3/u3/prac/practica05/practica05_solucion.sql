﻿-- ejercicio 01

  CALL ejercicio01();

-- ejercicio 02

  CALL ejercicio02();

-- ejercicio 03 y 04

  SELECT ejercicio03(1,'Pepe');
  SELECT ejercicio03(2,'Manuel');
  SELECT ejercicio03(3,'Carlos');
  SELECT ejercicio03(1,'Ana');

  CALL ejercicio03();

-- ejercicio 05

  ALTER TABLE datos ADD fecha date;

-- ejercicio 06

  SELECT ejercicio06(NOW());
  CALL ejercicio06();

-- ejercicio 07

  CALL ejercicio07();
  SELECT ejercicio07();

-- ejercicio 09

  CALL ejercicio09();

-- ejercicio 10

  SELECT ejercicio10();

