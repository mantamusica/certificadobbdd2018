﻿USE practica07apoyo;

ALTER DATABASE CHARACTER SET utf8 COLLATE utf8_general_ci;

-- ejercicio 01

  CALL ejercicio01();

-- ejercicio 02

  CALL ejercicio02();

  -- ahora el trigger y añadimos un check.


  ALTER TABLE notas
ADD CHECK (notas.Ejercicio1 BETWEEN 0 AND 10 );


-- ejercicio 10

  ALTER TABLE alumnos CHARACTER SET utf8 COLLATE utf8_general_ci;
  ALTER TABLE notas CHARACTER SET utf8 COLLATE utf8_general_ci;
  ALTER TABLE tablas CHARACTER SET utf8 COLLATE utf8_general_ci;

  CALL ejercicio10_modalidad();
  CALL ejercicio10_resultado();
  CALL ejercicio10();

  SELECT * FROM alumnos a LIMIT 3;


