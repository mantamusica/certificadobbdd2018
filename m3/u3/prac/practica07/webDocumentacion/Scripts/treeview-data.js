$(document).ready(function () {
var data = [{"text":"Cover Page","icon":"images/folder.svg","href":"startpage.html","target":"DATA"},{"text":"Servers","icon":"images/folder.svg","href":"Servers\\Servers.html","target":"DATA"},{"text":"localhost","icon":"images/folder.svg","href":"Servers\\localhost\\localhost.html","target":"DATA","nodes":[{"text":"Databases","icon":"images/folder.svg","href":"Servers\\localhost\\Databases\\Databases.html","target":"DATA","nodes":[{"text":"practica07apoyo","icon":"images/database.svg","href":"Servers\\localhost\\Databases\\practica07apoyo\\practica07apoyo.html","target":"DATA","nodes":[{"text":"Tables","icon":"images/folder.svg","href":"Servers\\localhost\\Databases\\practica07apoyo\\Tables\\Tables.html","target":"DATA","nodes":[{"text":"alumnos","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica07apoyo\\Tables\\alumnos.html","target":"DATA"},{"text":"ejemplo","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica07apoyo\\Tables\\ejemplo.html","target":"DATA"},{"text":"notas","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica07apoyo\\Tables\\notas.html","target":"DATA"},{"text":"tablas","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica07apoyo\\Tables\\tablas.html","target":"DATA"}]},{"text":"Procedures","icon":"images/folder.svg","href":"Servers\\localhost\\Databases\\practica07apoyo\\Procedures\\Procedures.html","target":"DATA","nodes":[{"text":"ejercicio01","icon":"images/procedure.svg","href":"Servers\\localhost\\Databases\\practica07apoyo\\Procedures\\ejercicio01.html","target":"DATA"},{"text":"ejercicio02","icon":"images/procedure.svg","href":"Servers\\localhost\\Databases\\practica07apoyo\\Procedures\\ejercicio02.html","target":"DATA"},{"text":"ejercicio10","icon":"images/procedure.svg","href":"Servers\\localhost\\Databases\\practica07apoyo\\Procedures\\ejercicio10.html","target":"DATA"},{"text":"ejercicio10_modalidad","icon":"images/procedure.svg","href":"Servers\\localhost\\Databases\\practica07apoyo\\Procedures\\ejercicio10_modalidad.html","target":"DATA"},{"text":"ejercicio10_resultado","icon":"images/procedure.svg","href":"Servers\\localhost\\Databases\\practica07apoyo\\Procedures\\ejercicio10_resultado.html","target":"DATA"}]},{"text":"Functions","icon":"images/folder.svg","href":"Servers\\localhost\\Databases\\practica07apoyo\\Functions\\Functions.html","target":"DATA","nodes":[{"text":"ejercicio10_11","icon":"images/function.svg","href":"Servers\\localhost\\Databases\\practica07apoyo\\Functions\\ejercicio10_11.html","target":"DATA"}]},{"text":"Triggers","icon":"images/folder.svg","href":"Servers\\localhost\\Databases\\practica07apoyo\\Triggers\\Triggers.html","target":"DATA","nodes":[{"text":"disparador1","icon":"images/trigger.svg","href":"Servers\\localhost\\Databases\\practica07apoyo\\Triggers\\disparador1.html","target":"DATA"},{"text":"ejercicio08_ insert","icon":"images/trigger.svg","href":"Servers\\localhost\\Databases\\practica07apoyo\\Triggers\\ejercicio08__insert.html","target":"DATA"},{"text":"ejercicio08_update","icon":"images/trigger.svg","href":"Servers\\localhost\\Databases\\practica07apoyo\\Triggers\\ejercicio08_update.html","target":"DATA"},{"text":"ejercicio11","icon":"images/trigger.svg","href":"Servers\\localhost\\Databases\\practica07apoyo\\Triggers\\ejercicio11.html","target":"DATA"}]}]}]}]}];
$('#tree').treeview({levels: 3,data: data,enableLinks: true,injectStyle: false,highlightSelected: true,collapseIcon: 'images/tree-node-expanded.svg',expandIcon: 'images/tree-node-collapsed.svg'});
});
var loadEvent = function () {

  $('#btn-expand-nodes').on('click', function (e) {
    $('#tree').treeview('expandAll', { silent: true });
  });
  $('#btn-collapse-nodes').on('click', function (e) {
    $('#tree').treeview('collapseAll', { levels:3, silent: true });
  });
  
  var searchTimeOut;
  $('#input-search').on('input', function() {
    if(searchTimeOut != null)
      clearTimeout(searchTimeOut);
    searchTimeOut = setTimeout(function(){
      var pattern = $('#input-search').val();
      var tree = $('#tree');
      tree.treeview('collapseAll', { levels:3, silent: true });
      var options = { ignoreCase: true, exactMatch: false, revealResults: true };
      var results = tree.treeview('search', [pattern, options]);
    }, 500);
  });
  
  $('#tree').on('nodeSelected', function(event, data) {
    // navigate to link
    window.open (data.href, 'DATA', false)
  });
  // select first node.
  $('#tree').treeview('selectNode', [0, { silent: false }]);
}

if (window.addEventListener) {
  window.addEventListener('load', loadEvent, false);
}
else if (window.attachEvent) {
  window.attachEvent('onload', loadEvent);
}