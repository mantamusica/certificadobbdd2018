﻿-- ejercicio 01

  SELECT * FROM ejercicio01;

-- ejercicio 02

  SELECT ejercicio02(9);

-- ejercicio 03

  SELECT ejercicio03();

-- ejercicio 04

  CALL ejercicio04();

-- ejercicio 05

  ALTER TABLE practica08apoyo.clientes
  ADD CONSTRAINT FK_clientes_departamento_id FOREIGN KEY (departamento_id)
  REFERENCES practica08apoyo.departamentos (DEPARTAMENTO_ID) ON DELETE CASCADE ON UPDATE CASCADE;

-- ejercicio 07

  SELECT * FROM ejercicio07;

-- ejercicio 08

  ALTER TABLE practica08apoyo.clientes
  DROP FOREIGN KEY FK_clientes_departamento_id;

-- error
  ALTER TABLE clientes 
  ADD CONSTRAINT FK_clientes_municipios_municipio_id FOREIGN KEY (municipio_id)
    REFERENCES municipios(municipio_id) ON DELETE CASCADE ON UPDATE CASCADE;

-- ejercicio 09

  SELECT * FROM ejercicio09 e;

-- ejercicio 10
  
  -- desactivamos claves foráneas para poder hacer este update
  SET FOREIGN_KEY_CHECKS=0;
  UPDATE ejercicio09 e SET e.municipio_id = 27001;
  -- volvemos a activarlas
  SET FOREIGN_KEY_CHECKS=1;

-- ejercicio 11

  SELECT * FROM ejercicio09 e;

-- ejercicio 12

  ALTER TABLE clientes 
  ADD CONSTRAINT FK_clientes_municipios_municipio_id FOREIGN KEY (municipio_id)
    REFERENCES municipios(municipio_id) ON DELETE CASCADE ON UPDATE CASCADE;

-- ejercicio 13

  SET FOREIGN_KEY_CHECKS=0;

-- ejercicio 14

  SELECT contarRegistrosNoValidosMunicipios();
  SELECT contarRegistrosNoValidosClientes();

