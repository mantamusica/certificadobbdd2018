$(document).ready(function () {
var data = [{"text":"Cover Page","icon":"images/folder.svg","href":"startpage.html","target":"DATA"},{"text":"Servers","icon":"images/folder.svg","href":"Servers\\Servers.html","target":"DATA"},{"text":"localhost","icon":"images/folder.svg","href":"Servers\\localhost\\localhost.html","target":"DATA","nodes":[{"text":"Databases","icon":"images/folder.svg","href":"Servers\\localhost\\Databases\\Databases.html","target":"DATA","nodes":[{"text":"practica08apoyo","icon":"images/database.svg","href":"Servers\\localhost\\Databases\\practica08apoyo\\practica08apoyo.html","target":"DATA","nodes":[{"text":"Tables","icon":"images/folder.svg","href":"Servers\\localhost\\Databases\\practica08apoyo\\Tables\\Tables.html","target":"DATA","nodes":[{"text":"categorias","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica08apoyo\\Tables\\categorias.html","target":"DATA"},{"text":"clientes","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica08apoyo\\Tables\\clientes.html","target":"DATA"},{"text":"departamentos","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica08apoyo\\Tables\\departamentos.html","target":"DATA"},{"text":"municipios","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica08apoyo\\Tables\\municipios.html","target":"DATA"},{"text":"pedidos","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica08apoyo\\Tables\\pedidos.html","target":"DATA"},{"text":"pedidos_detalle","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica08apoyo\\Tables\\pedidos_detalle.html","target":"DATA"},{"text":"productos","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica08apoyo\\Tables\\productos.html","target":"DATA"},{"text":"roles","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica08apoyo\\Tables\\roles.html","target":"DATA"},{"text":"roles_usuarios","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica08apoyo\\Tables\\roles_usuarios.html","target":"DATA"},{"text":"usuarios","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica08apoyo\\Tables\\usuarios.html","target":"DATA"}]},{"text":"Views","icon":"images/folder.svg","href":"Servers\\localhost\\Databases\\practica08apoyo\\Views\\Views.html","target":"DATA","nodes":[{"text":"ejercicio01","icon":"images/view.svg","href":"Servers\\localhost\\Databases\\practica08apoyo\\Views\\ejercicio01.html","target":"DATA"},{"text":"ejercicio07","icon":"images/view.svg","href":"Servers\\localhost\\Databases\\practica08apoyo\\Views\\ejercicio07.html","target":"DATA"},{"text":"ejercicio07_c1","icon":"images/view.svg","href":"Servers\\localhost\\Databases\\practica08apoyo\\Views\\ejercicio07_c1.html","target":"DATA"},{"text":"ejercicio07_c2","icon":"images/view.svg","href":"Servers\\localhost\\Databases\\practica08apoyo\\Views\\ejercicio07_c2.html","target":"DATA"},{"text":"ejercicio09","icon":"images/view.svg","href":"Servers\\localhost\\Databases\\practica08apoyo\\Views\\ejercicio09.html","target":"DATA"}]},{"text":"Procedures","icon":"images/folder.svg","href":"Servers\\localhost\\Databases\\practica08apoyo\\Procedures\\Procedures.html","target":"DATA","nodes":[{"text":"ejercicio04","icon":"images/procedure.svg","href":"Servers\\localhost\\Databases\\practica08apoyo\\Procedures\\ejercicio04.html","target":"DATA"}]},{"text":"Functions","icon":"images/folder.svg","href":"Servers\\localhost\\Databases\\practica08apoyo\\Functions\\Functions.html","target":"DATA","nodes":[{"text":"contarRegistrosNoValidosClientes","icon":"images/function.svg","href":"Servers\\localhost\\Databases\\practica08apoyo\\Functions\\contarRegistrosNoValidosClientes.html","target":"DATA"},{"text":"contarRegistrosNoValidosMunicipios","icon":"images/function.svg","href":"Servers\\localhost\\Databases\\practica08apoyo\\Functions\\contarRegistrosNoValidosMunicipios.html","target":"DATA"},{"text":"ejercicio02","icon":"images/function.svg","href":"Servers\\localhost\\Databases\\practica08apoyo\\Functions\\ejercicio02.html","target":"DATA"},{"text":"ejercicio03","icon":"images/function.svg","href":"Servers\\localhost\\Databases\\practica08apoyo\\Functions\\ejercicio03.html","target":"DATA"}]},{"text":"Triggers","icon":"images/folder.svg","href":"Servers\\localhost\\Databases\\practica08apoyo\\Triggers\\Triggers.html","target":"DATA","nodes":[{"text":"ejercicio14_insert","icon":"images/trigger.svg","href":"Servers\\localhost\\Databases\\practica08apoyo\\Triggers\\ejercicio14_insert.html","target":"DATA"},{"text":"ejercicio14_update","icon":"images/trigger.svg","href":"Servers\\localhost\\Databases\\practica08apoyo\\Triggers\\ejercicio14_update.html","target":"DATA"},{"text":"ejercicio15_insert","icon":"images/trigger.svg","href":"Servers\\localhost\\Databases\\practica08apoyo\\Triggers\\ejercicio15_insert.html","target":"DATA"},{"text":"ejercicio15_update","icon":"images/trigger.svg","href":"Servers\\localhost\\Databases\\practica08apoyo\\Triggers\\ejercicio15_update.html","target":"DATA"}]}]}]}]}];
$('#tree').treeview({levels: 3,data: data,enableLinks: true,injectStyle: false,highlightSelected: true,collapseIcon: 'images/tree-node-expanded.svg',expandIcon: 'images/tree-node-collapsed.svg'});
});
var loadEvent = function () {

  $('#btn-expand-nodes').on('click', function (e) {
    $('#tree').treeview('expandAll', { silent: true });
  });
  $('#btn-collapse-nodes').on('click', function (e) {
    $('#tree').treeview('collapseAll', { levels:3, silent: true });
  });
  
  var searchTimeOut;
  $('#input-search').on('input', function() {
    if(searchTimeOut != null)
      clearTimeout(searchTimeOut);
    searchTimeOut = setTimeout(function(){
      var pattern = $('#input-search').val();
      var tree = $('#tree');
      tree.treeview('collapseAll', { levels:3, silent: true });
      var options = { ignoreCase: true, exactMatch: false, revealResults: true };
      var results = tree.treeview('search', [pattern, options]);
    }, 500);
  });
  
  $('#tree').on('nodeSelected', function(event, data) {
    // navigate to link
    window.open (data.href, 'DATA', false)
  });
  // select first node.
  $('#tree').treeview('selectNode', [0, { silent: false }]);
}

if (window.addEventListener) {
  window.addEventListener('load', loadEvent, false);
}
else if (window.attachEvent) {
  window.attachEvent('onload', loadEvent);
}