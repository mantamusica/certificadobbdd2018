$(document).ready(function () {
var data = [{"text":"Cover Page","icon":"images/folder.svg","href":"startpage.html","target":"DATA"},{"text":"Servers","icon":"images/folder.svg","href":"Servers\\Servers.html","target":"DATA"},{"text":"localhost","icon":"images/folder.svg","href":"Servers\\localhost\\localhost.html","target":"DATA","nodes":[{"text":"Databases","icon":"images/folder.svg","href":"Servers\\localhost\\Databases\\Databases.html","target":"DATA","nodes":[{"text":"practica09apoyo","icon":"images/database.svg","href":"Servers\\localhost\\Databases\\practica09apoyo\\practica09apoyo.html","target":"DATA","nodes":[{"text":"Tables","icon":"images/folder.svg","href":"Servers\\localhost\\Databases\\practica09apoyo\\Tables\\Tables.html","target":"DATA","nodes":[{"text":"alumno","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica09apoyo\\Tables\\alumno.html","target":"DATA"},{"text":"daclase","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica09apoyo\\Tables\\daclase.html","target":"DATA"},{"text":"profesor","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica09apoyo\\Tables\\profesor.html","target":"DATA"}]},{"text":"Views","icon":"images/folder.svg","href":"Servers\\localhost\\Databases\\practica09apoyo\\Views\\Views.html","target":"DATA","nodes":[{"text":"ejercicio08","icon":"images/view.svg","href":"Servers\\localhost\\Databases\\practica09apoyo\\Views\\ejercicio08.html","target":"DATA"},{"text":"ejercicio20","icon":"images/view.svg","href":"Servers\\localhost\\Databases\\practica09apoyo\\Views\\ejercicio20.html","target":"DATA"}]},{"text":"Procedures","icon":"images/folder.svg","href":"Servers\\localhost\\Databases\\practica09apoyo\\Procedures\\Procedures.html","target":"DATA","nodes":[{"text":"ejercicio04","icon":"images/procedure.svg","href":"Servers\\localhost\\Databases\\practica09apoyo\\Procedures\\ejercicio04.html","target":"DATA"},{"text":"ejercicio06","icon":"images/procedure.svg","href":"Servers\\localhost\\Databases\\practica09apoyo\\Procedures\\ejercicio06.html","target":"DATA"},{"text":"ejercicio11","icon":"images/procedure.svg","href":"Servers\\localhost\\Databases\\practica09apoyo\\Procedures\\ejercicio11.html","target":"DATA"}]},{"text":"Functions","icon":"images/folder.svg","href":"Servers\\localhost\\Databases\\practica09apoyo\\Functions\\Functions.html","target":"DATA","nodes":[{"text":"contarClasesProfesores","icon":"images/function.svg","href":"Servers\\localhost\\Databases\\practica09apoyo\\Functions\\contarClasesProfesores.html","target":"DATA"},{"text":"ejercicio05","icon":"images/function.svg","href":"Servers\\localhost\\Databases\\practica09apoyo\\Functions\\ejercicio05.html","target":"DATA"},{"text":"ejercicio10","icon":"images/function.svg","href":"Servers\\localhost\\Databases\\practica09apoyo\\Functions\\ejercicio10.html","target":"DATA"}]},{"text":"Triggers","icon":"images/folder.svg","href":"Servers\\localhost\\Databases\\practica09apoyo\\Triggers\\Triggers.html","target":"DATA","nodes":[{"text":"ejercicio13","icon":"images/trigger.svg","href":"Servers\\localhost\\Databases\\practica09apoyo\\Triggers\\ejercicio13.html","target":"DATA"},{"text":"ejercicio17","icon":"images/trigger.svg","href":"Servers\\localhost\\Databases\\practica09apoyo\\Triggers\\ejercicio17.html","target":"DATA"},{"text":"ejercicio21","icon":"images/trigger.svg","href":"Servers\\localhost\\Databases\\practica09apoyo\\Triggers\\ejercicio21.html","target":"DATA"},{"text":"ejercicio22","icon":"images/trigger.svg","href":"Servers\\localhost\\Databases\\practica09apoyo\\Triggers\\ejercicio22.html","target":"DATA"},{"text":"ejercicio26","icon":"images/trigger.svg","href":"Servers\\localhost\\Databases\\practica09apoyo\\Triggers\\ejercicio26.html","target":"DATA"}]}]}]}]}];
$('#tree').treeview({levels: 3,data: data,enableLinks: true,injectStyle: false,highlightSelected: true,collapseIcon: 'images/tree-node-expanded.svg',expandIcon: 'images/tree-node-collapsed.svg'});
});
var loadEvent = function () {

  $('#btn-expand-nodes').on('click', function (e) {
    $('#tree').treeview('expandAll', { silent: true });
  });
  $('#btn-collapse-nodes').on('click', function (e) {
    $('#tree').treeview('collapseAll', { levels:3, silent: true });
  });
  
  var searchTimeOut;
  $('#input-search').on('input', function() {
    if(searchTimeOut != null)
      clearTimeout(searchTimeOut);
    searchTimeOut = setTimeout(function(){
      var pattern = $('#input-search').val();
      var tree = $('#tree');
      tree.treeview('collapseAll', { levels:3, silent: true });
      var options = { ignoreCase: true, exactMatch: false, revealResults: true };
      var results = tree.treeview('search', [pattern, options]);
    }, 500);
  });
  
  $('#tree').on('nodeSelected', function(event, data) {
    // navigate to link
    window.open (data.href, 'DATA', false)
  });
  // select first node.
  $('#tree').treeview('selectNode', [0, { silent: false }]);
}

if (window.addEventListener) {
  window.addEventListener('load', loadEvent, false);
}
else if (window.attachEvent) {
  window.attachEvent('onload', loadEvent);
}