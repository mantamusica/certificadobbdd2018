﻿--
-- Script was generated by Devart dbForge Studio for MySQL, Version 8.0.40.0
-- Product home page: http://www.devart.com/dbforge/mysql/studio
-- Script date 21/02/2019 12:03:10
-- Server version: 5.5.5-10.1.36-MariaDB
-- Client version: 4.1
--

-- 
-- Disable foreign keys
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Set SQL mode
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Set character set the client will use to send SQL statements to the server
--
SET NAMES 'utf8';

--
-- Set default database
--
USE practica09apoyo;

--
-- Drop function `contarClasesProfesores`
--
DROP FUNCTION IF EXISTS contarClasesProfesores;

--
-- Drop table `daclase`
--
DROP TABLE IF EXISTS daclase;

--
-- Drop view `ejercicio20`
--
DROP VIEW IF EXISTS ejercicio20 CASCADE;

--
-- Drop table `profesor`
--
DROP TABLE IF EXISTS profesor;

--
-- Drop view `ejercicio08`
--
DROP VIEW IF EXISTS ejercicio08 CASCADE;

--
-- Drop procedure `ejercicio04`
--
DROP PROCEDURE IF EXISTS ejercicio04;

--
-- Drop procedure `ejercicio06`
--
DROP PROCEDURE IF EXISTS ejercicio06;

--
-- Drop procedure `ejercicio11`
--
DROP PROCEDURE IF EXISTS ejercicio11;

--
-- Drop table `alumno`
--
DROP TABLE IF EXISTS alumno;

--
-- Drop function `ejercicio05`
--
DROP FUNCTION IF EXISTS ejercicio05;

--
-- Drop function `ejercicio10`
--
DROP FUNCTION IF EXISTS ejercicio10;

--
-- Set default database
--
USE practica09apoyo;

DELIMITER $$

--
-- Create function `ejercicio10`
--
CREATE DEFINER = 'root'@'localhost'
FUNCTION ejercicio10 (fechaNacimiento date)
RETURNS int(11)
COMMENT 'Funcion para calcular la edad'
BEGIN
  DECLARE edad int DEFAULT 0;
  SET edad = TIMESTAMPDIFF(year, fechaNacimiento, NOW());
  RETURN edad;
END
$$

--
-- Create function `ejercicio05`
--
CREATE DEFINER = 'root'@'localhost'
FUNCTION ejercicio05 (nombre varchar(255), apellidos varchar(255))
RETURNS varchar(5) CHARSET utf8
COMMENT 'Funcion que recoge dos string y devuelve otro editado'
BEGIN
  DECLARE name varchar(255) DEFAULT "";
  DECLARE surname varchar(255) DEFAULT "";
  DECLARE iniciales varchar(255) DEFAULT "";

  SET name = LEFT(nombre, 1);
  SET surname = LEFT(apellidos, 1);
  SET iniciales = CONCAT(name, '. ', surname, '.');

  RETURN iniciales;
END
$$

DELIMITER ;

--
-- Create table `alumno`
--
CREATE TABLE alumno (
  id int(11) NOT NULL AUTO_INCREMENT,
  nombre varchar(255) DEFAULT NULL,
  apellidos varchar(255) DEFAULT NULL,
  iniciales varchar(5) DEFAULT NULL,
  fechaNacimiento date DEFAULT NULL,
  añoNacimiento int(11) DEFAULT NULL,
  mesNacimiento int(11) DEFAULT NULL,
  diaNacimiento int(11) DEFAULT NULL,
  Edad int(3) DEFAULT 0,
  numProfesores int(3) DEFAULT 0,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 26,
AVG_ROW_LENGTH = 744,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

DELIMITER $$

--
-- Create procedure `ejercicio11`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE ejercicio11 ()
COMMENT 'Procedimiento para rellenar las edades de la tabla alumno con la función de edad creada anteriormente'
BEGIN
  DECLARE v_iniciales varchar(5) DEFAULT '';

  DECLARE contador integer DEFAULT 0;
  DECLARE numRegistros integer DEFAULT 0;

  SET numRegistros = (SELECT
      COUNT(*)
    FROM alumno);

  REPEAT
    SET contador = contador + 1;
    UPDATE alumno a
    SET a.edad = ejercicio10(a.fechaNacimiento)
    WHERE contador = a.id;
  UNTIL contador = numRegistros
  END REPEAT;
END
$$

--
-- Create procedure `ejercicio06`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE ejercicio06 ()
COMMENT 'Procedimiento que coloque las iniciales en su sitio utilizando la función antes creada'
BEGIN

  DECLARE v_iniciales varchar(5) DEFAULT '';

  DECLARE contador integer DEFAULT 0;
  DECLARE numRegistros integer DEFAULT 0;

  SET numRegistros = (SELECT
      COUNT(*)
    FROM alumno);

  REPEAT
    SET contador = contador + 1;
    UPDATE alumno a
    SET a.iniciales = ejercicio05(a.nombre, a.apellidos)
    WHERE contador = a.id;
  UNTIL contador = numRegistros
  END REPEAT;

END
$$

--
-- Create procedure `ejercicio04`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE ejercicio04 ()
COMMENT 'Procedimiento para introducir el año, el mes y el dia '
BEGIN

  DECLARE v_fecha date;

  DECLARE contador integer DEFAULT 0;
  DECLARE numRegistros integer DEFAULT 0;
  DECLARE fecha_cursor CURSOR FOR
  SELECT
    fechaNacimiento
  FROM alumno;

  SET numRegistros = (SELECT
      COUNT(*)
    FROM alumno);

  OPEN fecha_cursor;
  REPEAT
    SET contador = contador + 1;
    FETCH fecha_cursor INTO v_fecha;
    UPDATE alumno a
    SET a.añoNacimiento = YEAR(v_fecha),
        a.mesNacimiento = MONTH(v_fecha),
        diaNacimiento = DAY(v_fecha)
    WHERE contador = a.id;
  UNTIL contador = numRegistros
  END REPEAT;

  CLOSE fecha_cursor;

END
$$

DELIMITER ;

--
-- Create view `ejercicio08`
--
CREATE
DEFINER = 'root'@'localhost'
VIEW ejercicio08
AS
SELECT
  `alumno`.`id` AS `id`,
  `alumno`.`nombre` AS `nombre`,
  `alumno`.`apellidos` AS `apellidos`,
  `alumno`.`iniciales` AS `iniciales`,
  `alumno`.`fechaNacimiento` AS `fechaNacimiento`,
  `alumno`.`añoNacimiento` AS `añoNacimiento`,
  `alumno`.`mesNacimiento` AS `mesNacimiento`,
  `alumno`.`diaNacimiento` AS `diaNacimiento`,
  `alumno`.`Edad` AS `Edad`
FROM `alumno`
WHERE (`alumno`.`fechaNacimiento` < '2000-01-01')
WITH CASCADED CHECK OPTION;

--
-- Create table `profesor`
--
CREATE TABLE profesor (
  id int(11) NOT NULL AUTO_INCREMENT,
  nombre varchar(255) DEFAULT NULL,
  apellidos varchar(255) DEFAULT NULL,
  edad int(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 21,
AVG_ROW_LENGTH = 1365,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create view `ejercicio20`
--
CREATE
DEFINER = 'root'@'localhost'
VIEW ejercicio20
AS
SELECT
  `p`.`id` AS `id`,
  `p`.`nombre` AS `nombre`,
  `p`.`apellidos` AS `apellidos`,
  `p`.`edad` AS `edad`
FROM `profesor` `p`
WHERE (`p`.`edad` >= 18)
WITH CASCADED CHECK OPTION;

--
-- Create table `daclase`
--
CREATE TABLE daclase (
  profesor int(11) DEFAULT NULL,
  alumno int(11) DEFAULT NULL,
  fecha date DEFAULT NULL,
  horaInicio time DEFAULT NULL,
  horaFin time DEFAULT NULL
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 4096,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create foreign key
--
ALTER TABLE daclase
ADD CONSTRAINT FK_daclase_alumno FOREIGN KEY (alumno)
REFERENCES alumno (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Create foreign key
--
ALTER TABLE daclase
ADD CONSTRAINT FK_daclase_profesor FOREIGN KEY (profesor)
REFERENCES profesor (id) ON DELETE CASCADE ON UPDATE CASCADE;

DELIMITER $$

--
-- Create function `contarClasesProfesores`
--
CREATE DEFINER = 'root'@'localhost'
FUNCTION contarClasesProfesores (profesor varchar(255))
RETURNS int(11)
COMMENT 'Función para contar número de clases que dan los profesores'
BEGIN
  DECLARE numero int DEFAULT 0;
  SET numero = (SELECT
      COUNT(*)
    FROM daclase d
      INNER JOIN profesor p
        ON d.profesor = p.id
    GROUP BY p.nombre
    HAVING p.nombre = profesor);

  RETURN numero;
END
$$

DELIMITER ;

-- 
-- Dumping data for table profesor
--
INSERT INTO profesor VALUES
(1, 'Zereena', 'chneider', NULL),
(2, 'Syra', 'Dannenberg', NULL),
(3, 'Evaine', 'Totleben', NULL),
(4, 'Panja', 'Baade', NULL),
(5, 'Destina', 'Hofer', NULL),
(6, 'Gunthilde', 'otschield', NULL),
(7, 'Zeruja', 'chneidermann', NULL),
(8, 'Brijana', 'Gering', NULL),
(9, 'Evamaria', 'Kretschmer', NULL),
(14, 'ramon', 'abramo', 45),
(15, 'andres', 'lopez', 47),
(17, 'almudena', 'garcia', 47),
(18, 'manuel', 'palomo', 18),
(19, 'almudena', 'garcia', 50),
(20, 'carlos', 'manrique', 18);

-- 
-- Dumping data for table alumno
--
INSERT INTO alumno VALUES
(1, 'Bertrada', 'Zurbriggen', 'B. Z.', '1990-03-09', 1990, 3, 9, 28, 0),
(2, 'Wolfdietrich', 'Falkenhausen', 'W. F.', '1991-08-31', 1991, 8, 31, 27, 0),
(3, 'Helvin', 'Spangenberg', 'H. S.', '1996-01-08', 1996, 1, 8, 23, 0),
(4, 'Fridurich', 'Laufer', 'F. L.', '1993-05-13', 1993, 5, 13, 25, 0),
(5, 'Uodalrich', 'ünzenberg', 'U. ü.', '1989-10-13', 1989, 10, 13, 29, 0),
(6, 'Wastl', 'Zabel', 'W. Z.', '1996-10-28', 1996, 10, 28, 22, 0),
(7, 'Demian', 'Spiegel', 'D. S.', '1997-11-22', 1997, 11, 22, 21, 0),
(8, 'Kunigunde', 'Blume', 'K. B.', '1998-12-01', 1998, 12, 1, 20, 0),
(9, 'Abby ', 'Gardenberg', 'A. G.', '1998-12-01', 1998, 12, 1, 20, 0),
(10, 'Wedekind', 'Falkenstein', 'W. F.', '1989-03-05', 1989, 3, 5, 29, 0),
(11, 'Bernhardin', 'osenblatt', 'B. o.', '1989-08-12', 1989, 8, 12, 29, 0),
(12, 'Howe', 'Blumentahl', 'H. B.', '1998-05-03', 1998, 5, 3, 20, 0),
(13, 'Detlef', 'Zahn', 'D. Z.', '1989-01-05', 1989, 1, 5, 30, 0),
(14, 'Abbeygail ', 'Klee', 'A. K.', '1999-02-11', 1999, 2, 11, 20, 0),
(15, 'Bonifaz', 'Hildenbrandt', 'B. H.', '1989-01-01', 1989, 1, 1, 30, 0),
(16, 'Aleida ', 'Gartung', 'A. G.', '1996-04-05', 1996, 4, 5, 22, 0),
(17, 'Innozenz', 'Sprenger', 'I. S.', '1989-01-14', 1989, 1, 14, 30, 0),
(18, 'Demian', 'ünzer', 'D. ü.', '1998-02-17', 1998, 2, 17, 21, 0),
(19, 'Abby ', 'osenkranz', 'A. o.', '1991-07-31', 1991, 7, 31, 27, 0),
(20, 'Abby ', 'Hildesheimer', 'A. H.', '1998-12-01', 1998, 12, 1, 20, 0),
(21, 'Ana', 'Gomez', 'A. G.', '1999-01-01', NULL, NULL, NULL, 21, 0),
(22, 'Eva', 'Salmón', NULL, '1999-01-01', NULL, NULL, NULL, 20, 0),
(23, 'Michael', 'Fasbender', 'M. F.', '2001-01-09', NULL, NULL, NULL, 0, 0),
(24, 'Daniel', 'Lewis', 'D. L.', '1973-01-09', 1973, 1, 9, 0, 0),
(25, 'Charles', 'Dickens', 'C. D.', '1913-01-09', 1913, 1, 9, 106, 0);

-- 
-- Dumping data for table daclase
--
INSERT INTO daclase VALUES
(7, 1, '2016-01-01', '10:00:00', '11:00:00'),
(7, 2, '2016-01-01', '10:00:00', '11:00:00'),
(7, 3, '2016-01-01', '10:00:00', '11:00:00'),
(8, 1, '2016-01-01', '11:00:00', '12:00:00');

--
-- Set default database
--
USE practica09apoyo;

--
-- Drop trigger `ejercicio21`
--
DROP TRIGGER IF EXISTS ejercicio21;

--
-- Drop trigger `ejercicio22`
--
DROP TRIGGER IF EXISTS ejercicio22;

--
-- Drop trigger `ejercicio26`
--
DROP TRIGGER IF EXISTS ejercicio26;

--
-- Drop trigger `ejercicio13`
--
DROP TRIGGER IF EXISTS ejercicio13;

--
-- Drop trigger `ejercicio17`
--
DROP TRIGGER IF EXISTS ejercicio17;

--
-- Set default database
--
USE practica09apoyo;

DELIMITER $$

--
-- Create trigger `ejercicio21`
--
CREATE
DEFINER = 'root'@'localhost'
TRIGGER ejercicio21
BEFORE INSERT
ON profesor
FOR EACH ROW
BEGIN
  IF NEW.Edad < 18 THEN
    SET NEW.Edad = 18;
  END IF;
END
$$

--
-- Create trigger `ejercicio22`
--
CREATE
DEFINER = 'root'@'localhost'
TRIGGER ejercicio22
BEFORE UPDATE
ON profesor
FOR EACH ROW
BEGIN
  DECLARE msg varchar(255) DEFAULT '';
  IF NEW.Edad < 18 THEN
    SET msg = 'La edad introducida al profedor no puede ser menor de 18.';
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = msg;
  END IF;
END
$$

--
-- Create trigger `ejercicio26`
--
CREATE
DEFINER = 'root'@'localhost'
TRIGGER ejercicio26
BEFORE DELETE
ON profesor
FOR EACH ROW
BEGIN

  DECLARE msg varchar(255) DEFAULT '';
  DECLARE numeroClase int DEFAULT 0;
  SET numeroClase = contarClasesProfesores(OLD.nombre);

  IF numeroClase >= 2 THEN
    SET msg = 'Este profesor ya imparte al menos dos clases.';
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = msg;
  END IF;


END
$$

--
-- Create trigger `ejercicio13`
--
CREATE
DEFINER = 'root'@'localhost'
TRIGGER ejercicio13
BEFORE INSERT
ON alumno
FOR EACH ROW
BEGIN
  SET NEW.iniciales = ejercicio05(NEW.nombre, NEW.apellidos);
  SET NEW.añoNacimiento = YEAR(NEW.fechaNacimiento);
  SET NEW.mesNacimiento = MONTH(NEW.fechaNacimiento);
  SET NEW.diaNacimiento = DAY(NEW.fechaNacimiento);
  SET NEW.Edad = ejercicio10(NEW.fechaNacimiento);
END
$$

--
-- Create trigger `ejercicio17`
--
CREATE
DEFINER = 'root'@'localhost'
TRIGGER ejercicio17
BEFORE UPDATE
ON alumno
FOR EACH ROW
BEGIN
  SET NEW.iniciales = ejercicio05(NEW.nombre, NEW.apellidos);
  SET NEW.añoNacimiento = YEAR(NEW.fechaNacimiento);
  SET NEW.mesNacimiento = MONTH(NEW.fechaNacimiento);
  SET NEW.diaNacimiento = DAY(NEW.fechaNacimiento);
  SET NEW.Edad = ejercicio10(NEW.fechaNacimiento);
END
$$

DELIMITER ;

-- 
-- Restore previous SQL mode
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Enable foreign keys
-- 
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;