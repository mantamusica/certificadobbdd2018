$(document).ready(function () {
var data = [{"text":"Cover Page","icon":"images/folder.svg","href":"startpage.html","target":"DATA"},{"text":"Servers","icon":"images/folder.svg","href":"Servers\\Servers.html","target":"DATA"},{"text":"localhost","icon":"images/folder.svg","href":"Servers\\localhost\\localhost.html","target":"DATA","nodes":[{"text":"Databases","icon":"images/folder.svg","href":"Servers\\localhost\\Databases\\Databases.html","target":"DATA","nodes":[{"text":"practica10apoyo","icon":"images/database.svg","href":"Servers\\localhost\\Databases\\practica10apoyo\\practica10apoyo.html","target":"DATA","nodes":[{"text":"Tables","icon":"images/folder.svg","href":"Servers\\localhost\\Databases\\practica10apoyo\\Tables\\Tables.html","target":"DATA","nodes":[{"text":"categories","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica10apoyo\\Tables\\categories.html","target":"DATA"},{"text":"order details","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica10apoyo\\Tables\\order_details.html","target":"DATA"},{"text":"orders","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica10apoyo\\Tables\\orders.html","target":"DATA"},{"text":"products","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica10apoyo\\Tables\\products.html","target":"DATA"},{"text":"suppliers","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica10apoyo\\Tables\\suppliers.html","target":"DATA"}]},{"text":"Functions","icon":"images/folder.svg","href":"Servers\\localhost\\Databases\\practica10apoyo\\Functions\\Functions.html","target":"DATA","nodes":[{"text":"comprobacionCategoria","icon":"images/function.svg","href":"Servers\\localhost\\Databases\\practica10apoyo\\Functions\\comprobacionCategoria.html","target":"DATA"},{"text":"comprobacionCategoriasProductos","icon":"images/function.svg","href":"Servers\\localhost\\Databases\\practica10apoyo\\Functions\\comprobacionCategoriasProductos.html","target":"DATA"},{"text":"comprobacionPedido","icon":"images/function.svg","href":"Servers\\localhost\\Databases\\practica10apoyo\\Functions\\comprobacionPedido.html","target":"DATA"},{"text":"comprobacionPedidos","icon":"images/function.svg","href":"Servers\\localhost\\Databases\\practica10apoyo\\Functions\\comprobacionPedidos.html","target":"DATA"},{"text":"comprobacionProducto","icon":"images/function.svg","href":"Servers\\localhost\\Databases\\practica10apoyo\\Functions\\comprobacionProducto.html","target":"DATA"},{"text":"comprobacionProductosPedidos","icon":"images/function.svg","href":"Servers\\localhost\\Databases\\practica10apoyo\\Functions\\comprobacionProductosPedidos.html","target":"DATA"},{"text":"comprobacionStock","icon":"images/function.svg","href":"Servers\\localhost\\Databases\\practica10apoyo\\Functions\\comprobacionStock.html","target":"DATA"},{"text":"comprobacionSupplier","icon":"images/function.svg","href":"Servers\\localhost\\Databases\\practica10apoyo\\Functions\\comprobacionSupplier.html","target":"DATA"},{"text":"comprobacionSupplierProductos","icon":"images/function.svg","href":"Servers\\localhost\\Databases\\practica10apoyo\\Functions\\comprobacionSupplierProductos.html","target":"DATA"},{"text":"precioProducto","icon":"images/function.svg","href":"Servers\\localhost\\Databases\\practica10apoyo\\Functions\\precioProducto.html","target":"DATA"}]},{"text":"Triggers","icon":"images/folder.svg","href":"Servers\\localhost\\Databases\\practica10apoyo\\Triggers\\Triggers.html","target":"DATA","nodes":[{"text":"categories_delete","icon":"images/trigger.svg","href":"Servers\\localhost\\Databases\\practica10apoyo\\Triggers\\categories_delete.html","target":"DATA"},{"text":"order_details_insert","icon":"images/trigger.svg","href":"Servers\\localhost\\Databases\\practica10apoyo\\Triggers\\order_details_insert.html","target":"DATA"},{"text":"order_details_update","icon":"images/trigger.svg","href":"Servers\\localhost\\Databases\\practica10apoyo\\Triggers\\order_details_update.html","target":"DATA"},{"text":"pedidos_delete","icon":"images/trigger.svg","href":"Servers\\localhost\\Databases\\practica10apoyo\\Triggers\\pedidos_delete.html","target":"DATA"},{"text":"productos_delete","icon":"images/trigger.svg","href":"Servers\\localhost\\Databases\\practica10apoyo\\Triggers\\productos_delete.html","target":"DATA"},{"text":"products_insert","icon":"images/trigger.svg","href":"Servers\\localhost\\Databases\\practica10apoyo\\Triggers\\products_insert.html","target":"DATA"},{"text":"products_update","icon":"images/trigger.svg","href":"Servers\\localhost\\Databases\\practica10apoyo\\Triggers\\products_update.html","target":"DATA"},{"text":"suppliers_delete","icon":"images/trigger.svg","href":"Servers\\localhost\\Databases\\practica10apoyo\\Triggers\\suppliers_delete.html","target":"DATA"}]}]}]}]}];
$('#tree').treeview({levels: 3,data: data,enableLinks: true,injectStyle: false,highlightSelected: true,collapseIcon: 'images/tree-node-expanded.svg',expandIcon: 'images/tree-node-collapsed.svg'});
});
var loadEvent = function () {

  $('#btn-expand-nodes').on('click', function (e) {
    $('#tree').treeview('expandAll', { silent: true });
  });
  $('#btn-collapse-nodes').on('click', function (e) {
    $('#tree').treeview('collapseAll', { levels:3, silent: true });
  });
  
  var searchTimeOut;
  $('#input-search').on('input', function() {
    if(searchTimeOut != null)
      clearTimeout(searchTimeOut);
    searchTimeOut = setTimeout(function(){
      var pattern = $('#input-search').val();
      var tree = $('#tree');
      tree.treeview('collapseAll', { levels:3, silent: true });
      var options = { ignoreCase: true, exactMatch: false, revealResults: true };
      var results = tree.treeview('search', [pattern, options]);
    }, 500);
  });
  
  $('#tree').on('nodeSelected', function(event, data) {
    // navigate to link
    window.open (data.href, 'DATA', false)
  });
  // select first node.
  $('#tree').treeview('selectNode', [0, { silent: false }]);
}

if (window.addEventListener) {
  window.addEventListener('load', loadEvent, false);
}
else if (window.attachEvent) {
  window.attachEvent('onload', loadEvent);
}