﻿-- 1) Importar la base de datos Northwind

-- 2) Realizar el diagrama E/R de la base de datos 

-- 3) Crear un procedimiento que te actualice el precio unitario 
-- de los detalles del pedido al precio unitario del producto correspondiente

  CALL ejercicio03();

-- 4) Crear los disparadores necesarios para que controlar que el precio unitario
-- de las tablas productos y detalles de los pedidos coincidan

-- 5) Realizar una vista donde te permita calcular el precio total de todos los pedidos realizados. 
-- Para ello hay que tener en cuenta la siguiente formula Precio Total = Precio unitario * cantidad 

  SELECT * FROM ejercicio05;

-- 6) Añadir el campo precio total a la tabla pedidos 

  ALTER TABLE `order details` ADD COLUMN PrecioTotal decimal(10,2);

-- 7) Crear un procedimiento que te permita calcular los precios totales de la tabla pedidos teniendo en cuenta la fórmula del apartado 5. 

  CALL ejercicio07();

-- 8) Crear los disparadores necesarios para que el precio total se calcule automáticamente
-- 9) Hay que tener en cuenta que cada vez que haya un detalle de pedido el campo cantidad debe ser mayor que 0.
--  Controlarlo a través de disparadores y no de vistas 

-- 10) Hay que tener en cuenta que el número de unidades pedidas no debe exceder la cantidad de elementos que hay en stock en ese momento. 
--   Para ello debéis tener en cuenta que cada vez que se realice un detalle de pedido tendréis que ir restando las unidades pedidas al stock 
--  de ese producto y lógicamente si se cancela el pedido o el detalle del pedido sumar esas unidades al stock



-- 11) Añadir a la tabla pedidos un campo de tipo lógico llamado entregado. Si el pedido ya esta entregado ya no se debe poder borrar de la base de datos

  ALTER TABLE orders ADD COLUMN IF NOT EXISTS Delivered boolean DEFAULT 1;

-- 12) Crear un procedimiento que me permita crear una tabla con un campo que sea el nombre completo del empleado 
--  y el puesto junto con el nombre completo del empleado al que tiene que informar 

  SELECT * FROM ejercicio12 e;

  CALL ejercicio12();
