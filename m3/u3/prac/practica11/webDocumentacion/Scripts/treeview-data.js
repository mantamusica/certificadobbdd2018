$(document).ready(function () {
var data = [{"text":"Cover Page","icon":"images/folder.svg","href":"startpage.html","target":"DATA"},{"text":"Servers","icon":"images/folder.svg","href":"Servers\\Servers.html","target":"DATA"},{"text":"localhost","icon":"images/folder.svg","href":"Servers\\localhost\\localhost.html","target":"DATA","nodes":[{"text":"Databases","icon":"images/folder.svg","href":"Servers\\localhost\\Databases\\Databases.html","target":"DATA","nodes":[{"text":"practica11apoyo","icon":"images/database.svg","href":"Servers\\localhost\\Databases\\practica11apoyo\\practica11apoyo.html","target":"DATA","nodes":[{"text":"Tables","icon":"images/folder.svg","href":"Servers\\localhost\\Databases\\practica11apoyo\\Tables\\Tables.html","target":"DATA","nodes":[{"text":"categories","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica11apoyo\\Tables\\categories.html","target":"DATA"},{"text":"customercustomerdemo","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica11apoyo\\Tables\\customercustomerdemo.html","target":"DATA"},{"text":"customerdemographics","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica11apoyo\\Tables\\customerdemographics.html","target":"DATA"},{"text":"customers","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica11apoyo\\Tables\\customers.html","target":"DATA"},{"text":"employees","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica11apoyo\\Tables\\employees.html","target":"DATA"},{"text":"employees_informant","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica11apoyo\\Tables\\employees_informant.html","target":"DATA"},{"text":"employeeterritories","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica11apoyo\\Tables\\employeeterritories.html","target":"DATA"},{"text":"order details","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica11apoyo\\Tables\\order_details.html","target":"DATA"},{"text":"orders","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica11apoyo\\Tables\\orders.html","target":"DATA"},{"text":"products","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica11apoyo\\Tables\\products.html","target":"DATA"},{"text":"region","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica11apoyo\\Tables\\region.html","target":"DATA"},{"text":"shippers","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica11apoyo\\Tables\\shippers.html","target":"DATA"},{"text":"suppliers","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica11apoyo\\Tables\\suppliers.html","target":"DATA"},{"text":"territories","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica11apoyo\\Tables\\territories.html","target":"DATA"}]},{"text":"Views","icon":"images/folder.svg","href":"Servers\\localhost\\Databases\\practica11apoyo\\Views\\Views.html","target":"DATA","nodes":[{"text":"ejercicio05","icon":"images/view.svg","href":"Servers\\localhost\\Databases\\practica11apoyo\\Views\\ejercicio05.html","target":"DATA"},{"text":"ejercicio12","icon":"images/view.svg","href":"Servers\\localhost\\Databases\\practica11apoyo\\Views\\ejercicio12.html","target":"DATA"}]},{"text":"Procedures","icon":"images/folder.svg","href":"Servers\\localhost\\Databases\\practica11apoyo\\Procedures\\Procedures.html","target":"DATA","nodes":[{"text":"ejercicio03","icon":"images/procedure.svg","href":"Servers\\localhost\\Databases\\practica11apoyo\\Procedures\\ejercicio03.html","target":"DATA"},{"text":"ejercicio07","icon":"images/procedure.svg","href":"Servers\\localhost\\Databases\\practica11apoyo\\Procedures\\ejercicio07.html","target":"DATA"},{"text":"ejercicio12","icon":"images/procedure.svg","href":"Servers\\localhost\\Databases\\practica11apoyo\\Procedures\\ejercicio12.html","target":"DATA"}]},{"text":"Functions","icon":"images/folder.svg","href":"Servers\\localhost\\Databases\\practica11apoyo\\Functions\\Functions.html","target":"DATA","nodes":[{"text":"agregarProductos","icon":"images/function.svg","href":"Servers\\localhost\\Databases\\practica11apoyo\\Functions\\agregarProductos.html","target":"DATA"},{"text":"restaProductos","icon":"images/function.svg","href":"Servers\\localhost\\Databases\\practica11apoyo\\Functions\\restaProductos.html","target":"DATA"}]},{"text":"Triggers","icon":"images/folder.svg","href":"Servers\\localhost\\Databases\\practica11apoyo\\Triggers\\Triggers.html","target":"DATA","nodes":[{"text":"ejercicio04_insert","icon":"images/trigger.svg","href":"Servers\\localhost\\Databases\\practica11apoyo\\Triggers\\ejercicio04_insert.html","target":"DATA"},{"text":"ejercicio04_update","icon":"images/trigger.svg","href":"Servers\\localhost\\Databases\\practica11apoyo\\Triggers\\ejercicio04_update.html","target":"DATA"},{"text":"ejercicio08_11_insert","icon":"images/trigger.svg","href":"Servers\\localhost\\Databases\\practica11apoyo\\Triggers\\ejercicio08_11_insert.html","target":"DATA"},{"text":"ejercicio08_11_update","icon":"images/trigger.svg","href":"Servers\\localhost\\Databases\\practica11apoyo\\Triggers\\ejercicio08_11_update.html","target":"DATA"},{"text":"ejercicio11","icon":"images/trigger.svg","href":"Servers\\localhost\\Databases\\practica11apoyo\\Triggers\\ejercicio11.html","target":"DATA"},{"text":"ejercicio11_parte_dos","icon":"images/trigger.svg","href":"Servers\\localhost\\Databases\\practica11apoyo\\Triggers\\ejercicio11_parte_dos.html","target":"DATA"}]}]}]}]}];
$('#tree').treeview({levels: 3,data: data,enableLinks: true,injectStyle: false,highlightSelected: true,collapseIcon: 'images/tree-node-expanded.svg',expandIcon: 'images/tree-node-collapsed.svg'});
});
var loadEvent = function () {

  $('#btn-expand-nodes').on('click', function (e) {
    $('#tree').treeview('expandAll', { silent: true });
  });
  $('#btn-collapse-nodes').on('click', function (e) {
    $('#tree').treeview('collapseAll', { levels:3, silent: true });
  });
  
  var searchTimeOut;
  $('#input-search').on('input', function() {
    if(searchTimeOut != null)
      clearTimeout(searchTimeOut);
    searchTimeOut = setTimeout(function(){
      var pattern = $('#input-search').val();
      var tree = $('#tree');
      tree.treeview('collapseAll', { levels:3, silent: true });
      var options = { ignoreCase: true, exactMatch: false, revealResults: true };
      var results = tree.treeview('search', [pattern, options]);
    }, 500);
  });
  
  $('#tree').on('nodeSelected', function(event, data) {
    // navigate to link
    window.open (data.href, 'DATA', false)
  });
  // select first node.
  $('#tree').treeview('selectNode', [0, { silent: false }]);
}

if (window.addEventListener) {
  window.addEventListener('load', loadEvent, false);
}
else if (window.attachEvent) {
  window.attachEvent('onload', loadEvent);
}