﻿--
-- Script was generated by Devart dbForge Studio for MySQL, Version 8.0.40.0
-- Product home page: http://www.devart.com/dbforge/mysql/studio
-- Script date 25/02/2019 18:11:03
-- Server version: 5.5.5-10.1.30-MariaDB
-- Client version: 4.1
--

-- 
-- Disable foreign keys
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Set SQL mode
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Set character set the client will use to send SQL statements to the server
--
SET NAMES 'utf8';

DROP DATABASE IF EXISTS practica12apoyo;

CREATE DATABASE practica12apoyo
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Set default database
--
USE practica12apoyo;

--
-- Create table `emple`
--
CREATE TABLE emple (
  emp_no int(11) NOT NULL,
  apellido varchar(50) NOT NULL,
  oficio varchar(30) DEFAULT NULL,
  dir int(11) DEFAULT NULL,
  fecha_alt date DEFAULT NULL,
  salario int(11) DEFAULT NULL,
  comision int(11) DEFAULT NULL,
  dept_no int(11) DEFAULT NULL,
  PRIMARY KEY (emp_no)
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 1170,
CHARACTER SET utf8,
COLLATE utf8_spanish_ci;

DELIMITER $$

--
-- Create procedure `ejercicio01`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE ejercicio01 (OUT valor int)
COMMENT 'Procedimiento para contar las altas en el día actual'
BEGIN

  SELECT
    COUNT(*) INTO valor
  FROM emple e
  WHERE e.fecha_alt = CURRENT_DATE();
END
$$

DELIMITER ;

--
-- Create table `test`
--
CREATE TABLE test (
  id int(11) NOT NULL AUTO_INCREMENT,
  a int(11) DEFAULT NULL,
  b int(11) DEFAULT NULL,
  c int(11) DEFAULT NULL,
  time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 7,
AVG_ROW_LENGTH = 5461,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

DELIMITER $$

--
-- Create procedure `ejercicio07`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE ejercicio07 ()
COMMENT 'Procedimiento para eliminar las filas de datos que se repitan a y b'
BEGIN
  ALTER IGNORE TABLE test ADD UNIQUE INDEX (a, b);
  ALTER TABLE test DROP INDEX a;
END
$$

--
-- Create event `ejercicio08`
--
CREATE 
	DEFINER = 'root'@'localhost'
EVENT ejercicio08
	ON SCHEDULE EVERY '1' DAY
	STARTS '2019-02-25 17:00:00'
	COMMENT 'Evento que hará que le procedimiento 07 ser repita todos los dia'
	DO 
BEGIN
  CALL ejercicio07();
END
$$

ALTER EVENT ejercicio08
	ENABLE
$$

DELIMITER ;

--
-- Create table `datos`
--
CREATE TABLE datos (
  id int(11) NOT NULL AUTO_INCREMENT,
  fecha date DEFAULT NULL,
  número int(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 2,
AVG_ROW_LENGTH = 16384,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

DELIMITER $$

--
-- Create procedure `ejercicio02`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE ejercicio02 ()
COMMENT 'Procedimiento para crear una tabla y guardar los datos creados en el procedimiento anterior.'
BEGIN

  DECLARE valor int DEFAULT 0;
  CREATE TABLE IF NOT EXISTS datos (
    id int AUTO_INCREMENT PRIMARY KEY,
    fecha date,
    número int
  );

  CALL ejercicio01(@valor);
  SET valor = @valor;
  INSERT INTO datos
  SET fecha = CURRENT_DATE(),
      número = valor;
END
$$

--
-- Create event `ejercicio04`
--
CREATE
DEFINER = 'root'@'localhost'
EVENT ejercicio04
ON SCHEDULE EVERY '1' DAY
STARTS '2019-02-25 11:00:00'
DISABLE
COMMENT 'Evento que ejecuta el procedimiento 02 cada día a las once de la'
DO
BEGIN
  CALL ejercicio02();
END
$$

--
-- Create event `ejercicio03`
--
CREATE 
	DEFINER = 'root'@'localhost'
EVENT ejercicio03
	ON SCHEDULE EVERY '1' MINUTE
	STARTS '2019-02-25 17:55:28'
	COMMENT 'Evento que ejecuta el procedimiento anterior cada minuto.'
	DO 
BEGIN
  CALL ejercicio02();
END
$$

ALTER EVENT ejercicio03
	ENABLE
$$

DELIMITER ;

--
-- Create table `depart`
--
CREATE TABLE depart (
  dept_no int(11) DEFAULT NULL,
  dnombre varchar(30) DEFAULT NULL,
  loc varchar(30) DEFAULT NULL
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 4096,
CHARACTER SET utf8,
COLLATE utf8_spanish_ci;

-- 
-- Dumping data for table test
--
INSERT INTO test VALUES
(1, 1, 2, 3, '2019-02-25 18:04:35'),
(3, 2, 3, 3, '2019-02-25 18:04:35'),
(4, 3, 4, 4, '2019-02-25 18:04:35');

-- 
-- Dumping data for table emple
--
INSERT INTO emple VALUES
(7369, 'SÁNCHEZ', 'EMPLEADO', 7902, '1990-12-17', 1040, NULL, 20),
(7499, 'ARROYO', 'VENDEDOR', 7698, '1990-02-20', 1500, 390, 30),
(7521, 'SALA', 'VENDEDOR', 7698, '1991-02-22', 1625, 650, 30),
(7566, 'JIMÉNEZ', 'DIRECTOR', 7839, '1991-04-02', 2900, NULL, 20),
(7654, 'MARTÍN', 'VENDEDOR', 7698, '1991-09-29', 1600, 1020, 30),
(7698, 'NEGRO', 'DIRECTOR', 7839, '1991-05-01', 3005, NULL, 30),
(7782, 'CEREZO', 'DIRECTOR', 7839, '1991-06-09', 2885, NULL, 10),
(7788, 'GIL', 'ANALISTA', 7566, '1991-11-09', 3000, NULL, 20),
(7839, 'REY', 'PRESIDENTE', NULL, '1991-11-17', 4100, NULL, 10),
(7844, 'TOVAR', 'VENDEDOR', 7698, '1991-09-08', 1350, 0, 30),
(7876, 'ALONSO', 'EMPLEADO', 7788, '1991-09-23', 1430, NULL, 20),
(7900, 'JIMENO', 'EMPLEADO', 7698, '1991-12-03', 1335, NULL, 30),
(7902, 'FERNÁNDEZ', 'ANALISTA', 7566, '1991-12-03', 3000, NULL, 20),
(7934, 'MUÑOZ', 'EMPLEADO', 7782, '1992-01-23', 1690, NULL, 10);

-- 
-- Dumping data for table depart
--
INSERT INTO depart VALUES
(10, 'CONTABILIDAD', 'SEVILLA'),
(20, 'INVESTIGACIÓN', 'MADRID'),
(30, 'VENTAS', 'BARCELONA'),
(40, 'PRODUCCIÓN', 'BILBAO');

-- 
-- Dumping data for table datos
--
INSERT INTO datos VALUES
(1, '2019-02-25', 0);

-- 
-- Restore previous SQL mode
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Enable foreign keys
-- 
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;