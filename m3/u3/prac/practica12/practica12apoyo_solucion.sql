﻿/* 2- Modificarle para que vuelque los datos en una tabla nueva llamada DATOS(crearla antes). 
  Esta tabla tendrá un campo id de tipo autonumerico y el campo fecha (tipo DATE) y el campo número (INT). 
  Cada vez que se ejecute el script creara un nuevo registro con id automático y en fecha pondrá la de hoy 
  y en número el valor devuelto por la consulta del procedimiento almacenado. A este procedimiento llamarle p2.*/

  CALL ejercicio01(@numero);
  SELECT @numero;

  CALL ejercicio02();

/* 5- Crear una tabla con la siguiente estructura (llamarla test)  */

  DROP TABLE IF EXISTS test;
  CREATE TABLE IF NOT EXISTS test(
    id int AUTO_INCREMENT PRIMARY KEY,
    a int DEFAULT NULL,
    b int DEFAULT NULL,
    c int DEFAULT NULL,
    time  timestamp DEFAULT CURRENT_TIMESTAMP
  );

/* Introducir los siguientes registros */

  INSERT INTO test VALUES 
    (DEFAULT,1,2,3,DEFAULT),
    (DEFAULT,1,2,3,DEFAULT),
    (DEFAULT,2,3,3,DEFAULT),
    (DEFAULT,3,4,4,DEFAULT),
    (DEFAULT,1,2,5,DEFAULT),
    (DEFAULT,1,2,3,DEFAULT);

  CALL ejercicio07();
  SELECT * FROM test;