$(document).ready(function () {
var data = [{"text":"Cover Page","icon":"images/folder.svg","href":"startpage.html","target":"DATA"},{"text":"Servers","icon":"images/folder.svg","href":"Servers\\Servers.html","target":"DATA"},{"text":"localhost","icon":"images/folder.svg","href":"Servers\\localhost\\localhost.html","target":"DATA","nodes":[{"text":"Databases","icon":"images/folder.svg","href":"Servers\\localhost\\Databases\\Databases.html","target":"DATA","nodes":[{"text":"practica12apoyo","icon":"images/database.svg","href":"Servers\\localhost\\Databases\\practica12apoyo\\practica12apoyo.html","target":"DATA","nodes":[{"text":"Tables","icon":"images/folder.svg","href":"Servers\\localhost\\Databases\\practica12apoyo\\Tables\\Tables.html","target":"DATA","nodes":[{"text":"datos","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica12apoyo\\Tables\\datos.html","target":"DATA"},{"text":"depart","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica12apoyo\\Tables\\depart.html","target":"DATA"},{"text":"emple","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica12apoyo\\Tables\\emple.html","target":"DATA"},{"text":"test","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica12apoyo\\Tables\\test.html","target":"DATA"}]},{"text":"Procedures","icon":"images/folder.svg","href":"Servers\\localhost\\Databases\\practica12apoyo\\Procedures\\Procedures.html","target":"DATA","nodes":[{"text":"ejercicio01","icon":"images/procedure.svg","href":"Servers\\localhost\\Databases\\practica12apoyo\\Procedures\\ejercicio01.html","target":"DATA"},{"text":"ejercicio02","icon":"images/procedure.svg","href":"Servers\\localhost\\Databases\\practica12apoyo\\Procedures\\ejercicio02.html","target":"DATA"},{"text":"ejercicio07","icon":"images/procedure.svg","href":"Servers\\localhost\\Databases\\practica12apoyo\\Procedures\\ejercicio07.html","target":"DATA"}]},{"text":"Events","icon":"images/folder.svg","href":"Servers\\localhost\\Databases\\practica12apoyo\\Events\\Events.html","target":"DATA","nodes":[{"text":"ejercicio03","icon":"images/event.svg","href":"Servers\\localhost\\Databases\\practica12apoyo\\Events\\ejercicio03.html","target":"DATA"},{"text":"ejercicio04","icon":"images/event.svg","href":"Servers\\localhost\\Databases\\practica12apoyo\\Events\\ejercicio04.html","target":"DATA"},{"text":"ejercicio08","icon":"images/event.svg","href":"Servers\\localhost\\Databases\\practica12apoyo\\Events\\ejercicio08.html","target":"DATA"}]}]}]}]}];
$('#tree').treeview({levels: 3,data: data,enableLinks: true,injectStyle: false,highlightSelected: true,collapseIcon: 'images/tree-node-expanded.svg',expandIcon: 'images/tree-node-collapsed.svg'});
});
var loadEvent = function () {

  $('#btn-expand-nodes').on('click', function (e) {
    $('#tree').treeview('expandAll', { silent: true });
  });
  $('#btn-collapse-nodes').on('click', function (e) {
    $('#tree').treeview('collapseAll', { levels:3, silent: true });
  });
  
  var searchTimeOut;
  $('#input-search').on('input', function() {
    if(searchTimeOut != null)
      clearTimeout(searchTimeOut);
    searchTimeOut = setTimeout(function(){
      var pattern = $('#input-search').val();
      var tree = $('#tree');
      tree.treeview('collapseAll', { levels:3, silent: true });
      var options = { ignoreCase: true, exactMatch: false, revealResults: true };
      var results = tree.treeview('search', [pattern, options]);
    }, 500);
  });
  
  $('#tree').on('nodeSelected', function(event, data) {
    // navigate to link
    window.open (data.href, 'DATA', false)
  });
  // select first node.
  $('#tree').treeview('selectNode', [0, { silent: false }]);
}

if (window.addEventListener) {
  window.addEventListener('load', loadEvent, false);
}
else if (window.attachEvent) {
  window.attachEvent('onload', loadEvent);
}