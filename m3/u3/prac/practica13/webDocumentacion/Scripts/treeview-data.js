$(document).ready(function () {
var data = [{"text":"Cover Page","icon":"images/folder.svg","href":"startpage.html","target":"DATA"},{"text":"Servers","icon":"images/folder.svg","href":"Servers\\Servers.html","target":"DATA"},{"text":"localhost","icon":"images/folder.svg","href":"Servers\\localhost\\localhost.html","target":"DATA","nodes":[{"text":"Databases","icon":"images/folder.svg","href":"Servers\\localhost\\Databases\\Databases.html","target":"DATA","nodes":[{"text":"practica13apoyo","icon":"images/database.svg","href":"Servers\\localhost\\Databases\\practica13apoyo\\practica13apoyo.html","target":"DATA","nodes":[{"text":"Tables","icon":"images/folder.svg","href":"Servers\\localhost\\Databases\\practica13apoyo\\Tables\\Tables.html","target":"DATA","nodes":[{"text":"examenes","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica13apoyo\\Tables\\examenes.html","target":"DATA"},{"text":"notas","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica13apoyo\\Tables\\notas.html","target":"DATA"},{"text":"opositores","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica13apoyo\\Tables\\opositores.html","target":"DATA"},{"text":"tribunales","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica13apoyo\\Tables\\tribunales.html","target":"DATA"},{"text":"vocales","icon":"images/table.svg","href":"Servers\\localhost\\Databases\\practica13apoyo\\Tables\\vocales.html","target":"DATA"}]},{"text":"Views","icon":"images/folder.svg","href":"Servers\\localhost\\Databases\\practica13apoyo\\Views\\Views.html","target":"DATA","nodes":[{"text":"ejercicio01","icon":"images/view.svg","href":"Servers\\localhost\\Databases\\practica13apoyo\\Views\\ejercicio01.html","target":"DATA"},{"text":"ejercicio02max","icon":"images/view.svg","href":"Servers\\localhost\\Databases\\practica13apoyo\\Views\\ejercicio02max.html","target":"DATA"},{"text":"ejercicio02min","icon":"images/view.svg","href":"Servers\\localhost\\Databases\\practica13apoyo\\Views\\ejercicio02min.html","target":"DATA"},{"text":"ejercicio04","icon":"images/view.svg","href":"Servers\\localhost\\Databases\\practica13apoyo\\Views\\ejercicio04.html","target":"DATA"}]},{"text":"Procedures","icon":"images/folder.svg","href":"Servers\\localhost\\Databases\\practica13apoyo\\Procedures\\Procedures.html","target":"DATA","nodes":[{"text":"ejercicio02b","icon":"images/procedure.svg","href":"Servers\\localhost\\Databases\\practica13apoyo\\Procedures\\ejercicio02b.html","target":"DATA"},{"text":"ejercicio03_con_cursor","icon":"images/procedure.svg","href":"Servers\\localhost\\Databases\\practica13apoyo\\Procedures\\ejercicio03_con_cursor.html","target":"DATA"},{"text":"ejercicio03_sin_cursor","icon":"images/procedure.svg","href":"Servers\\localhost\\Databases\\practica13apoyo\\Procedures\\ejercicio03_sin_cursor.html","target":"DATA"},{"text":"ejercicio04_vocales","icon":"images/procedure.svg","href":"Servers\\localhost\\Databases\\practica13apoyo\\Procedures\\ejercicio04_vocales.html","target":"DATA"},{"text":"ejercicio05","icon":"images/procedure.svg","href":"Servers\\localhost\\Databases\\practica13apoyo\\Procedures\\ejercicio05.html","target":"DATA"},{"text":"ejercicio08a","icon":"images/procedure.svg","href":"Servers\\localhost\\Databases\\practica13apoyo\\Procedures\\ejercicio08a.html","target":"DATA"}]},{"text":"Triggers","icon":"images/folder.svg","href":"Servers\\localhost\\Databases\\practica13apoyo\\Triggers\\Triggers.html","target":"DATA","nodes":[{"text":"ejercicio07_insert","icon":"images/trigger.svg","href":"Servers\\localhost\\Databases\\practica13apoyo\\Triggers\\ejercicio07_insert.html","target":"DATA"},{"text":"ejercicio07_update","icon":"images/trigger.svg","href":"Servers\\localhost\\Databases\\practica13apoyo\\Triggers\\ejercicio07_update.html","target":"DATA"},{"text":"introducirNota","icon":"images/trigger.svg","href":"Servers\\localhost\\Databases\\practica13apoyo\\Triggers\\introducirNota.html","target":"DATA"},{"text":"modificarNota","icon":"images/trigger.svg","href":"Servers\\localhost\\Databases\\practica13apoyo\\Triggers\\modificarNota.html","target":"DATA"}]}]}]}]}];
$('#tree').treeview({levels: 3,data: data,enableLinks: true,injectStyle: false,highlightSelected: true,collapseIcon: 'images/tree-node-expanded.svg',expandIcon: 'images/tree-node-collapsed.svg'});
});
var loadEvent = function () {

  $('#btn-expand-nodes').on('click', function (e) {
    $('#tree').treeview('expandAll', { silent: true });
  });
  $('#btn-collapse-nodes').on('click', function (e) {
    $('#tree').treeview('collapseAll', { levels:3, silent: true });
  });
  
  var searchTimeOut;
  $('#input-search').on('input', function() {
    if(searchTimeOut != null)
      clearTimeout(searchTimeOut);
    searchTimeOut = setTimeout(function(){
      var pattern = $('#input-search').val();
      var tree = $('#tree');
      tree.treeview('collapseAll', { levels:3, silent: true });
      var options = { ignoreCase: true, exactMatch: false, revealResults: true };
      var results = tree.treeview('search', [pattern, options]);
    }, 500);
  });
  
  $('#tree').on('nodeSelected', function(event, data) {
    // navigate to link
    window.open (data.href, 'DATA', false)
  });
  // select first node.
  $('#tree').treeview('selectNode', [0, { silent: false }]);
}

if (window.addEventListener) {
  window.addEventListener('load', loadEvent, false);
}
else if (window.attachEvent) {
  window.attachEvent('onload', loadEvent);
}